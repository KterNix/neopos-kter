//
//  BasicInforCartCell.swift
//  neoPOS
//
//  Created by mac on 8/30/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class BasicInforCartCell: MasterTableViewCell {

    //MARK:- Outlet
    @IBOutlet weak var positionOrder: TitleLabel!
    @IBOutlet weak var nameCustomer: RecommentLabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.borderWidth = 2
        self.borderColor = .white
    }
    func setupDataSource(order:OrderModel,index:Int) {
        self.positionOrder.text =  index.description
        self.nameCustomer.text = order.customer?.fullname
    }
    
    //MARK:- Outlet Actions
    
}

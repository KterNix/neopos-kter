//
//  CartController.swift
//  neoPOS
//
//  Created by mac on 8/28/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import SideMenu
import TSMessages
private let idCell = "CartProductCell"

class CartController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var totalMoneyLabel: TitleLabel!
    @IBOutlet weak var btnDraft: StyleDefault!
    @IBOutlet weak var btnOrder: StyleDefault!
    @IBOutlet weak var viewParentTotalMoney: UIView!
    @IBOutlet weak var textEmpty: TitleLabel!
    
    
    //MARK:- Declare
    var order :OrderModel?
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        setupUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadAfterSelectedFromMultiCart()
        self.listenKeyboard()
    }
    deinit {
        Utils.indexOrderSelected = nil
        print("\(self) deinit =================")
    }
    //MARK:- SubFunction
    func setupUI() {
        self.totalMoneyLabel.textColor = .red
        self.collectionView.backgroundColor = .groupTableViewBackground
        self.view.backgroundColor = .groupTableViewBackground
//        self.createBarItem( title: nil, imageName: "customers", selector: #selector(self.didPressCustomers))
        self.btnDraft.colorTitle = .white
        self.btnDraft.setUpWithBackground(color: .orange)
        self.btnOrder.colorTitle = .white
        self.btnOrder.setUpWithBackground(color: .mainColor)
        self.viewParentTotalMoney.backgroundColor = .groupTableViewBackground
        setBackButton()
        
    }
    func updateTotalPrice() {
        guard let products = order?.details else {return}
        let prices = products.map{$0.sizeWareHouse?.price}
        let quantities = products.map{$0.sizeWareHouse?.quantity}
        var total = 0.0
        for (index, value) in prices.enumerated(){
            total += (quantities[index]).asDouble * value.asDouble
        }
        self.totalMoneyLabel.text = total.withCommas()
        self.order?.total = total.description
    }
    func setupDataSource() {
        self.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    func reloadAfterSelectedFromMultiCart() {
//        if let indexOrderSelected = Utils.indexOrderSelected {
//            self.order = Utils.orders.isEmpty ? nil : Utils.orders[indexOrderSelected]
//        }
        
        if let details = self.order?.details, !details.isEmpty {
            self.collectionView.isHidden =  false
            self.viewParentTotalMoney.isHidden = false
            self.updateTotalPrice()
            self.collectionView.reloadData()
            
        }else {
            self.collectionView.isHidden = true
            self.viewParentTotalMoney.isHidden = true
        }
        if self.order != nil, self.navigationItem.rightBarButtonItems?.count == nil {
            self.createBarItem( title: "+", imageName: nil, selector: #selector(self.didPressAddMoreProducts), fontSize: 30)
            if self.order?.code != nil {
                self.createBarItem(title: nil, imageName: "delete", selector: #selector(self.didPressDeleteDraft))
            }
        }
        self.title = order == nil  ? "Giỏ hàng" : "Giỏ hàng ( \((order?.customer?.fullname).asString) )"
        if self.order == nil {
            let attacment = NSTextAttachment()
            attacment.image = UIImage(named: "customers")?.resize(width: 20, height: 20)?.withRenderingMode(.alwaysTemplate)
            let imageString = NSAttributedString(attachment: attacment)
            let firstString = NSMutableAttributedString(string: "Nhấn \" ")
            let lastString = NSAttributedString(string: " \" để thêm khách hàng trước khi chọn sản phẩm.")
            firstString.append(imageString)
            firstString.append(lastString)
            self.textEmpty.attributedText = firstString
        }else {
            self.textEmpty.text = "Giỏ hàng đang trống, nhấn \" + \" để thêm sản phẩm vào giỏ."
        }
        
    }
    @objc func didPressDeleteDraft() {
        Utils.alertWithAction(title: "Xoá nháp", cancelTitle: "Huỷ", message: "Bạn muốn xoá bản nháp này?", actionTitles: ["Đúng"], actions: [{ (alert) in
            guard let order = self.order else {
                TSMessage.showNotification(in: self.navigationController!, title: "Xoá không thành công!", subtitle: "Đơn hàng này không tồn tại nên không thể xoá.", type: .error)
                return}
            guard var orders = Utils.shared.getDraftOrders() else {return}
            if let index = orders.firstIndex(where: { (orderSaved) -> Bool in
                orderSaved.code == order.code
            }) {
                
                orders.remove(at: index)
            }
            Utils.shared.saveDraftOrders(value: orders)
            TSMessage.showNotification(withTitle: "Xoá thành công", type: .success)
            }], actionStyle: .destructive, viewController: self, style: .alert)
       
    }
    
    @objc func didPressCustomers()  {
        self.dismissKeyboard()
        let multi = UISideMenuNavigationController(rootViewController: MultiCartController() )
        multi.sideMenuDelegate = self
        SideMenuManager.defaultManager.menuRightNavigationController = multi
        self.present(SideMenuManager.defaultManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    @objc func didPressAddMoreProducts() {
        let vc = ProductsController()
        vc.type = .Order
        vc.order = self.order
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func parseOrderToParam() {
        
    }
    //MARK:- Outlet Actions
    @IBAction func abtnCraft(_ sender: Any) {
        guard let order = self.order else {
            TSMessage.showNotification(in: self.navigationController!, title: "Lưu không thành công!", subtitle: "Đơn hàng này không tồn tại nên không thể lưu thành bản nháp, vui lòng nhập thông tin cho hoá đơn.", type: .error)
            return}
        order.code = order.code == nil ? "Draft-0" : order.code
        order.status = OrderStatus.Draft.rawValue
        order.created = order.created == nil ? Date().toString(dateFormat: "yyyy-MM-dd hh:mm:ss") : order.created
        guard var orders = Utils.shared.getDraftOrders() else {
            Utils.shared.saveDraftOrders(value: [order])
            TSMessage.showNotification(withTitle: "Lưu thành công", type: .success)
            return}
        if let index = orders.firstIndex(where: { (orderSaved) -> Bool in
            orderSaved.code == order.code
        }) {

            orders[index] = order
        }else {
            order.code = "Draft-\(orders.count)"
            orders.append(order)
        }
        Utils.shared.saveDraftOrders(value: orders)
        TSMessage.showNotification(withTitle: "Lưu thành công", type: .success)
        
    }
    @IBAction func abtnOrder(_ sender: Any) {
        let vc = DetailOrderController()
        vc.order = self.order
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension CartController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.order?.details?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! CartProductCell
        cell.tag = indexPath.row
        cell.setupDataSource(product: (self.order?.details?[indexPath.row])!)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10, height: 155)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
extension CartController: UISideMenuNavigationControllerDelegate {
    func sideMenuDidDisappear(menu: UISideMenuNavigationController, animated: Bool) {
        self.reloadAfterSelectedFromMultiCart()
    }
    
}
extension CartController:CellDelegate {
    func didEndEditing(textField: UITextField) {
        self.order?.details?[textField.tag].sizeWareHouse?.quantity = textField.text
        self.updateTotalPrice()
        guard let index = Utils.orders.index(where: { (order) -> Bool in
            self.order?.cname == order.cname
        }) else {return}
        Utils.orders[index] = self.order!
    }
    func didPressDelete(index: Int) {
        self.collectionView.performBatchUpdates({
            self.order?.details?.remove(at: index)
            self.collectionView.deleteItems(at: [IndexPath(row: index, section: 0)])
            self.collectionView.isUserInteractionEnabled = false
        }) { (bool) in
            self.collectionView.reloadData()
            self.collectionView.isUserInteractionEnabled = true

        }
        self.reloadAfterSelectedFromMultiCart()
        self.updateTotalPrice()
    }
    
}

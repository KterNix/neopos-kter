//
//  CartProductCell.swift
//  neoPOS
//
//  Created by mac on 8/24/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class CartProductCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var codeProduct: TitleLabel!
    @IBOutlet weak var nameProduct: RecommentLabel!
    @IBOutlet weak var textFieldQuantity: UITextField!
    @IBOutlet weak var priceSize: RecommentLabel!
    @IBOutlet weak var totalPriceSize: RecommentDiscountLabel!
    @IBOutlet weak var sizeName: UILabel!
    @IBOutlet weak var widthSizeName: NSLayoutConstraint!
    @IBOutlet weak var deleteButton: StyleDefault!
    @IBOutlet weak var minusQuantityButton: StyleDefault!
    @IBOutlet weak var addQuantityButton: StyleDefault!
    
    
    //MARK:- Declare
    var product:ProductModel?
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    //MARK:- SubFunction
    func setupUI() {
        self.sizeName.cornerRadius = 5
        self.sizeName.backgroundColor = .mainColor
        self.sizeName.textColor = .white
        self.sizeName.font = UIFont.recommentFont
        
        self.backgroundColor = .white
        
        self.textFieldQuantity.font = UIFont.recommentFont
        self.textFieldQuantity.text = "1"
        self.textFieldQuantity.keyboardType = .numberPad
        self.textFieldQuantity.delegate = self
        
        self.deleteButton.fontTitle = UIFont.recommentFont
        self.deleteButton.colorTitle = .red
        self.deleteButton.borderWidth = 1
        self.deleteButton.borderColor = .red

        self.minusQuantityButton.fontTitle = UIFont.recommentFont
        self.minusQuantityButton.borderColor =  .mainColor
        self.minusQuantityButton.borderWidth = 1
        
        self.addQuantityButton.fontTitle = UIFont.recommentFont
        self.addQuantityButton.borderColor =  .mainColor
        self.addQuantityButton.borderWidth = 1
    }
    
    func setupDataSource(product:ProductModel) {
        self.textFieldQuantity.tag = self.tag
        self.product = product
        self.sizeName.text = "Size: " + (product.sizeWareHouse?.title).asString
        nameProduct.text = product.title.asString
        codeProduct.text = product.code.asString
        widthSizeName.constant = sizeName.intrinsicContentSize.width + 30
        updatePrice()
    }
    func updatePrice() {
        let price = (product?.sizeWareHouse?.pricekm).asDouble == 0 ? (product?.sizeWareHouse?.price).asDouble : (product?.sizeWareHouse?.pricekm).asDouble
//        let stringPrice = NSMutableAttributedString(string: "Đơn giá: " + price.withCommas())
//        stringPrice.addAttribute(.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 0, length: 8))
        priceSize.text = price.withCommas()
        self.textFieldQuantity.text = self.product?.sizeWareHouse?.quantity ?? 1.description
        let quantityFormTextFied = self.textFieldQuantity.text.asDouble
        totalPriceSize.text = (price * (quantityFormTextFied <= 0 ? 1 : quantityFormTextFied)).withCommas()
    }
    //MARK:- Outlet Actions
    @IBAction func aDeleteButton(_ sender: Any) {
        self.delegate?.didPressDelete!(index: self.tag)
    }
    @IBAction func aMinusQuantityButton(_ sender: Any) {
        guard let quantity = self.product?.sizeWareHouse?.quantity else {return}
        guard var number = Int(quantity) else {return}
        number -= 1
        if number <= 0 {
            return
        }
        self.product?.sizeWareHouse?.quantity = number.description
        self.updatePrice()
        self.delegate?.didEndEditing!(textField: self.textFieldQuantity)
    }
    @IBAction func aAddQuantityButton(_ sender: Any) {
        guard let quantity = self.product?.sizeWareHouse?.quantity else {return}
        guard var number = Int(quantity) else {return}
        number += 1
        self.product?.sizeWareHouse?.quantity = number.description
        self.updatePrice()
        self.delegate?.didEndEditing!(textField: self.textFieldQuantity)

    }
    
}
extension CartProductCell:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text.asDouble >= 0 {
            self.product?.sizeWareHouse?.quantity = textField.text
            self.delegate?.didEndEditing!(textField: textField)
        }else {
            textField.text = 1.description
        }
        updatePrice()
    }
}

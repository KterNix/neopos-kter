//
//  MultiCartController.swift
//  neoPOS
//
//  Created by mac on 8/30/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import TSMessages
import SideMenu
private let idBasicInforCell = "BasicInforCartCell"

class MultiCartController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var tableViewMultiCart: UITableView!

    
    //MARK:- Declare
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupUIAfterSyncData()
    }
    //MARK:- SubFunction
    func setupUI() {
        self.title = "Khách hàng"
        self.createBarItem( title: "Thêm mới", imageName: nil, selector: #selector(self.didPressAddNewOrder), fontSize: 13)
        
    }
    func setupUIAfterSyncData()  {
        self.tableViewMultiCart.isHidden = Utils.orders.isEmpty ? true : false
        guard !Utils.orders.isEmpty else {return}
        if let indexOrderSelected = Utils.indexOrderSelected {
            self.tableViewMultiCart.selectRow(at: IndexPath(row:indexOrderSelected , section: 0), animated: true, scrollPosition: .top)
        }
    }
    
    func setupDataSource() {
       self.tableViewMultiCart.register(UINib(nibName: idBasicInforCell, bundle: nil), forCellReuseIdentifier: idBasicInforCell)
        self.tableViewMultiCart.delegate = self
        self.tableViewMultiCart.dataSource = self
    }
    @objc func didPressAddNewOrder() {
//        DispatchQueue.main.async {
//            Utils.alertWithTextfield(titile: "Vui lòng nhập tên khách", message: "", placeholder1: "Tên", placeholder2: nil, viewController: self) { (texts) in
//                guard let texts = texts else {return}
//                guard let text = texts.first, text != nil, !(text?.isEmpty)! else {return}
//                guard  Utils.orders.index(where: { (order) -> Bool in order.cname == text })  !=  nil else {
        
                    let order = OrderModel()
                    var customer =  CustomerModel()
                    customer.fullname = "Không có tên"
                    order.customer = customer
                    Utils.orders.append(order)
                    self.tableViewMultiCart.isHidden = false
                    DispatchQueue.main.async {
                        self.tableViewMultiCart.beginUpdates()
                        self.tableViewMultiCart.insertRows(at: [IndexPath(row: Utils.orders.count - 1, section: 0)], with: .automatic)
                        self.tableViewMultiCart.endUpdates()
                        Utils.indexOrderSelected = Utils.orders.count - 1
                        self.dismiss(animated: true, completion: nil)
                        
                    }
//
//                    return }
//                TSMessage.showNotification(in: self, title: "Trùng tên với khách hàng trước đó.", subtitle: "Vui lòng nhập tên khác.", type: .error)
//
//            }
//        }
    }
    //MARK:- Outlet Actions
}
extension  MultiCartController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Utils.orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: idBasicInforCell) else {return UITableViewCell()}
        guard let cellInfor = cell as? BasicInforCartCell else {return UITableViewCell() }
        cellInfor.setupDataSource(order: Utils.orders[indexPath.row], index: indexPath.row + 1)
        return cellInfor
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Utils.indexOrderSelected = indexPath.row
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isSelected {
            (cell as! BasicInforCartCell).nameCustomer.textColor = .mainColor
        }else {
            (cell as! BasicInforCartCell).nameCustomer.textColor = .black
        }
    }
}


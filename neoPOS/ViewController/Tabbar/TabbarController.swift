//
//  TabbarController.swift
//  neoPOS
//
//  Created by mac on 8/22/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class TabbarController: UITabBarController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    lazy var dashBoardVC :UINavigationController = {
        let navi = DashboardController().createNaviControllerForTabbar(icon: "dashboard", title: "Dashboard")
        return navi
    }()
    lazy var profileVC:UINavigationController = {
        let navi = ProfileController().createNaviControllerForTabbar(icon: "profile", title: "Cá nhân")
        return navi
    }()
    lazy var ordersVC :UINavigationController = {
        let navi = OrdersController().createNaviControllerForTabbar(icon: "order", title: "Đơn hàng")
        return navi
    }()
    lazy var wareHouseVC:UINavigationController = {
        let navi = WareHouseController().createNaviControllerForTabbar(icon: "assets", title: "Kho")
        return navi
    }()
    lazy var expenseRevenueVC:UINavigationController = {
        let navi = PaymentNote_ReceiptViewController().createNaviControllerForTabbar(icon: "money", title: "Thu chi")
        return navi
    }()
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewControllers = [dashBoardVC,ordersVC,expenseRevenueVC,wareHouseVC,profileVC]
        self.setupUI()
        self.setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.tabBar.tintColor = .secondColor
//        self.modalTransitionStyle = .partialCurl
        self.delegate = self
        self.lightStatusBar()
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions
    
    
    
}
extension TabbarController: UITabBarControllerDelegate  {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        guard let fromView = selectedViewController?.view, let toView = viewController.view else {
            return false // Make sure you want this as false
        }
        
        if fromView != toView {
            UIView.transition(from: fromView, to: toView, duration: 0.3, options: [.transitionCrossDissolve], completion: nil)
        }
        
        return true
    }
}

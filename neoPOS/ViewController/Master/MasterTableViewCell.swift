//
//  MasterTableViewCell.swift
//  neoBMSCare
//
//  Created by mac on 7/10/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
@objc public protocol CellDelegate:class {
    @objc optional func didSelect(tag:Int, indexSelected:IndexPath)
    @objc optional func didEndEditing(textField:UITextField)
    @objc optional func didPressDelete(index: Int)
    @objc optional func didDeselect(tag:Int, indexSelected:IndexPath)

}
class MasterTableViewCell: UITableViewCell {
    weak var delegate:CellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}

//
//  MasterFormController.swift
//  neoBMSCare
//
//  Created by mac on 7/10/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka
class MasterFormController: FormViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDefault()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupDataForViewWillAppear()
    }
    func setupDataForViewWillAppear() {
        
    }
    func setDefault() {
        
        LabelRow.defaultCellUpdate = { cell, row in
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.font = UIFont.textFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
            
        }
        SwitchRow.defaultCellUpdate = { cell, row in
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.font = UIFont.textFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
            cell.switchControl.onTintColor = .mainColor
            cell.switchControl.thumbTintColor = .orange
        }
        TextRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textField.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }
        PasswordRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textField.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }
        ListCheckRow<String>.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .black
            cell.tintColor = .mainColor
        }
        TextAreaRow.defaultCellUpdate = { cell, row in
            cell.textView.font = UIFont.recommentFont
            cell.textView.textColor = .black
        }
        ButtonRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .mainColor
        }
        PushRow<String>.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }
        DatePickerRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }

        PickerInlineRow<String>.defaultCellUpdate = {cell,row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }
        DecimalRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.textField?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.textField?.textColor = .black
        }
        PickerInputRow<String>.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }
        DateInlineRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }
        TimeRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }
        DateTimeRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }
        IntRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
            cell.textField.font = UIFont.recommentFont
        }
        PhoneRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
            cell.textField.font = UIFont.recommentFont
        }
        EmailRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
            cell.textField.font = UIFont.recommentFont
        }
        DateTimeInlineRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.textFont
            cell.detailTextLabel?.font = UIFont.recommentFont
            cell.textLabel?.textColor = .lightGray
            cell.detailTextLabel?.textColor = .black
        }
    }
    func setUpForm() {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

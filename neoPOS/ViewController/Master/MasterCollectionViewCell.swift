//
//  MasterCollectionViewCell.swift
//  neoBMSCare
//
//  Created by mac on 7/10/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class MasterCollectionViewCell: UICollectionViewCell {
    weak var delegate:CellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        setDefault()
    }
    func setDefault() {
        self.layer.cornerRadius = 5
    }
    
}

//
//  SizesPopupController.swift
//  neoPOS
//
//  Created by mac on 8/25/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import ObjectMapper
private let idCell = "SizeSelectionCell"

class SizesPopupController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var collectionSize: UICollectionView!
    
    
    //MARK:- Declare
    var sizes : [SizeProdcutInWareHouse]?
    var product:WareHouseProductModel?
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.collectionSize.backgroundColor = .groupTableViewBackground
    }
    func getData() {
        RequestHelper.shared.getProductInWareHouse(product: product, vc: self) { (dic, error) in
            guard let newDic = dic else {return}
            guard let sizes = Mapper<SizeProdcutInWareHouse>().mapArray(JSONObject: newDic["warehouses"]) , !sizes.isEmpty else {
                let label = TitleLabel(frame: self.view.bounds)
                label.text = "Sản phẩm này chưa có size vui lòng qua màn hình chi tiết sản phẩm để thêm size."
                label.numberOfLines = 4
                label.textAlignment = .center
                self.view.addSubview(label)
                return}
            
            self.sizes = sizes
            self.setupDataSource()
        }
    }
    func setupDataSource() {
        self.collectionSize.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionSize.delegate = self
        self.collectionSize.dataSource = self
    }
    
    //MARK:- Outlet Actions
}
extension SizesPopupController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sizes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! SizeSelectionCell
        cell.setupDataSource(size: (sizes?[indexPath.row])!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10, height: 170)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


//
//  SizeSelectionCell.swift
//  neoPOS
//
//  Created by mac on 8/25/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class SizeSelectionCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var sizeName: TitleLabel!
    @IBOutlet weak var quantitySizes: NormalLabel!
    @IBOutlet weak var priceSize: RecommentLabel!
    @IBOutlet weak var discountSize: RecommentDiscountLabel!
    @IBOutlet weak var btnMinus:  StyleDefault!
    @IBOutlet weak var textfieldQuantityChoice: UITextField!
    @IBOutlet weak var btnAdd: StyleDefault!
    
    
    //MARK:- Declare
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.textfieldQuantityChoice.font = UIFont.recommentFont
        self.textfieldQuantityChoice.text = "0"
        self.textfieldQuantityChoice.keyboardType = .numberPad
        self.textfieldQuantityChoice.textAlignment = .center
        self.backgroundColor = .white
        self.btnAdd.setAttributedTitle(NSAttributedString(string: "+", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 30, weight: .thin)]), for: .normal)
        self.btnMinus.setAttributedTitle(NSAttributedString(string: "−", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 30, weight: .thin)]), for: .normal)
    }
    func setupDataSource(size:SizeProdcutInWareHouse) {
        self.sizeName.text = "Size: " + size.title.asString
        self.quantitySizes.text = "Số lượng tồn: " + size.quantity.asString
        self.priceSize.text =  "Giá bán: " + size.price.asDouble.withCommas()
        self.discountSize.text = "Giá KM: " + size.pricekm.asDouble.withCommas()
    }
    
    //MARK:- Outlet Actions

}

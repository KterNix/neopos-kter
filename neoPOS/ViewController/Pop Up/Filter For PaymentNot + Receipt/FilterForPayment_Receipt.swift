//
//  FilterForPayment_Receipt.swift
//  neoPOS
//
//  Created by mac on 9/7/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
protocol FilterForPayment_ReceiptDelegate:class {
    func didPressChangeTypeDate(fromdate:Date?,todate:Date?,type:TypeDateToSelect)
}
class FilterForPayment_Receipt: MasterFormController {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    var dateSelected:String?
    var fromdate : Date?
    var todate : Date?
    weak var delegate:FilterForPayment_ReceiptDelegate?
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpForm()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
            +++ Section("Chọn nhanh")
            <<< ButtonRow(){
                $0.title = "Ngày hôm nay"
                }.onCellSelection({ (cell, row) in
                    self.didPressQuickAction(fromdate: Date(), todate: Date(), type: .Today)
                })

            <<< ButtonRow(){
                $0.title = "Ngày hôm qua"
                }.onCellSelection({ (cell, row) in
                    let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
                    self.didPressQuickAction(fromdate: yesterday, todate: yesterday, type: .Yesterday)

                })

            <<< ButtonRow(){
                $0.title = "Tuần này"
                }.onCellSelection({ (cell, row) in
                    let daysOfWeek = self.getDaysWith(type: .ThisWeek)
                    self.didPressQuickAction(fromdate: daysOfWeek.first!, todate: daysOfWeek.last!, type: .ThisWeek)

                })

            <<< ButtonRow(){
                $0.title = "Tháng này"
                }.onCellSelection({ (cell, row) in
                    let daysOfMonth = self.getDaysWith(type: .ThisMonth)
                    self.didPressQuickAction(fromdate: daysOfMonth.first!, todate: daysOfMonth.last!, type: .ThisMonth)
                })

            <<< ButtonRow(){
                $0.title = "Tháng trước"
                }.onCellSelection({ (cell, row) in
                    let daysOfMonth = self.getDaysWith(type: .PreviousMonth)
                    self.didPressQuickAction(fromdate: daysOfMonth.first!, todate: daysOfMonth.last!, type: .PreviousMonth)
                    
                })
        +++ Section("hoặc")
            <<< DateInlineRow(){
                $0.title = "Từ ngày"
                $0.value = Date()
                }.cellUpdate({ (cell, row) in
                    self.fromdate = row.value
                })
            <<< DateInlineRow(){
                $0.title = "Đến ngày"
                $0.value = Date()
                }.cellUpdate({ (cell, row) in
                    self.todate = row.value
                })
    }
    func  getDaysWith(type:TypeDateToSelect) -> [Date] {
        let calendar = Calendar.current
        let today = Date()
        
        switch type {
        case .ThisWeek:
            let dayOfWeek = calendar.component(.weekday, from: today)
            let weekdays = calendar.range(of: .weekday, in: .weekOfYear, for: today)!
            let days = (weekdays.lowerBound ..< weekdays.upperBound)
                .compactMap { calendar.date(byAdding: .day, value: $0 - (dayOfWeek - 1), to: today) }
            return days
        case .ThisMonth:
            let todayDate = calendar.component(.day, from: today)
            let monthDays = calendar.range(of: .day, in: .month, for: today)!
            let days = (monthDays.lowerBound ..< monthDays.upperBound)
                .compactMap { calendar.date(byAdding: .day, value: $0 - (todayDate), to: today) }
            return days

        default:
            let dateOfPreviousMonth = calendar.date(byAdding: .month, value: -1, to: today)
            let todayDate = calendar.component(.day, from: today)
            let weekdays = calendar.range(of: .day, in: .month, for: dateOfPreviousMonth!)!
            let days = (weekdays.lowerBound ..< weekdays.upperBound)
                .compactMap { calendar.date(byAdding: .day, value: $0 - todayDate, to: dateOfPreviousMonth!) }
            return days

        }
        
        
    }
    //MARK:- SubFunction
    func didPressQuickAction(fromdate:Date?,todate:Date?,type:TypeDateToSelect) {
        self.fromdate = fromdate
        self.todate = todate
        self.delegate?.didPressChangeTypeDate(fromdate: self.fromdate, todate: self.todate ,type: type)
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Outlet Actions
}

//
//  PercentAndDiscountController.swift
//  neoPOS
//
//  Created by mac on 9/11/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
private let idCell = "TextCell"
@objc protocol PercentAndDiscountControllerDelegate:class{
    @objc optional func didPressDone(discount:Double)
}
class PercentAndDiscountController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var collectionViewPercent: UICollectionView!
    @IBOutlet weak var textFieldDiscountValue: UITextField!
    @IBOutlet weak var btnDone: StyleDefault!
    
    
    //MARK:- Declare
    var total:Double?
    var indexSelected:Int?
    var discount:Double?
    weak var delegate:PercentAndDiscountControllerDelegate?
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        calculationDiscount()
    }
    //MARK:- SubFunction
    func setupUI() {
        self.btnDone.borderColor = .mainColor
        self.btnDone.borderWidth = 0.5
        self.btnDone.cornerRadius = DefaultSetting.radius
        self.textFieldDiscountValue.font = UIFont.recommentDiscountFont
        self.textFieldDiscountValue.textColor = .red
        self.textFieldDiscountValue.delegate = self
        self.textFieldDiscountValue.addTarget(self, action: #selector(self.didChangeValue(textField:)), for: .editingChanged)
        self.textFieldDiscountValue.keyboardType = .numbersAndPunctuation
    }
    func setupDataSource() {
        collectionViewPercent.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        collectionViewPercent.delegate = self
        collectionViewPercent.dataSource = self
    }
    func calculationDiscount() {
        guard let indexSelected = self.indexSelected else {
            self.textFieldDiscountValue.text = self.discount?.withCommas()
            return}
        let percent = Double((indexSelected + 1) * 5)
        let discount = ((self.total ?? 0)  * percent)/100
        self.discount = discount
        self.textFieldDiscountValue.text = discount.withCommas()
    }
    @objc func didChangeValue(textField:UITextField) {
        self.discount = textField.text.asDouble
        if let indexSelected = self.indexSelected {
            self.collectionViewPercent.deselectItem(at: IndexPath(item: indexSelected, section: 0), animated: true)
        }
        self.indexSelected = nil
    }
    //MARK:- Outlet Actions
    @IBAction func abtnDone(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        guard let discount = self.discount else {return}
        self.delegate?.didPressDone!(discount: discount)
    }
    
}
extension PercentAndDiscountController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! TextCell
        cell.labelText.text = "-" + (5 * (indexPath.row + 1)).description + "%"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexSelected == nil || indexPath.row != indexSelected {
            indexSelected = indexPath.row
        }else {
            indexSelected = nil
            collectionView.deselectItem(at: indexPath, animated: true)
            self.discount = nil
        }
        calculationDiscount()

        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.width -  20)/3, height: (collectionView.height - 20)/3)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
extension PercentAndDiscountController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = self.discount?.description
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = self.discount?.withCommas()
    }
    
}

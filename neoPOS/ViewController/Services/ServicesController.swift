//
//  ServicesController.swift
//  neoPOS
//
//  Created by mac on 8/27/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import FSCalendar
import ObjectMapper
private let idCell = "ServiceCell"
protocol ServicesControllerDelegate:class{
     func didSelect(services:[ServiceModel]?)
}
class ServicesController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var topCollectionViewContraint: NSLayoutConstraint!
    @IBOutlet weak var btnContinue: StyleDefault!
    @IBOutlet weak var bottomCollectionViewConstraint: NSLayoutConstraint!
    
    
    //MARK:- Declare
    var isPressedCalendar = false
    var services : [ServiceModel]?
    weak var delegate:ServicesControllerDelegate?
    var servicesSelected:[ServiceModel]?
    var typeClass:TypeList = .Multiselect
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getData()
        getServicesCate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.title = typeClass == .Multiselect ? "Chọn dịch vụ" : "Dịch vụ"
        self.bottomCollectionViewConstraint.constant = typeClass == .Multiselect ? 56 : 0
        setBackButton()
//        self.createBarItem( title: "Lịch", imageName: nil, selector: #selector(self.didPressCalendar), fontSize: 13)
        self.calendar.appearance.eventDefaultColor =  UIColor.mainColor
        self.calendar.appearance.titleWeekendColor = UIColor.red
        self.calendar.appearance.headerTitleColor = .mainColor
        self.calendar.appearance.selectionColor = .lightGray
        self.collectionView.backgroundColor = .groupTableViewBackground
        self.view.backgroundColor = .groupTableViewBackground
        self.btnContinue.cornerRadius = 5
        self.btnContinue.borderWidth = 0.5
        self.btnContinue.borderColor = .orange
        self.btnContinue.colorTitle = .orange
        if typeClass == .View {
            self.createBarItem( title: "+", imageName: nil, selector: #selector(self.didPressCreate), fontSize: 25)
        }
    }
    func setupDataSource() {
        self.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.allowsMultipleSelection = self.typeClass == .Multiselect ? true:false
        self.selectItemsBefore()
        
    }
    @objc func didPressCreate() {
        let vc = DetailServiceController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func didPressCalendar() {
        isPressedCalendar = !isPressedCalendar
        self.topCollectionViewContraint.constant = isPressedCalendar ? 200 : 0
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
        }) { (bool) in
            
        }
    }
    func getData() {
        RequestHelper.shared.getServices(vc: self) { (dic, error) in
            guard let newDic = dic else {return}
            guard let services =  Mapper<ServiceModel>().mapArray(JSONObject: newDic["services"]) else {return}
            self.services = services
            self.setupDataSource()
        }
    }
    func selectItemsBefore() {
        if let servicesChoiced = self.servicesSelected {
            if let services = self.services {
                for (index, model) in services.enumerated() {
                    if  servicesChoiced.index(where: { (serviceModel) -> Bool in
                        if serviceModel.scode == nil {
                            return model.code == serviceModel.code

                        }else {
                            return model.code == serviceModel.scode
                        }
                        
                    }) != nil{
                        self.collectionView.selectItem(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .centeredHorizontally)
                    }
                }
            }
            
        }
    }
    func getServicesCate() {
        RequestHelper.shared.getServicesCate(vc: self) { (dic, error) in
            guard let serviceCates = Mapper<ServiceCateModel>().mapArray(JSONObject: dic?["categories"]) else {return}
            Utils.serviceCategories =  serviceCates
        }
    }
    //MARK:- Outlet Actions
    @IBAction func abtnContinue(_ sender: Any) {
        guard let indexsSelected =  collectionView.indexPathsForSelectedItems else {return}
        let rows = indexsSelected.map{$0.row}
        var servicesChoiced = [ServiceModel]()
        for row in rows {
            var model = services![row]
            model.quantity = model.quantity ?? 1.description
            servicesChoiced.append(model)
        }
        self.delegate?.didSelect(services: servicesChoiced)
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension ServicesController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.services?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! ServiceCell
        let model = services![indexPath.row]
        cell.setupDataSource(service: model)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if typeClass != .Multiselect {
            collectionView.deselectItem(at: indexPath, animated: true)
            let vc = DetailServiceController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


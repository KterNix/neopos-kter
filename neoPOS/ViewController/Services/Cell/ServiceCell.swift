//
//  ServiceCell.swift
//  neoPOS
//
//  Created by mac on 8/27/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class ServiceCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var titleService: RecommentLabel!
    @IBOutlet weak var descriptionService: NormalLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.backgroundColor = .white
    }
    func setupDataSource(service: ServiceModel) {
        self.titleService.text = service.title.asString
        self.descriptionService.text = service.descriptionValue.asString
    }
    override var isSelected: Bool{
        didSet{
            if isSelected {
                self.backgroundColor = .mainColor
                self.titleService.textColor = .white
                self.descriptionService.textColor = .white
                return
            }
            self.backgroundColor = .white
            self.titleService.textColor = .black
            self.descriptionService.textColor = .lightGray
        }
        
    }
    //MARK:- Outlet Actions

}

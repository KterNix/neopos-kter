//
//  DetailServiceController.swift
//  neoPOS
//
//  Created by mac on 9/28/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
class DetailServiceController: MasterFormController {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    var service:ServiceModel?
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        setUpForm()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
        +++ Section("Chi tiết")
            <<< TextRow(){
                $0.title = "Mã"
        }
            <<< TextRow(){
                $0.title = "Tên"
        }
            <<< DecimalRow(){
                $0.title = "Giá"
        }
            <<< DecimalRow(){
                $0.title = "Giá khuyến mãi"
        }
            <<< TimeRow(){
                $0.title = "Thời gian xong"
        }
            <<< SwitchRow(){
                $0.title = "Chấp nhận thẻ thành viên?"
            }
            <<< PickerInlineRow<String>(){
                $0.title = "Thuộc danh mục"
                $0.options = Utils.serviceCategories.map{$0.title.asString}
            }
        +++ Section("Mô tả")
            <<< TextAreaRow(){
                $0.value = service?.descriptionValue
        }
            +++ Section("Ghi chú")
            <<< TextAreaRow(){
                $0.value = service?.note
        }
        +++ Section("Kích hoạt")
            <<< SwitchRow(){
                $0.title = "Khoá"
        }
            <<< SwitchRow(){
                $0.title = "Cho phép hiển thị?"
        }
        
    }
    //MARK:- SubFunction
    func setupUI() {
        self.title = service?.id == nil ? "Tạo dịch vụ": "Chi tiết dịch vụ"
        if service?.id != nil {
            self.createBarItem( title: "Tạo", imageName: nil, selector: #selector(self.didPressCreate), fontSize: 13)
        }else {
            self.createBarItem( title: "Cập nhật", imageName: nil, selector: #selector(self.didPressUpdate), fontSize: 13)
        }
        setBackButton()
    }
    func setupDataSource() {
        
    }
    @objc func didPressUpdate() {
        
    }
    @objc func didPressCreate() {
        
    }
    @objc func didPressDeleted() {
        
    }
    //MARK:- Outlet Actions

}

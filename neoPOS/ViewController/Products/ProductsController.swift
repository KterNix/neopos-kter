//
//  ProductsController.swift
//  neoPOS
//
//  Created by mac on 8/15/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//
import Foundation
import UIKit
import ObjectMapper
import UIScrollView_InfiniteScroll
import IQKeyboardManagerSwift
import TSMessages
private let idCell = "ProductCell"
protocol ProductsControllerDelegate:class{
    func didSelected(product:ProductModel)
}
class ProductsController: UIViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: SearchDefault!
    
    
    //MARK:- Declare
    var isPressRefesh = false
    weak var delegate:ProductsControllerDelegate?
    var textSearch :String?
    var pageFiltered = 0
    var page  = 0
    var productsFiltered:[ProductModel]?
    var sizesSelected:[SizeProdcutInWareHouse?]?
    var type = ProductsViewControllerType.View
    var order :OrderModel?
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.mainColor
        
        return refreshControl
    }()
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        getData()
        getCategories()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        parseForFilter()
        self.collectionView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        Utils.productsHome.removeAll()
        print("\(self) deinit =================")

    }
    
    //MARK:- SubFunction
    func setupUI() {
        let title = self.type == .View ? "Danh sách sản phẩm" : "Chọn sản phẩm"
        self.title = title
        
        if self.type == .View {
            self.createBarItem(title: "+", imageName: nil, selector: #selector(self.didPressMore), fontSize: 25)
        }else {
            
        }
        
        self.setBackButton()
        self.productsFiltered = nil
        self.collectionView.addInfiniteScroll { [weak self] (collectionView) in
            self?.collectionView.isUserInteractionEnabled = false
            guard self?.productsFiltered == nil else {
                self?.getFiltered()
                collectionView.finishInfiniteScroll()
                return}
            self?.getData()
        }
        setSearchBarUI()
        addRefreshControl()
     }
    func setSearchBarUI() {
        searchBar.placeholder = "Tên sản phẩm"
        searchBar.delegate = self
    }
    
    @objc func didPressDoneSearch() {
        guard let text = searchBar.text else {return}
        self.productsFiltered = Utils.productsHome.filter({ (product) -> Bool in
            ((product.title?.range(of: text, options: [.caseInsensitive, .diacriticInsensitive], range: nil, locale: nil)) != nil)
        })
        self.collectionView.reloadData()
    }
    func setupDataSource() {
        self.setUpForOrder()
        self.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    @objc func didPressMore() {
        let vc = DetailProductController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func checkDataForEmptyView() {
        guard self.isSearchStatus() else {
            
            guard Utils.productsHome.count == 0 else {
                self.removeEmptyView(onItem: self.collectionView)
                return}
            
            self.addViewEmpty()
            return}
        
        guard let productsFilteredCount = self.productsFiltered?.count  else {
            self.addEmptyView()
            return}
        
        guard productsFilteredCount == 0 else {
            self.removeEmptyView(onItem: self.collectionView)
            return }
        
        self.addEmptyView()
    }
    
    func isSearchStatus() -> Bool {
        return self.textSearch != nil
    }
    
    func addEmptyView() {
        self.createEmptyView(onItem: self.collectionView) {
            guard self.isSearchStatus() else {
                self.page = 0
                self.getData()
                return}
            self.pageFiltered = 0
            self.getFiltered()
        }
    }
    func getData() {
        guard Utils.getCurrentToken() != nil else {return}
        RequestHelper.shared.getProducts(page: page.description, keyWord: nil, vc: self) { [weak self] (dic, error) in
            guard let newDic = dic else {
                self?.completedProgressingGetData()
                return}
            guard let products = Mapper<ProductModel>().mapArray(JSONObject: newDic["products"]) else {
                self?.completedProgressingGetData()
                return}
            
            guard !(self?.isPressRefesh ?? true) else {
                Utils.productsHome = products
                self?.collectionView.reloadData()
                self?.isPressRefesh = false
                self?.refreshControl.endRefreshing()
                return}
            let oldCount = Utils.productsHome.count
            Utils.productsHome.append(contentsOf: products)
            let newCount = Utils.productsHome.count
            self?.checkDataForEmptyView()
            self?.page += 1
            self?.insertIndexPaths(oldCount: oldCount, newCount: newCount)
            
        }
    }
    func insertIndexPaths(oldCount:Int, newCount:Int) {
        if newCount > oldCount {
            let range = newCount - oldCount
            var indexs = [IndexPath]()
            for i in 0..<range {
                let indexPath = IndexPath(item: oldCount + i, section: 0)
                indexs.append(indexPath)
            }
            self.collectionView.performBatchUpdates({
                self.collectionView.insertItems(at: indexs)
            }, completion: { (bool) in
                self.completedProgressingGetData()
            })
            return
        }else {
            self.completedProgressingGetData()
        }
    }
    func completedProgressingGetData() {
        self.collectionView.isUserInteractionEnabled = true
        self.collectionView.finishInfiniteScroll()
    }
    func addViewEmpty() {
        self.createEmptyView(onItem: self.collectionView) {
            guard  self.textSearch  != nil else {
                self.getData()
                return}
            self.getFiltered()
        }
    }
    func setUpForOrder() {
        self.sizesSelected = self.order?.details?.map{$0.sizeWareHouse}
    }
    func parseForFilter() {
        guard self.productsFiltered != nil else {
            Utils.productsDidUpdated = nil
            return }
        guard let productUpdated = Utils.productsDidUpdated else {return}
        guard let index = self.productsFiltered?.index(where: { (product) -> Bool in
                productUpdated.id == product.id
            }) else {return}
            self.productsFiltered?[index] = productUpdated
        Utils.productsDidUpdated = nil
    }
    func addRefreshControl() {
        self.collectionView.addSubview(self.refreshControl)
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        if self.productsFiltered != nil {
            self.pageFiltered = 0
            self.getFiltered()
        }else {
            self.page = 0
            self.getData()
        }
        isPressRefesh = true
    }
    func getFiltered() {
        RequestHelper.shared.getProducts(page: pageFiltered.description, keyWord: self.textSearch.asString, vc: self) { [weak self] (dic, error) in
            guard let newDic = dic else {
                self?.completedProgressingGetData()
                return}
            guard let products = Mapper<ProductModel>().mapArray(JSONObject: newDic["products"]) else {
                self?.completedProgressingGetData()
                return}
            self?.pageFiltered += 1
            let oldCount = self?.productsFiltered?.count
            guard !(self?.isPressRefesh)! else {
                self?.productsFiltered = products
                let newCount = self?.productsFiltered?.count
                self?.insertIndexPaths(oldCount: oldCount ?? 0 , newCount: newCount ?? 0)
                self?.isPressRefesh = false
                self?.refreshControl.endRefreshing()
                
                return}
            self?.productsFiltered == nil ? self?.productsFiltered =  products : self?.productsFiltered?.append(contentsOf: products)
            let newCount = self?.productsFiltered?.count
            self?.insertIndexPaths(oldCount: oldCount ?? 0 , newCount: newCount ?? 0)
            self?.checkDataForEmptyView()

        }
    }
    func getCategories() {
        RequestHelper.shared.getCategories(vc: self) { (dic, error) in
            guard let categories = Mapper<ProductCategoryModel>().mapArray(JSONObject: dic?["categories"]) else {return}
            Utils.productCategories = categories
        }
    }
    //MARK:- Outlet Actions

}
extension ProductsController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let models = self.isSearchStatus() ? self.productsFiltered : Utils.productsHome
        return models?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as? ProductCell else {return
            UICollectionViewCell()
        }
        let models = self.isSearchStatus() ? self.productsFiltered : Utils.productsHome
        if self.type == .Order {
            cell.hiddenForOrder()
            cell.tag = indexPath.row
            cell.delegate = self
        }
        let model = models![indexPath.row]
        cell.setupDataSource(product:model)
        self.sizesSelected?.forEach({ (size) in
            if size?.pid == model.id {
                guard let indexSizeInProduct = model.sizes?.firstIndex(where: { (sizeInProduct) -> Bool in
                    guard let sizeId = size?.sizeid else {
                        return sizeInProduct.id == size?.id
                    }
                    return sizeInProduct.id == sizeId
                }) else {return}
                cell.collectionSizes.selectItem(at: IndexPath(row: indexSizeInProduct, section: 0), animated: false, scrollPosition: .centeredVertically)
            }
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.dismissKeyboard()
        }
        let vc = DetailProductController()
        let models = self.isSearchStatus() ? self.productsFiltered : Utils.productsHome
        vc.product = models?[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height:CGFloat = self.type == .View ? 100 : 160
        return CGSize(width: collectionView.width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let magrin:CGFloat = 0
        return UIEdgeInsets.init(top: magrin, left: magrin, bottom: magrin, right: magrin)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0//5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0// 5
    }
    
}
extension ProductsController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
            self.dismissKeyboard()
            searchBar.text = nil
            self.productsFiltered = nil
            self.pageFiltered = 0
            self.textSearch = nil
            self.collectionView.reloadData()
            self.checkDataForEmptyView()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.dismissKeyboard()
        self.pageFiltered = 0
        self.productsFiltered?.removeAll()
        guard let text = searchBar.text , !text.isEmpty else {
            self.productsFiltered = nil
            self.collectionView.reloadData()
            return}
        self.textSearch = text
        self.collectionView.reloadData()
        self.getFiltered()
    }

}
extension ProductsController:CellDelegate{
    func didDeselect(tag: Int, indexSelected: IndexPath) {
        let product = self.productsFiltered == nil ? Utils.productsHome[tag] : self.productsFiltered![tag]
        guard let indexRemove = self.order?.details?.firstIndex(where: { (productSelected) -> Bool in
            guard productSelected.sizeWareHouse?.sizeid != nil  else {
                return product.sizeWareHouse?.id == productSelected.sizeWareHouse?.id
            }
            return product.sizeWareHouse?.id == productSelected.sizeWareHouse?.sizeid
        }) else {return}
        self.order?.details?.remove(at: indexRemove)
    }
    func didSelect(tag: Int, indexSelected: IndexPath) {
        var id = ""
        var size = SizeProductModel() // Lấy thông tin Size từ Server và kiểm tra để đưa vào orderProducts
        var product = ProductModel()
        
        if self.productsFiltered != nil {
            product = self.productsFiltered![tag]
        }else {
            product = Utils.productsHome[tag]
        }
        
        id = product.id.asString
        size = product.sizes![indexSelected.row]
        
        var productWareHouse = WareHouseProductModel()
        productWareHouse.id = id
        
        RequestHelper.shared.getProductInWareHouse(product: productWareHouse, vc: self) { [weak self](dic, error) in
            guard let newDic = dic else {return}
            guard let sizes = Mapper<SizeProdcutInWareHouse>().mapArray(JSONObject: newDic["warehouses"]) , !sizes.isEmpty else {
                TSMessage.showNotification(in: self, title: "Không có size!", subtitle: "Sản phẩm bạn vừa chọn không có trong kho.", type: .error, duration: 10, canBeDismissedByUser: true)
                self?.deselectSize(indexProduct: tag, indexPathDeselect: indexSelected)
                return}
            let sizeWares = sizes.filter({ (sizeWareHouse) -> Bool in
                sizeWareHouse.sizeid == size.id
            })
            if (sizeWares.first?.quantity).asInt <= 0 {
                TSMessage.showNotification(in: self, title: "Số lượng sản phẩm trong kho đã hết vui lòng chọn sản phẩm hoặc size khác.", subtitle: "", type: .error, duration: 10, canBeDismissedByUser: true)
                self?.deselectSize(indexProduct: tag, indexPathDeselect: indexSelected)
                return
            }
            self?.sizesSelected == nil ? self?.sizesSelected = [sizeWares.first] : self?.sizesSelected?.append(product.sizeWareHouse)

            product.sizeWareHouse = sizeWares.first
            
            if (product.sizeWareHouse?.pricekm).asDouble != 0  {
                let price = product.sizeWareHouse?.pricekm
                product.sizeWareHouse?.price = price
            }
            
            product.sizeWareHouse?.pricekm = nil
            product.sizeWareHouse?.quantity = "1"
            if self?.order?.details == nil  { self?.order?.details = [product] }
            self?.order?.details?.append(product)
            self?.delegate?.didSelected(product: product)
        }
    }
    func deselectSize(indexProduct:Int, indexPathDeselect:IndexPath) {
        guard let cell = self.collectionView.cellForItem(at: IndexPath(row: indexProduct, section: 0)) as? ProductCell else {return}
        guard  cell.collectionSizes.cellForItem(at: indexPathDeselect)  != nil else {return}
        cell.collectionSizes.deselectItem(at: indexPathDeselect, animated: true)
    }
}

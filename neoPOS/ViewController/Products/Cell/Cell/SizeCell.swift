//
//  SizeCell.swift
//  neoPOS
//
//  Created by mac on 8/28/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class SizeCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var sizeName: TitleLabel!
    @IBOutlet weak var sizeQuantity: NormalLabel!
    @IBOutlet weak var sizePrice: RecommentDiscountLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        sizeQuantity.isHidden = true
        sizePrice.isHidden = true
        self.backgroundColor = .white
        self.dropShadow()
    }
    
    func setupDataSource(size:SizeProductModel) {
        self.sizeName.text = size.title.asString
    }
    override var isSelected: Bool{
        didSet{
            if isSelected {
                self.sizeName.textColor = .white
                self.backgroundColor = .mainColor
            }else {
                self.sizeName.textColor = .mainColor
                self.backgroundColor = .white
            }
        }
    }
    //MARK:- Outlet Actions

}

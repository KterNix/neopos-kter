//
//  ProductCell.swift
//  neoPOS
//
//  Created by mac on 8/15/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
private let idCell = "SizeCell"

class ProductCell: MasterCollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var nameProduct: RecommentLabel!
    @IBOutlet weak var codeProduct: NormalLabel!

    @IBOutlet weak var rightArrow: UIImageView!
    @IBOutlet weak var numberSizes: UILabel!
    @IBOutlet weak var collectionSizes: UICollectionView!
    
    @IBOutlet weak var emptyLabel: RecommentLabel!
    
    //MARK:- Declare
    var sizes:[SizeProductModel]?
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setUpCollectionView()

    }
    
    //MARK:- SubFunction
    func setUpCollectionView() {
        
        self.collectionSizes.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionSizes.delegate = self
        self.collectionSizes.dataSource = self
    }
    func setupUI() {
        rightArrow.setImageWith(tintColor: .groupTableViewBackground)
        self.emptyLabel.color = .lightGray
        self.collectionSizes.allowsMultipleSelection = true
    }
    func setupDataSource(product : ProductModel) {
        if let isUrl = product.image?.contains("http"){
            if isUrl {
                self.imageProduct.setImageWith(path: product.image.asString, imageType: .NORMAL_IMAGE)
            }else {
                self.imageProduct.setImageWith(text: product.image.asString)
            }
        }
        self.nameProduct.text = product.title.asString
        self.codeProduct.text = "Mã: \((product.code).asString)"
        self.numberSizes.text = (product.sizes?.count.description).asInt.description + " sizes"
        self.sizes = product.sizes
        if self.sizes != nil && !(sizes?.isEmpty)! {
            self.collectionSizes.isHidden = false
            self.collectionSizes.reloadData()

        }else {
            self.collectionSizes.isHidden = true
        }
    }
    func hiddenForOrder() {
        self.rightArrow.isHidden = true
    }
    //MARK:- Outlet Actions

}
extension ProductCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sizes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! SizeCell
        cell.setupDataSource(size: sizes![indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelect!(tag: self.tag, indexSelected: indexPath)
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        self.delegate?.didDeselect!(tag: self.tag, indexSelected: indexPath)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: collectionView.frame.height * 0.8)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

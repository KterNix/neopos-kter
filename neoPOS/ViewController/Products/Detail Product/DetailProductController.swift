//
//  DetailProductController.swift
//  neoPOS
//
//  Created by mac on 8/15/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
import TSMessages
import ObjectMapper
import SDWebImage
import PopupDialog
private let tagSectionSize = "size"
private let tagSectionFooterImage = "footerImage"
class DetailProductController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var product:ProductModel?
    var urlImageCache:String?
    var categories = [String]()
    var sizesUpdateVC : SizesUpdateController?

    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSourceBeforeSetupForm()
        setUpForm()
        setupUI()
        setupDataSource()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
        +++ Section("Thông tin")
            <<< TextRow(){
                $0.title = "Tên"
                $0.value = product?.title
                }.cellUpdate({ [weak self ](cell, row) in
                    self?.product?.title = row.value
                    self?.initModel()
                })
            <<< TextRow(){
                $0.title = "Mã"
                $0.value = product?.code
                }.cellUpdate({[weak self] (cell, row) in
                    self?.product?.code = row.value
                    self?.initModel()

                })
            <<< IntRow(){
                $0.title = "Giá gốc"
                $0.value = (product?.pprice).asInt
                }.cellUpdate({ [weak self](cell, row) in
                    self?.product?.pprice = row.value?.description
                    self?.initModel()

                })
            <<< PickerInlineRow<String>{
                $0.title = "Danh mục"
                $0.options = categories
                guard let index = Utils.productCategories.index(where: { (cate) -> Bool in
                    cate.id == self.product?.pcateid
                }) else {return}
                $0.value = self.categories[index]
                }.cellUpdate({ [weak self](cell, row) in
                    guard let index = Utils.productCategories.index(where: { (cate) -> Bool in
                        cate.title == row.value.asString
                    }) else {return}
                    self?.product?.pcateid = Utils.productCategories[index].id
                })
        +++ Section("Mô tả")
            <<< TextAreaRow(){
                $0.value = product?.descriptionValue
                }.cellUpdate({[weak self] (cell, row) in
                    self?.product?.descriptionValue = row.value
                    self?.initModel()

                })
        +++ Section("Ghi chú")
            <<< TextAreaRow(){
                $0.value = product?.note
                
                }.cellUpdate({ [weak self](cell, row) in
                    self?.product?.note = row.value
                    self?.initModel()

                })
        +++ Section() {[weak self] section in
            section.tag = tagSectionSize
            var headerSectionSize = HeaderFooterView<HeaderTitileWithButton>(.nibFile(name: "HeaderTitileWithButton", bundle:nil))
            headerSectionSize.onSetupView = { [weak self] view , _ in
                view.title.text = "Size".uppercased()
                view.btnMore.title = "Thêm Size"
                let hide = self?.product?.sizes?.first?.id != nil ? false : true
                view.editButton(hide: hide)
                view.btnEdit.addTarget(self, action: #selector(self?.didPressEdit), for: .touchUpInside)
                view.btnMore.addTarget(self, action: #selector(self?.didPressAddMoreSize), for: .touchUpInside)
            }
            headerSectionSize.height = { 60 }
            section.header = headerSectionSize
//            section.header = self.product == nil ? nil : header
//            section.hidden = self.product == nil ? true : false
            section.evaluateHidden()
        }
            
        +++ Section("Hình ảnh"){[weak self] section in
            section.tag = tagSectionFooterImage
            var footerImages = HeaderFooterView<FooterImages>(.nibFile(name: "FooterImages", bundle:nil))
            footerImages.onSetupView = {[weak self] view, _ in
                view.image = self?.product?.image == nil ? self?.product?.path : self?.product?.image
                view.collectionView.reloadData()
                view.delegate = self
            }
            footerImages.height = { 200 }
            section.footer = footerImages
        }
        
    }
    //MARK:- SubFunction
    deinit {
        print(self.description + "deinit")
    }
    
    func initModel() {
        guard product == nil else {
            return
        }
        product = ProductModel()
        product?.applycard = "1"
        product?.status = "1"
        product?.pcateid = Utils.productCategories.first?.id
        product?.ishow = "1"
    }
    func pushToSizeVC(row:Int) {
        let vc = ProductSizeDetailController()
        vc.size = product?.sizes?[row]
        vc.product = product
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func setupUI() {
        self.title = "Chi tiết sản phẩm"
        setBackButton()
        
        if product == nil {
            createBarItem(title: "Tạo", imageName: nil, selector: #selector(self.didPressCreateProduct), fontSize: 13)
        }else {
            createBarItem(title: "Cập nhật", imageName: nil, selector: #selector(self.didPressUpdate), fontSize: 13)
        }
    }
    func getProduct() {
        
        guard let product = product else {return}
        RequestHelper.shared.getProduct(id: product.id.asString, vc: self) { [weak self] (dic, error) in
            guard let newDic = dic else {return}
            guard let product = Mapper<ProductModel>().map(JSONObject: newDic["product"]) else {return}
            self?.product = product
            self?.setupDataSource()
        }
    }
    func setupDataSourceBeforeSetupForm() {
        self.categories = Utils.productCategories.map{$0.title.asString}
    }
    func setupDataSource() {
        guard let sizeCount = product?.sizes?.count else {return}
        for i in 0..<sizeCount {
            let name = (product?.sizes?[i].title).asString
            self.addSizeForSection(name: name)
        }
    }
    @objc func didPressEdit() {
        sizesUpdateVC = SizesUpdateController()
        sizesUpdateVC?.sizes = self.product?.sizes
        let cancelButton = CancelButton(title: "Huỷ") {
        }
        cancelButton.setTitleColor(.lightGray, for: .normal)
        cancelButton.backgroundColor = .groupTableViewBackground
        let okButton = DefaultButton(title: "Cập nhật") {
            guard let size = self.sizesUpdateVC?.detailSize.size else {return}
            guard let index = self.product?.sizes?.firstIndex(where: { (sizeInProduct) -> Bool in
                sizeInProduct.id == size.id
            }) else {return}
            self.product?.sizes?[index] = size
            self.sizesUpdateVC = nil
            self.didPressUpdate()
        }
        okButton.setTitleColor(.white, for: .normal)
        okButton.backgroundColor = .orange
        
        let popUp = PopupDialog(viewController: sizesUpdateVC ?? UIViewController())
        popUp.addButtons([cancelButton,okButton])
        popUp.buttonAlignment = .horizontal
        self.present(popUp, animated: true, completion: nil)
    }
    @objc func didPressCreateProduct() {
        guard let product = self.product else {return}
        RequestHelper.shared.postProduct(product: product, vc: self) { [weak self](dic, error) in
            guard  let newDic = dic else {return}
            guard let product = Mapper<ProductModel>().map(JSONObject: newDic["product"]) else {return}
            self?.product = product
            TSMessage.showNotification(withTitle: "Tạo (\((product.title).asString)) thành công.", type: .success)
            
            self?.form.sectionBy(tag: tagSectionSize)?.removeAll()
            self?.setupDataSource()
            self?.navigationItem.rightBarButtonItems = nil
            self?.setupUI()
            self?.form.sectionBy(tag: tagSectionFooterImage)?.reload()
        }
    }
    @objc func didPressUpdate() {
        RequestHelper.shared.putProduct(product: product!, vc: self) { [weak self](dic, error) in
            guard dic != nil else {return}
            TSMessage.showNotification(in: self, title: "Cập nhật sản phẩm \((self?.product?.title).asString) thành công.", subtitle: "", type: .success)
            SDImageCache.shared().removeImage(forKey: self?.urlImageCache, withCompletion: nil)
            Utils.productsDidUpdated = self?.product
            guard let index = Utils.productsHome.index(where: { [weak self] (product) -> Bool in
                product.id == self?.product?.id
            }) else {return}

            Utils.productsHome[index] = (self?.product)!
            self?.form.sectionBy(tag: tagSectionSize)?.reload()
            
        }
    }
    @objc func didPressAddMoreSize() {
        self.addSize()
    }
    func addSize() {
        self.initModel()

        Utils.alertWithTextfield(titile: "Tên size mới", message: "Vui lòng nhập tên size mới của sản phẩm này.", placeholder1: "Tên size", placeholder2: "Mã size", viewController: self) { [weak self] (string) in
            guard let name = string?[0], !name.isEmpty else {
                TSMessage.showNotification(withTitle: "Vui lòng nhập đủ thông tin.", type: .error)
                return}
            guard let code = string?[1], !code.isEmpty else {
                TSMessage.showNotification(withTitle: "Vui lòng nhập đủ thông tin.", type: .error)
                return }
            var size = SizeProductModel(JSON: ["code":code ,"title":name,"status":"1"])

            guard let idProduct = self?.product?.id else {
                let text = TextRow(){
                    $0.title = name
                    $0.disabled = true
                    }.onCellSelection({ (cell, row) in
                        TSMessage.showNotification(withTitle: "Để vào xem chi tiết size .Vui lòng nhập đủ thông tin và tạo sản phẩm trước.", type: .error)
                    })
                var section = self?.form.sectionBy(tag: tagSectionSize)
                section?.insert(text, at: 0)
               self?.product?.sizes == nil ? self?.product?.sizes = [size!] : self?.product?.sizes?.append(size!)

                return}
            size?.pid = idProduct
            RequestHelper.shared.postSize(size: size!, vc: self ?? UIViewController(), completetion: { [weak self](dic, error) in
                guard let newDic = dic else {return}
                guard let size = Mapper<SizeProductModel>().map(JSONObject: newDic["size"]) else {return}
                TSMessage.showNotification(withTitle: "Tạo size: \(name) thành công!", type: .success)
                self?.product?.sizes == nil ? self?.product?.sizes = [size] : self?.product?.sizes?.insert(size, at: 0)
                self?.addSizeForSection(name: name)

            })

        
        }
    }
    func addSizeForSection(name:String?) {
        let text = PushRow<String>(){
            $0.title = name
            $0.value = "Thêm vào kho"
            $0.disabled = true
            }.onCellSelection({ [weak self] (cell, row) in
                let index = self?.product?.sizes?.index(where: { (size) -> Bool in
                    row.title == size.title
                })
                self?.pushToSizeVC(row: index!)
            }).cellUpdate { (cell, row) in
                guard let rowIndex = row.indexPath?.row else {return}
                row.title = self.product?.sizes?[rowIndex].title
                cell.update()
        }
        var section = self.form.sectionBy(tag: tagSectionSize)
        section?.insert(text, at: 0)
        
    }
    
    func chooseCamera() {
        let imagePicker = UIImagePickerController()
        imagePicker.applyAttributedNAV()
        imagePicker.delegate = self
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.isEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            Utils.showAlertWithCompletion(title: "Lỗi", message: "Camera không khả dụng", viewController: self, completion: {

            })
        }
    }
    func choosePhoto() {
        let imagePicker = UIImagePickerController()
        imagePicker.applyAttributedNAV()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.isEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    //MARK:- Outlet Actions
}
extension DetailProductController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        self.dismiss(animated: true, completion: nil)
        var data = Data()
        let chooseImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        let width = chooseImage.size.width/chooseImage.size.height > 1 ? 400 : 300
        let height = chooseImage.size.width/chooseImage.size.height > 1 ? 300 : 400
        let imageResized = chooseImage.scaleImageToSize(size: CGSize(width: width, height: height))
        data = imageResized.jpegData(compressionQuality: 0.5)!
        let string = data.base64EncodedString(options: .lineLength64Characters)
        self.initModel()
        self.urlImageCache = self.product?.image
        self.product?.image = string
        self.form.sectionBy(tag: "footerImage")?.reload()
        
    }
    func removeExifData(data: NSData) -> NSData? {
        guard let source = CGImageSourceCreateWithData(data, nil) else {
            return nil
        }
        guard let type = CGImageSourceGetType(source) else {
            return nil
        }
        let count = CGImageSourceGetCount(source)
        let mutableData = NSMutableData(data: data as Data)
        guard let destination = CGImageDestinationCreateWithData(mutableData, type, count, nil) else {
            return nil
        }
        // Check the keys for what you need to remove
        // As per documentation, if you need a key removed, assign it kCFNull
        let removeExifProperties: NSDictionary = [kCGImagePropertyExifDictionary : kCFNull, kCGImagePropertyOrientation: kCFNull]

        for i in 0..<count {
            CGImageDestinationAddImageFromSource(destination, source, i, removeExifProperties)
        }

        guard CGImageDestinationFinalize(destination) else {
            return nil
        }

        return mutableData;
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
extension DetailProductController:FooterImagesDelegate {
    func didSelected(index:Int) {
        if index == 0 {
            Utils.alertWithAction(title: "Chọn nơi lấy hình ảnh", cancelTitle: "Huỷ", message: "", actionTitles: ["Thư viện","Máy ảnh"], actions: [{ [weak self] (alert) in
                self?.choosePhoto()
                
                },{ [weak self] (alert) in
                    self?.chooseCamera()
                    
                }], actionStyle: .default, viewController: self, style: .actionSheet)
        }
        
    }
}

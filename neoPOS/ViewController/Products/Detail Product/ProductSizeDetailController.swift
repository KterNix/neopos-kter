//
//  ProductSizeDetailController.swift
//  neoPOS
//
//  Created by mac on 8/15/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
import TSMessages
class ProductSizeDetailController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var size:SizeProductModel?
    var sizeProductWareHouse:SizeProdcutInWareHouse?
    var product:ProductModel?
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpForm()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
        +++ Section("Giá")
            <<< IntRow(){
                $0.title = "Số lượng"
                }.cellUpdate({ (cell, row) in
                    self.initModel()
                    self.sizeProductWareHouse?.quantity = row.value?.description
                })
            <<< IntRow(){
                $0.title = "Giá bán"
                }.cellUpdate({ (cell, row) in
                    self.initModel()

                    self.sizeProductWareHouse?.price = row.value?.description

                })
            <<< IntRow(){
                $0.title = "Giá khuyến mãi"
                }.cellUpdate({ (cell, row) in
                    self.initModel()

                    self.sizeProductWareHouse?.pricekm = row.value?.description

                })
            +++ Section("Ghi chú")
            
            
            <<< TextAreaRow().cellUpdate({ (cell, row) in
                self.initModel()

                self.sizeProductWareHouse?.note = row.value
            })
            <<< DateInlineRow(){
                $0.title = "Hết hạn"
            
                }.onCellSelection({ (cell, row) in
                    self.initModel()

                    self.sizeProductWareHouse?.expired = row.value?.toString(dateFormat: "yyyy-MM-dd")
                })
        
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.title = "Size: \((size?.title).asString)"
        setBackButton()
        createBarItem(title: "Đưa vào kho", imageName: nil, selector: #selector(self.didCreateProductInWareHouse), fontSize: 13)
    }
    func setupDataSource() {
        
    }
    @objc func didCreateProductInWareHouse() {
        guard var size = sizeProductWareHouse else{return}
        size.pid = self.product?.id
        size.sizeid = self.size?.id
        size.package = "L1"
        size.wid = "1"
    
        RequestHelper.shared.postAndPutProductInWareHouse(size: size, vc: self) { (dic, error) in
            guard dic != nil else {return}
            TSMessage.showNotification(in: self, title: "Đưa vào kho thành công.", subtitle: "", type: .success)
            
        }
    }
    func initModel() {
        guard sizeProductWareHouse == nil  else {return}
        sizeProductWareHouse = SizeProdcutInWareHouse()
    }
    //MARK:- Outlet Actions

}


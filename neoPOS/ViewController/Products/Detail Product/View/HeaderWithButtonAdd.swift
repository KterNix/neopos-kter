//
//  HeaderWithButtonAdd.swift
//  neoPOS
//
//  Created by mac on 8/15/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class HeaderWithButtonAdd: UIView {

    
    //MARK:- Outlet
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.title.textColor = UIColor.init(hexString: "#6e6e73")
        self.backgroundColor = .groupTableViewBackground
        self.btnAdd.borderColor = .mainColor
        self.btnAdd.borderWidth = 1
        self.btnAdd.makeCircle = true
        self.btnAdd.setTitleColor(UIColor.mainColor, for: .normal)
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}

//
//  ImageCell.swift
//  neoPOS
//
//  Created by mac on 8/15/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {

    
    //MARK:- Outlet
    
    @IBOutlet weak var image: UIImageView!
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        // Initialization code
    }
    //MARK:- SubFunction
    func setupUI() {
        self.backgroundColor = .white
        
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}

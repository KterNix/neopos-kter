//
//  SizesUpdateController.swift
//  neoPOS
//
//  Created by mac on 9/27/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
private let idCell = "TextCell"

class SizesUpdateController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var viewDetailSize: UIView!
    @IBOutlet weak var collectionSizes: UICollectionView!
    
    
    //MARK:- Declare
    var sizes:[SizeProductModel]?
    let detailSize = DetailSizeController()

    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        addChild(detailSize)
        detailSize.size = self.sizes?.first
        detailSize.view.frame = viewDetailSize.bounds
        viewDetailSize.addSubview(detailSize.view)
        detailSize.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        detailSize.didMove(toParent: self)
    }
    func setupDataSource() {
        self.collectionSizes.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionSizes.delegate = self
        self.collectionSizes.dataSource = self
        self.collectionSizes.selectItem(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .centeredHorizontally)
    }
    
    //MARK:- Outlet Actions

}
extension SizesUpdateController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sizes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! TextCell
        cell.labelText.text = sizes![indexPath.row].title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.detailSize.size = sizes![indexPath.row]
        self.detailSize.changeSize()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


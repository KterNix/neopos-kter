//
//  DetailSizeController.swift
//  neoPOS
//
//  Created by mac on 9/27/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
class DetailSizeController: MasterFormController {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    var size:SizeProductModel?
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setUpForm()
    }
    override func setUpForm() {
        form
        +++ Section("Thông tin chung")
            <<< TextRow(){
                $0.title = "Tên size"
                $0.value = size?.title
                }.cellUpdate({ (cell, row) in
                    self.size?.title = row.value
                })
            <<< TextRow(){
                $0.title = "Mã size"
                $0.value = size?.code
                }.cellUpdate({ (cell, row) in
                    self.size?.code = row.value
                })
        +++ Section("Trạng thái")
            <<< SwitchRow(){
                $0.title = "Kích hoạt?"
                $0.value = size?.status == 1.description ? true : false
                }.cellUpdate({ (cell, row) in
                    guard let value = row.value else {return}
                    self.size?.status = value ? 1.description : 0.description
                })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
//        self.tableView.backgroundColor = .secondColor
    }
    
    func changeSize() {
        form.removeAll()
        setUpForm()
    }
    
    //MARK:- Outlet Actions

}

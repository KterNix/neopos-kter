//
//  ProfileController.swift
//  neoPOS
//
//  Created by mac on 8/22/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import ObjectMapper
import Eureka
import TSMessages
class ProfileController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var user:UserModel?
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
            +++ Section("Thông tin")
            <<< TextRow(){
                $0.title = "Họ & Tên"
                $0.value = user?.fullname
                }.cellUpdate({ (cell, row) in
                    self.user?.fullname = row.value
                })
            <<< PhoneRow(){
                $0.title = "Số điện thoại"
                $0.value = user?.mobile
                }.cellUpdate({ (cell, row) in
                    self.user?.mobile = row.value
                })
//            <<< PickerInlineRow<String>(){
//                $0.title = "Giới tính"
//                $0.options = ["Nam","Nữ","Khác"]
//                $0.value = user.
//            }
            <<< TextRow(){
                $0.title = "Địa chỉ"
                $0.value = user?.address
                }.cellUpdate({ (cell, row) in
                    self.user?.address = row.value
                })
        +++ Section("Tài khoản")
            <<< EmailRow(){
                $0.title = "Email"
                $0.value = user?.email
                $0.add(rule: RuleEmail())
                }.cellUpdate({ (cell, row) in
                    self.user?.email = row.value
                })
            
            <<< PasswordRow() {
                $0.title = "Mật khẩu"
                $0.value = user?.password
                }.cellUpdate({ (cell, row) in
                    self.user?.password = row.value
                })
        +++ Section("Quản lý")
            <<< ButtonRow(){
                $0.title = "Thành viên"
                $0.cellStyle = .value1
                }.onCellSelection({ (cell, row) in
                    let vc = CustomersController()
                    vc.typeUser = .Member
                    vc.typeClass = .View
                    self.navigationController?.pushViewController(vc, animated: true)
                })
            <<< ButtonRow(){
                $0.title = "Sản phẩm"
                $0.cellStyle = .value1

                }.onCellSelection({ (cell, row) in
                    let vc = ProductsController()
                    self.navigationController?.pushViewController(vc, animated: true)
                })
            <<< ButtonRow(){
                $0.title = "Dịch vụ"
                $0.cellStyle = .value1
                }.onCellSelection({ (cell, row) in
                    let vc = ServicesController()
                    vc.typeClass = .View
                    self.navigationController?.pushViewController(vc, animated: true)
                })
            <<< ButtonRow(){
                $0.title = "Khách hàng"
                $0.cellStyle = .value1

                }.onCellSelection({ (cell, row) in
                    let vc = CustomersController()
                    vc.typeClass = .View
                    self.navigationController?.pushViewController(vc, animated: true)
                })
            <<< ButtonRow(){
                $0.title = "Báo cáo"
                $0.cellStyle = .value1
            }
//        +++ SelectableSection<ListCheckRow<String>>("Quyền hạn", selectionType: .multipleSelection)
//        let continents = ["Admin","Order & Task","Quản trị kho","Quản trị khách hàng","Quản lý thu chi"]
//        for option in continents {
//            form.last! <<< ListCheckRow<String>(option){ listRow in
//                listRow.title = option
//                listRow.selectableValue = option
//                listRow.value = option
////                listRow.
//            }
//        }
//        form
        +++ Section(){section in
            var header = HeaderFooterView<UIView>(.callback({
                let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                let button = StyleDefault(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                button.title = "Đăng xuất"
                button.cornerRadius = 5
                button.borderWidth = 0.5
                button.borderColor = .red
                button.colorTitle = .red
                button.center = view.center
                button.tag = 2
                view.addSubview(button)
                return view
            }))
            header.height = {50}
            header.onSetupView = {view, _ in
                guard let button = view.viewWithTag(2) as? UIButton else {return}
                button.addTarget(self, action: #selector(self.didPressSignOut), for: .touchUpInside)
            }
            section.header = header
        }
        
    }
    @objc func didPressSignOut() {
        Utils.alertWithAction(title: "Đăng xuất!",cancelTitle:"Huỷ", message: "Vui lòng nhấn OK để xác nhận bạn muốn đăng xuất.", actionTitles: ["OK"], actions: [{ (alert) in
            Utils.shared.changeRootView(LoginViewController()) { () -> (Void) in
                Utils.removeUser()
            }
            }],actionStyle: .destructive, viewController: self, style: .actionSheet)
        
    }
    //MARK:- SubFunction
    func setupUI() {
        self.title = "Cá nhân"
        self.createBarItem( title: "Cập nhật", imageName: nil, selector: #selector(self.didPressUpdate), fontSize: 13)
    }
    func setupDataSource() {
        guard let user = Utils.getCurrentUser() else {return}
        RequestHelper.shared.getMember(id: (user.id).asString, vc: self) { [weak self](dic, error) in
            guard let user = Mapper<UserModel>().map(JSONObject: dic?["member"]) else {
                self?.createEmptyView(onItem: (self?.view)!, didTryAgain: {
                    self?.setupDataSource()
                })
                return}
            self?.user = user
            self?.setUpForm()
        }
    }
    @objc func didPressUpdate() {
        guard let param = user?.toJSON() else {return}
        RequestHelper.shared.putUser(id: (self.user?.id).asString, param: param, vc: self) { (dic, error) in
            guard dic != nil  else {return}
            TSMessage.showNotification(withTitle: "Cập nhật thành công!", type: .success)
        }
    }
    
//    func parseInforDistrict() {
//        do {
//            if let file = Bundle.main.url(forResource: "quan_huyen", withExtension: "json") {
//                let data = try Data(contentsOf: file)
//                let json = try JSONSerialization.jsonObject(with: data, options: [])
//                if let object = json as? Any {
//                    guard let models = Mapper<ParentDistrict>().map(JSONObject: object) else { return }
//                } else {
//                }
//            } else {
//            }
//        } catch {
//        }
//    }
//    func parser(code:String) -> [String]  {
//        filterModelWithProvince.removeAll()
//        if code.isEmpty {
//            var listString = [String]()
//            let arrayData = Utils.districtModel?.district?.values
//            arrayData?.forEach({ (model) in
//                self.arrayModel.append(model)
//            })
//            districtsCode = arrayModel.map{$0.parentCode!}.unique
//            var array = [DistrictModel]()
//            districtsCode.forEach { (code) in
//                array = arrayModel.filter {$0.parentCode! == code }
//                filterModelWithProvince.append(array.first!)
//                self.valuesForProvince[code] = array.map{ $0.name!}
//            }
//            filterModelWithProvince.forEach({ (district) in
//                let province = district.path?.split(separator: ",").last
//                listString.append(String(province!))
//            })
//            return listString
//        }else {
//            filterModelWithProvince = arrayModel.filter{$0.parentCode == code}
//            return filterModelWithProvince.map{ $0.name!}
//        }
//    }
    //MARK:- Outlet Actions

}

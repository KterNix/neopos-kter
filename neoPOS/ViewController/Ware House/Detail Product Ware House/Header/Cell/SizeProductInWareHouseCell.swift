//
//  SizeProductInWareHouseCell.swift
//  neoPOS
//
//  Created by mac on 8/17/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class SizeProductInWareHouseCell: UICollectionViewCell {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    @IBOutlet weak var titleSize: RecommentLabel!
    @IBOutlet weak var expired: NormalLabel!
    @IBOutlet weak var priceSize: RecommentLabel!
    @IBOutlet weak var priceDiscount: RecommentLabel!
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        priceDiscount.color = .red
        backgroundColor = .white
        self.cornerRadius = DefaultSetting.radius

    }
    func setupDataSource(sizeProduct:SizeProdcutInWareHouse) {
        
        titleSize.text = sizeProduct.title.asString
        expired.text = "Hết hạn: " +  sizeProduct.expired.asString
        priceSize.text = "Giá bán: " + sizeProduct.price.asDouble.withCommas()
        let priceKM = sizeProduct.pricekm.asDouble == 0 ? "Không áp dụng." : sizeProduct.pricekm.asDouble.withCommas()
        priceDiscount.text = "Giá KM: " + priceKM

    }
    
    //MARK:- Outlet Actions

}

//
//  DetailHouseInforHeader.swift
//  neoBMSCare
//
//  Created by mac on 7/24/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class DetailHouseInforHeader: UIView {

    
    //MARK:- Outlet
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var widthButtonContraint: NSLayoutConstraint!
    
    
    //MARK:- Declare
    var isEnableResizingButton = false
    var pressMore = false
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
//        checkStatus()
    }
    //MARK:- SubFunction
    func setupUI() {
        self.title.textColor = UIColor.init(hexString: "#6e6e73")
        self.title.text = title.text?.uppercased()
    }
    func setupDataSource() {
        
    }
//    func checkStatus() {
//        guard isEnableResizingButton else {return}
//        widthButtonContraint.constant = pressMore ? 120 : 100
//        let text = pressMore ? "Thu nhỏ" : "Xem thêm"
//        UIView.animate(withDuration: 0.5, animations: {
//            self.layoutIfNeeded()
//        }) { (bool) in
//        }
//    }
    //MARK:- Outlet Actions
    @IBAction func abtnMore(_ sender: Any) {
        guard isEnableResizingButton else {return}
        pressMore = !pressMore
        
        
    }
}


//
//  DetailProductWareHouseController.swift
//  neoPOS
//
//  Created by mac on 8/16/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
import ObjectMapper
import TSMessages
private let idCell = "SizeProductInWareHouseCell"
class DetailProductWareHouseController: MasterFormController {

    
    //MARK:- Outlet
    
    
    //MARK:- Declare
    var sizeProducts: [SizeProdcutInWareHouse]?
    var sizeProductSelected: SizeProdcutInWareHouse?
    var product:WareHouseProductModel?
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
            +++ Section(){section in
                var header = HeaderFooterView<DetailHouseInforHeader>(.nibFile(name:"DetailHouseInforHeader",bundle:nil))
                header.onSetupView = {view, _ in
                    view.title.text = "Danh sách size".uppercased()
                    view.setupUI()
                    view.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
                    view.collectionView.delegate = self
                    view.collectionView.dataSource = self
                    view.collectionView.backgroundColor = .groupTableViewBackground
                }
                header.height = { 200 }
                section.header = header
            }
           addSectionSizeInfo()
            addSectionNote()
        

    }
    //MARK:- SubFunction
    func setupUI() {
        self.title = product?.title
        setBackButton()
        self.createBarItem(forLeft: false, title: "Cập nhật", imageName: nil, selector: #selector(self.didPressUpdate), fontSize: 13)
    }
    func setupDataSource() {
        RequestHelper.shared.getProductInWareHouse(product: product, vc: self) { (dic, error) in
            guard let newDic = dic else {return}
            guard let sizes = Mapper<SizeProdcutInWareHouse>().mapArray(JSONObject: newDic["warehouses"]) , !sizes.isEmpty else {
                let label = TitleLabel(frame: self.view.bounds)
                label.text = "Sản phẩm này chưa có size vui lòng qua màn hình chi tiết sản phẩm để thêm size."
                label.numberOfLines = 4
                label.textAlignment = .center
                self.view.addSubview(label)
                return}
            
            self.sizeProducts = sizes
            self.sizeProductSelected =  sizes.first
            self.setUpForm()
        }
    }
    @objc func didPressUpdate() {
        guard let sizeSelected = self.sizeProductSelected else {return}
        self.sizeProductSelected?.pwid = self.product?.pwid
        RequestHelper.shared.postAndPutProductInWareHouse(size: sizeSelected, vc: self) { (dic, error) in
            TSMessage.showNotification(in: self, title: "Cập nhật thành công", subtitle: "Đã cập nhật cho size: \((self.sizeProductSelected?.title).asString) thành công", type: .success)
        }
    }
    func addSectionSizeInfo() {
        form
            +++ Section("Thông tin size: \((sizeProductSelected?.title).asString)") {
                $0.tag = "sizeInfo"
            }
            <<< IntRow(){
                $0.title = "Số lượng"
                $0.value = (sizeProductSelected?.quantity).asInt
                }.cellUpdate({ (cell, row) in
                    self.sizeProductSelected?.quantity = row.value?.description
                })
            <<< IntRow(){
                $0.title = "Giá bán"
                $0.value = (sizeProductSelected?.price).asInt
                }.cellUpdate({ (cell, row) in
                    self.sizeProductSelected?.price = row.value?.description

                })
            <<< IntRow(){
                $0.title = "Giá khuyến mãi"
                $0.value = (sizeProductSelected?.pricekm).asInt
                }.cellUpdate({ (cell, row) in
                    self.sizeProductSelected?.pricekm = row.value?.description

                })
            <<< DateInlineRow(){
                $0.title = "Hết hạn"
                $0.value = Date().toDate(string: (sizeProductSelected?.expired).asString)
                }.cellUpdate({ (cell, row) in
                    self.sizeProductSelected?.expired = row.value?.toString(dateFormat: "yyyy-MM-dd")

                })
    }
    func addSectionNote() {
        form
            +++ Section("Ghi chú"){
                $0.tag = "note"
            }
            
            <<< TextAreaRow(){
                $0.value = sizeProductSelected?.note
                }.cellUpdate({ (cell, row) in
                    self.sizeProductSelected?.note = row.value
                })
    }
    //MARK:- Outlet Actions

}
extension DetailProductWareHouseController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sizeProducts?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! SizeProductInWareHouseCell
        cell.setupDataSource(sizeProduct: (sizeProducts?[indexPath.row])!)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.sizeProductSelected = sizeProducts?[indexPath.row]
        self.form.remove(at: 1)
        self.form.remove(at: 1)
        addSectionSizeInfo()
        addSectionNote()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 220, height: collectionView.frame.height * 0.8)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


//
//  WareHouseController.swift
//  neoPOS
//
//  Created by mac on 8/16/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import ObjectMapper
import PopupDialog

private let idCell  = "ProductInWareHouseCell"

class WareHouseController: UIViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: SearchDefault!
    @IBOutlet weak var topCollectionConstraint: NSLayoutConstraint!
    
    
    //MARK:- Declare
    var products:[WareHouseProductModel]?
    var productsFiltered:[WareHouseProductModel]?
    var textSearch:String?
    var page = 0
    var pageFiltered = 0
    var type = WareHouseType.View
    var isPressedSearch = false
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getData()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func getData()  {
        var param = ["page":self.page.description]

        if let textSearch = self.textSearch  {
            param = ["page":self.pageFiltered.description,"keyword":textSearch]
        }
        self.getWareHouseProducts(param: param) { [weak self](products) in
            guard let products = products else {return}
            let oldProductsCount = self?.textSearch == nil ? self?.products?.count : self?.productsFiltered?.count
            
            self?.sortData(products: products , completionItem: { [weak self](productFiltered) in
                switch self?.textSearch {
                case nil:
                    self?.products = self?.products == nil ? [] : self?.products
                    self?.products?.append(productFiltered)

                default:
                    self?.productsFiltered = self?.productsFiltered == nil ? [] : self?.productsFiltered
                    self?.productsFiltered?.append(productFiltered)
                }
                }, completionSort: {
                    let newProductsCount = self?.textSearch == nil ? self?.products?.count : self?.productsFiltered?.count
                    let oldCount = oldProductsCount ?? 0
                    let newCount = newProductsCount ?? 0
                    if (newCount + oldCount) > 0 {
                        self?.removeEmptyView(onItem: self?.collectionView ?? UIView())
                        self?.insertIndexPaths(oldCount: oldCount, newCount: newCount)
                    }else{
                        self?.createEmptyView(onItem: self?.collectionView ?? UIView(), didTryAgain: {
                            self?.getData()
                        })
                    }
            })
            
            
        }
    }
    func completedProgressingGetData() {
        self.collectionView.isUserInteractionEnabled = true
        self.collectionView.finishInfiniteScroll()
    }
    func sortData(products:[WareHouseProductModel],completionItem:@escaping (WareHouseProductModel) -> Void,completionSort:@escaping() -> Void) {
        var  ids = products.map{$0.id.asString}.unique
        
        switch textSearch {
        case nil:
            
            if  let oldProducts = self.products  {
                let oldIds = oldProducts.map{ $0.id.asString }.unique
                ids.append(contentsOf: oldIds)
                ids = ids.unique
            }
            self.page += 1

        default:
            
            if  let oldProducts = self.productsFiltered  {
                let oldIds = oldProducts.map{ $0.id.asString }.unique
                ids.append(contentsOf: oldIds)
                ids = ids.unique
            }
            self.pageFiltered += 1
        }
        
        ids.forEach({ (id) in
            if let productFiltered = products.filter({ (wareHouse) -> Bool in
                wareHouse.id == id
            }).first {
                completionItem(productFiltered)
            }
            
        })
        completionSort()
    }
    func insertIndexPaths(oldCount:Int,newCount:Int)  {
        if newCount > oldCount {
            let range = newCount - oldCount
            var indexs = [IndexPath]()
            for i in 0..<range {
                let indexPath = IndexPath(item: oldCount + i, section: 0)
                indexs.append(indexPath)
            }
            self.collectionView.performBatchUpdates({
                self.collectionView.insertItems(at: indexs)
            }, completion: { (bool) in
                self.completedProgressingGetData()
            })
            return
        }
        self.completedProgressingGetData()

    }
    func setupUI() {
        self.title = "Kho"
        self.searchBar.delegate = self
        self.createBarItem(title: nil, imageName: "search", selector: #selector(self.didPressSearch))
        if type == .View {
            self.createBarItem(title: nil, imageName: "caretdown", selector: #selector(self.didPressMore))
        }else {
            setBackButton()
        }
        

    }
    @objc func didPressSearch() {
        isPressedSearch = !isPressedSearch
        let topConstraint = isPressedSearch ? self.searchBar.height : 0
        self.topCollectionConstraint.constant = topConstraint
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    @objc func didPressWareHouse() {
        
    }
    func setupDataSource() {
        self.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.addInfiniteScroll { (collectionView) in
            self.collectionView.isUserInteractionEnabled = false
            self.getData()
        }
        
    }
    @objc func didPressMore() {
        
    }
    func getWareHouseProducts(param:[String:Any],completion: @escaping ([WareHouseProductModel]?) -> Void ) {
        RequestHelper.shared.getProductsInWareHouse(param: param , vc: self) { (dic, error) in
            guard let newDic = dic else {
                self.collectionView.isUserInteractionEnabled = true
                self.collectionView.finishInfiniteScroll()
                completion(nil)
                return}
            guard let products = Mapper<WareHouseProductModel>().mapArray(JSONObject: newDic["products"]) else {
                self.collectionView.isUserInteractionEnabled = true
                self.collectionView.finishInfiniteScroll()
                completion(nil)
                return}
            completion(products)
        
        }
    }
    //MARK:- Outlet Actions
    
}
extension WareHouseController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.textSearch != nil ? self.productsFiltered?.count ?? 0 : self.products?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as!
        ProductInWareHouseCell
        let models = self.textSearch != nil ? self.productsFiltered : self.products
        cell.setupDataSource(product: models![indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let models = self.textSearch != nil ? self.productsFiltered : self.products

        guard self.type == .View else {
            let vc = SizesPopupController()
            vc.product = models?[indexPath.row]
            let popUp = PopupDialog(viewController: vc)
            self.present(popUp, animated: true, completion: nil)
            return
        }
        let vc = DetailProductWareHouseController()
        vc.product = models?[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let magrin:CGFloat = 0
        return UIEdgeInsets.init(top: magrin, left: magrin, bottom: magrin, right: magrin)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0//5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0// 5
    }
}

extension WareHouseController:UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        self.removeEmptyView(onItem: self.collectionView)
        self.dismissKeyboard()
        self.productsFiltered = nil
        self.pageFiltered = 0
        self.textSearch = nil
        self.collectionView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.dismissKeyboard()
        self.productsFiltered = nil
        self.collectionView.reloadData()
        guard let text = searchBar.text , !text.isEmpty else {
            return}
        self.textSearch = text
        self.pageFiltered = 0
        self.getData()
    }
}

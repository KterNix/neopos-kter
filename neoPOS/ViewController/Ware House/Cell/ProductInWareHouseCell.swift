//
//  ProductInWareHouseCell.swift
//  neoPOS
//
//  Created by mac on 8/16/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class ProductInWareHouseCell: UICollectionViewCell {

    
    //MARK:- Outlet
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nameProduct: RecommentLabel!
    @IBOutlet weak var codeProduct: NormalLabel!
    @IBOutlet weak var priceProduct: UILabel!
    @IBOutlet weak var imageRightArrow: UIImageView!
    @IBOutlet weak var collectionSizes: UICollectionView!
    
    
    //MARK:- Declare
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        // Initialization code
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.imageRightArrow.setImageWith(tintColor: .groupTableViewBackground)
    }
    func setupDataSource(product:WareHouseProductModel) {
        self.image.setImageWith(path: (product.image).asString, imageType: .NORMAL_IMAGE)
        self.codeProduct.text = "Mã: \((product.code).asString)"
        self.priceProduct.text = (product.price).asDouble.withCommas()
        self.nameProduct.text = (product.title).asString
        
        
    }
    
    //MARK:- Outlet Actions

}



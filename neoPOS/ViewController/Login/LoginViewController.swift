//
//  LoginViewController.swift
//  neoPOS
//
//  Created by mac on 9/19/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import TSMessages
import ObjectMapper
class LoginViewController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var emailTF: DefaultTextField!
    @IBOutlet weak var passwordTF: DefaultTextField!
    @IBOutlet weak var btnLogin: StyleDefault!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var labelWelcome1: UILabel!
    @IBOutlet weak var labelWelcome2: UILabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.labelWelcome1.textColor = .mainColor
        self.labelWelcome2.textColor = .mainColor
        self.btnLogin.colorTitle = .orange
        self.btnLogin.borderColor = .orange
        self.btnLogin.borderWidth = 0.5
        self.emailTF.delegate = self
        self.passwordTF.delegate = self
        self.defaultStatusBar()
        ///////////
        self.emailTF.text = "ntcnet@gmail.com"
        self.passwordTF.text = "123456"
    }
    
    func setupDataSource() {
        
    }
    func login() {
        guard Utils.getCurrentToken() == nil else {
            Utils.shared.changeRootView(TabbarController(), completion_: nil)
            return}
        guard let email = self.emailTF.text ,!email.isEmpty else {
            self.showErrorNotHaveAnyString()
            return}
        guard let password = self.passwordTF.text, !password.isEmpty else {
            self.showErrorNotHaveAnyString()
            return}
        RequestHelper.shared.loginWith(username: email, password: password, viewController: self) { (dic, error) in
            guard let baseUser = Mapper<BaseUser>().map(JSONObject: dic) else {return}
            Utils.saveModelForUser(baseUser)
            Utils.shared.changeRootView(TabbarController(), completion_: nil)
        }
    }
    func showErrorNotHaveAnyString() {
        TSMessage.showNotification(withTitle: "Hmm.. Có vẻ không đủ thông tin. Vui lòng kiểm tra lại.", type: .error)
    }
    //MARK:- Outlet Actions
    @IBAction func abtnLogin(_ sender: Any) {
        self.login()
    }
    @IBAction func abtnForgotPassword(_ sender: Any) {
        
    }
    
}
extension LoginViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.emailTF:
            self.passwordTF.becomeFirstResponder()
        default:
            self.dismissKeyboard()
            self.login()
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case self.emailTF:
            textField.returnKeyType = .continue
        default:
            textField.returnKeyType = .join
        }
    }

}

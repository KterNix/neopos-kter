//
//  DetailPaymentNot_ReceiptController.swift
//  neoPOS
//
//  Created by mac on 9/13/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class DetailPaymentNot_ReceiptController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var codeModel: TitleLabel!
    @IBOutlet weak var amountModel: RecommentDiscountLabel!
    @IBOutlet weak var descriptionModel: UITextView!
    @IBOutlet weak var fullnameModel: RecommentLabel!
    @IBOutlet weak var titleWhomResponsibility: NormalLabel!
    
    
    //MARK:- Declare
    var paymentReceipt :PaymentNote_ReceiptModel?
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.descriptionModel.font = UIFont.recommentFont
        setBackButton()
        
    }
    func setupDataSource() {
        self.title = "Chi tiết phiếu \(paymentReceipt?.type == "1" ? "thu" : "chi")"
        self.titleWhomResponsibility.text = paymentReceipt?.type == "1" ? "Người nộp" : "Người chi tiền"
        self.codeModel.text = paymentReceipt?.code
        self.descriptionModel.text = paymentReceipt?.descriptionValue
        self.fullnameModel.text = paymentReceipt?.fullname
        self.amountModel.text = (paymentReceipt?.amount).asDouble.withCommas()
    }
    
    //MARK:- Outlet Actions

}

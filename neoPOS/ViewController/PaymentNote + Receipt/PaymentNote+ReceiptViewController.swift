//
//  PaymentNote+ReciptViewController.swift
//  neoPOS
//
//  Created by mac on 8/22/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import ObjectMapper
import PopupDialog

private let idCell = "PaymentNote_ReceiptCell"
private let idReuseview = "SegmentedViewControl"
private let imageUpArrow = "up-arrow"
private let imageDownArrow = "down-arrow"

class PaymentNote_ReceiptViewController: UIViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var searchBar: SearchDefault!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewEmptyTopContraint: NSLayoutConstraint!
    @IBOutlet weak var viewPaymentNote_ReceiptTopContraint: NSLayoutConstraint!
    @IBOutlet weak var labelEmpty: TitleLabel!
    @IBOutlet weak var btnReload: StyleDefault!
    @IBOutlet weak var viewEmpty: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var viewReceipt: UIView!
    @IBOutlet weak var viewPaymentNote: UIView!
    @IBOutlet weak var totalReceipt: TitleLabel!
    @IBOutlet weak var totalPaymentNote: TitleLabel!
    @IBOutlet weak var titleReceipt: RecommentLabel!
    @IBOutlet weak var titlePaymentNote: RecommentLabel!
    @IBOutlet weak var btnExpand: UIButton!
    
    
    //MARK:- Declare
    var isPressedSearch = false
    var page = 0
    var fromdate : Date?
    var todate : Date?
    var typeFilter = TypeDateToSelect.Today
    var type = ""
    var status = ""
    var keyword = ""
    var isPressedExpand = true
    var paymentNote_Receipts:[PaymentNote_ReceiptModel]?
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.btnReload.colorTitle = .secondColor
        self.collectionView.backgroundColor = .groupTableViewBackground
        self.view.backgroundColor = .white
        self.createBarItem(forLeft: true, title: nil, imageName: "search", selector: #selector(self.didPressSearch))
        self.createBarItem( title: nil, imageName: "caretdown", selector: #selector(self.didPressFilter))
        self.createBarItem( title: "+", imageName: nil, selector: #selector(self.didPressAddMore), fontSize: 25)
        self.titlePaymentNote.textColor = .white
        self.titleReceipt.textColor = .white
        self.totalReceipt.textColor = .white
        self.totalPaymentNote.textColor = .white
        self.viewReceipt.backgroundColor = .mainColor
        self.viewPaymentNote.backgroundColor = UIColor.Social.googlePlus
        self.btnExpand.tintColor = .lightGray
        self.btnExpand.setImage(self.imageForButtonExpand(), for: .normal)
        self.segmentControl.tintColor = .orange
        self.segmentControl.addTarget(self, action: #selector(self.didPressChangeType(segment:)), for: .valueChanged)
        self.collectionView.addInfiniteScroll { [weak self] (collection) in
            self?.getData()
        }
    }
    func setupDataSource() {
        self.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionView.register(UINib(nibName: idReuseview, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: idReuseview)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.initValueToGetData()
    }
    @objc func didPressAddMore() {
        let vc = CreateNewPaymentNotOrReceiptController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func didPressFilter() {
        let vc = FilterForPayment_Receipt()
        vc.delegate = self
        let popup = PopupDialog(viewController: vc)
        let doneBtn = DefaultButton(title: "Xong") { [weak self] in
            self?.fromdate = vc.fromdate
            self?.todate = vc.todate
            self?.typeFilter = .Custom
            self?.page = 0
            self?.paymentNote_Receipts = nil
            self?.getData()
        }
        doneBtn.setTitleColor(.white, for: .normal)
        doneBtn.backgroundColor = .orange
        popup.addButton(doneBtn)
        self.present(popup, animated: true, completion: nil)
    }
    @objc func didPressSearch() {
        isPressedSearch = !isPressedSearch
        let topContraint:CGFloat = isPressedSearch ? 56 : 0
        //        guard topContraint != self.collectionTopContraint.constant else {return}
        self.viewPaymentNote_ReceiptTopContraint.constant = topContraint
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        if !isPressedSearch {
            self.dismissKeyboard()
        }
    }
    func imageForButtonExpand() -> UIImage {
    
        let imageName = isPressedExpand ? imageUpArrow : imageDownArrow
        return (UIImage(named: imageName)?.resize(width: 40, height: 20)?.withRenderingMode(.alwaysTemplate))!
    }
    
    func getData() {
        var param = ["status":status,"type":type,"page": page.description]
        if let fromdate = fromdate {
            param["fromdate"] = fromdate.toString(dateFormat: "yyyy-MM-dd")
        }
        if let todate = todate {
            param["todate"] = todate.toString(dateFormat: "yyyy-MM-dd")
        }
        self.checkTypeFilterToSetTitle()
        RequestHelper.shared.getPaymentNote_Receipt(param: param, vc: self) { (dic, error) in
            guard let newDic = dic else {
                self.checkData()
                return}
            guard let payments = Mapper<PaymentNote_ReceiptModel>().mapArray(JSONObject: newDic["ledgers"]) else {
                self.checkData()
                return}
            if let tongthu = newDic["TongThu"] as? Double {
                self.totalReceipt.text = tongthu.withCommas()
            }
            if let tongchi = newDic["TongChi"] as? Double {
                self.totalPaymentNote.text = tongchi.withCommas()
            }
            guard let oldCount = self.paymentNote_Receipts?.count else {
                self.paymentNote_Receipts = payments
                self.page += 1
                self.collectionView.reloadData()
                self.collectionView.finishInfiniteScroll()
                self.checkData()
                return}
            self.paymentNote_Receipts?.append(contentsOf: payments)
             let newCount = self.paymentNote_Receipts?.count
            if newCount! > oldCount {
                let range = newCount! - oldCount
                var indexs = [IndexPath]()
                for i in 0..<range{
                    let index = IndexPath(row: i + oldCount, section: 0)
                    indexs.append(index)
                }
                self.collectionView.performBatchUpdates({
                    self.collectionView.insertItems(at: indexs)
                }, completion: { (bool) in
                    self.page += 1
                    self.checkData()
                })
            }else {
                self.collectionView.finishInfiniteScroll()
            }
        }
    }
    func checkData() {
        guard let count = self.paymentNote_Receipts?.count else {
            self.viewEmpty.isHidden = false
            self.collectionView.isHidden = true
            return}
        self.collectionView.finishInfiniteScroll()
        var text  = ""
        if count == 0 {
            if type == "1"{
                text = "thu"
            }else if type == "2"{
                text = "chi"
            }else {
                text = "thu và chi"
            }
            self.viewEmpty.isHidden = false
            self.collectionView.isHidden = true
        }else {
            self.viewEmpty.isHidden = true
            self.collectionView.isHidden = false
        }
        self.labelEmpty.text = "Danh sách \(text) ( \((self.navigationItem.title)!) ) trống"
    }
    func checkTypeFilterToSetTitle() {
        var title = ""
        switch self.typeFilter {
        case .Yesterday:
            title = "Hôm qua"
        case .ThisWeek:
            title = "Tuần này"
        case .ThisMonth:
            title = "Tháng này"
        case .PreviousMonth:
            title = "Tháng trước"
        case .Custom:
            title = "\((fromdate?.toString(dateFormat: "dd/MM/yyyy"))!) đến \((todate?.toString(dateFormat: "dd/MM/yyyy"))!)"
        default:
            title = "Hôm nay"
        }
        self.navigationItem.title = title
        self.tabBarItem.title = "Thu chi"
    }
    func initValueToGetData() {
        self.page = 0
        self.status = "1"
        self.type = "0"
        self.getData()
    }
    @objc func didPressChangeType(segment:UISegmentedControl) {
        self.paymentNote_Receipts = nil
        switch segment.selectedSegmentIndex {
        case 0:
            self.type = "1"
        case 1:
            self.type = "2"
        default:
            self.type = "0"
        }
        self.page = 0
        self.getData()
    }
    //MARK:- Outlet Actions
    @IBAction func abtnReload(_ sender: Any) {
        self.getData()
    }
    @IBAction func abtnExpand(_ sender: Any) {
        isPressedExpand = !isPressedExpand
        let heightContraint:CGFloat = isPressedExpand ? 0  : -150
        self.viewEmptyTopContraint.constant = heightContraint
        self.btnExpand.setImage(self.imageForButtonExpand(), for: .normal)
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        
    }
    
}
extension PaymentNote_ReceiptViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paymentNote_Receipts?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! PaymentNote_ReceiptCell
        cell.setupDataSource(model: paymentNote_Receipts![indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailPaymentNot_ReceiptController()
        vc.paymentReceipt = paymentNote_Receipts![indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10 , height: 70)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: idReuseview, for: indexPath) as! SegmentedViewControl
//        view.segmentControl.addTarget(self, action: #selector(self.didPressChangeType(segment:)), for: .valueChanged)
//        return view
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: collectionView.width, height: 56)
//    }
}
extension PaymentNote_ReceiptViewController : FilterForPayment_ReceiptDelegate{
    func didPressChangeTypeDate(fromdate: Date?, todate: Date?, type:TypeDateToSelect) {
        self.fromdate = fromdate
        self.todate = todate
        self.typeFilter = type
        self.page = 0
        self.paymentNote_Receipts = nil
        self.getData()
    }
    
    
}

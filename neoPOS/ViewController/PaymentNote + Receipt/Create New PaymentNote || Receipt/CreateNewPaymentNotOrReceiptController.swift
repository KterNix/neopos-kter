//
//  CreateNewPaymentNotOrReceiptController.swift
//  neoPOS
//
//  Created by mac on 9/13/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
import TSMessages
private let tagSectionInfoPeople = "sectionInfoPeople"
class CreateNewPaymentNotOrReceiptController: MasterFormController {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    var type = 1
    var amount = 0.0
    var refer = ""
    var descriptionValue = ""
    var fullname = ""
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setUpForm()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
            +++ Section(){ section in
                var header = HeaderFooterView<SegmentedViewControl>(.nibFile(name:"SegmentedViewControl",bundle:nil))
                header.onSetupView = { view, _ in
                    view.segmentControl.addTarget(self, action: #selector(self.didChangeValue(segment:)), for: .valueChanged)
                    
                }
                header.height = { 50 }
                section.header = header
        }
            +++ Section("Thông tin người nộp"){
                $0.tag = tagSectionInfoPeople
            }
            <<< TextRow(){
                $0.title = "Tên"
                }.cellUpdate({ (cell, row) in
                    self.fullname = row.value ?? ""
                })
        +++ Section("Phí")
        <<< DecimalRow(){
            $0.title = "Số tiền"
            }.cellUpdate({ (cell, row) in
                self.amount = row.value ?? 0
            })
        +++ Section("Lý do")
            <<< TextAreaRow().cellUpdate({ (cell, row) in
                
            }).cellUpdate({ (cell, row) in
                self.descriptionValue = row.value ?? ""
            })
        +++ Section("Tuỳ chọn")
            <<< TextRow(){
                $0.title = "Liên tài liệu"
                $0.placeholder = "VD: Mã số hoá đơn..."
                }.cellUpdate({ (cell, row) in
                    self.refer = row.value ?? ""
                })
            +++ Section(){section in
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                    let button = StyleDefault(frame: CGRect(x: 0, y: 0, width: self.view.width * 0.8, height: 40))
                    button.title = "Lưu"
                    button.colorTitle = .orange
                    button.borderWidth = 0.5
                    button.borderColor = .orange
                    button.cornerRadius = 5
                    button.center = view.center
                    button.tag = 2
                    view.addSubview(button)
                    return view
                }))
                header.height = {50}
                header.onSetupView = {view, _ in
                    guard let button = view.viewWithTag(2) as? UIButton else {return}
                    button.addTarget(self, action: #selector(self.didPressDone), for: .touchUpInside)
                }
                section.header = header
        }
    }
    //MARK:- SubFunction
    func setupUI() {
        self.title = "Tạo thu chi"
        setBackButton()
    }
    func setupDataSource() {
        
    }
    @objc func didChangeValue(segment:UISegmentedControl) {
        self.type = segment.selectedSegmentIndex + 1
        self.form.sectionBy(tag: tagSectionInfoPeople)?.header?.title = ""
        self.form.sectionBy(tag: tagSectionInfoPeople)?.reload()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.form.sectionBy(tag: tagSectionInfoPeople)?.header?.title = "Thông tin người \(self.type == 1 ? "nộp" : "chi")"
            self.form.sectionBy(tag: tagSectionInfoPeople)?.reload()
        }) 
        
    
    }
    @objc func didPressDone() {
        let param = self.parseToParam()
        RequestHelper.shared.postLedger(param: param, vc: self) { (dic, error) in
        guard  dic != nil else {return}
            TSMessage.showNotification(withTitle: "Tạo thu chi thành công!", type: .success)
        }
    }
    func parseToParam() -> [String:String] {
        let param = ["code":"","type":self.type.description,"aid":"","amount":self.amount.description,"ltype":"1","refer":self.refer.description,"refertype":"","ctype":"VNĐ","cdate":Date().toString(dateFormat: "yyyy-MM-dd"),"description":self.descriptionValue,"status":"1","fullname":fullname]
        return param
    }
    //MARK:- Outlet Actions

}

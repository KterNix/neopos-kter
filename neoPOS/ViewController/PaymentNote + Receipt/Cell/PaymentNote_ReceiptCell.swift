//
//  PaymentNote_ReceiptCell.swift
//  neoPOS
//
//  Created by mac on 9/6/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class PaymentNote_ReceiptCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var timeLabel: TitleLabel!
    @IBOutlet weak var dayLabel: RecommentLabel!
    @IBOutlet weak var code: RecommentLabel!
    @IBOutlet weak var type: StatusLabel!
    @IBOutlet weak var totalPrice: RecommentDiscountLabel!
    @IBOutlet weak var imageRightArrow: UIImageView!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.imageRightArrow.tintColor = .groupTableViewBackground
        self.imageRightArrow.image = self.imageRightArrow.image?.withRenderingMode(.alwaysTemplate)
        self.backgroundColor = .white
        self.type.cornerRadius = 5
    }
    func setupDataSource(model:PaymentNote_ReceiptModel) {
        if let date = Date().toDate(string:model.cdate.asString) {
            let hour = Calendar.current.component(.hour, from: date)
            let minute = Calendar.current.component(.minute, from: date)
            let day = Calendar.current.component(.day, from: date)
            let month = Calendar.current.component(.month, from: date)
            self.dayLabel.text = day.description + "/" + month.description
            self.timeLabel.text = hour.description + ":" + minute.description
        }
        
        self.code.text = model.code
        switch model.type.asString {
        case "1":
            self.type.text = "Thu"
            self.type.backgroundColor = .mainColor
        default:
            self.type.text = "Chi"
            self.type.backgroundColor = .red
        }
        self.totalPrice.text = model.amount.asDouble.withCommas()
    }
    
    //MARK:- Outlet Actions

}

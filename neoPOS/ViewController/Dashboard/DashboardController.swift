//
//  DashboardController.swift
//  neoPOS
//
//  Created by mac on 8/22/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import ObjectMapper
import Crashlytics
private let idTaskCell = "TaskCell"
private let idStatisticsCell = "StatisticsForTodayCell"
private let idChartCell = "ChartCell"
private let idReuseableView = "TitleCagoriesView"

class DashboardController: UIViewController {
    
    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    //MARK:- Declare
    var tasks:[TaskModel]?
    let titles = ["Công việc mới nhất","Đơn hàng hôm nay","Biểu đồ năm 2018"]
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        getTasks()
        getChartData()
        getOrdersForToday()
//        Crashlytics.sharedInstance().crash()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.title = "Dashboard"
        self.collectionView.backgroundColor = .groupTableViewBackground
        self.createBarItem( title: "+", imageName: nil, selector: #selector(self.didPressAddTask), fontSize: 25)
    }
    func setupDataSource() {
        self.collectionView.register(UINib(nibName: idTaskCell, bundle: nil), forCellWithReuseIdentifier: idTaskCell)
        self.collectionView.register(UINib(nibName: idReuseableView, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: idReuseableView)
        self.collectionView.register(UINib(nibName: idStatisticsCell, bundle: nil), forCellWithReuseIdentifier: idStatisticsCell)
        self.collectionView.register(UINib(nibName: idChartCell, bundle: nil), forCellWithReuseIdentifier: idChartCell)
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    @objc func didPressAddTask() {
        let vc = FormCreateNewTaskController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func getTasks() {
        let date = Date().toString(dateFormat: "yyy-MM-dd")
        let param = ["page": 0.description, "date":date]
        RequestHelper.shared.getTasks(param: param, vc: self) { (dic, error) in
            guard let newDic = dic else {return}
            guard let tasks = Mapper<TaskModel>().mapArray(JSONObject: newDic["tasks"]) else {return}
            self.tasks = tasks
            self.collectionView.reloadSections([0])
        }
    }
    @objc func didSelectedViewMore(button:UIButton) {
        switch button.tag {
        case 0:
            let vc = TasksController()
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            self.tabBarController?.selectedViewController = self.tabBarController?.viewControllers![1]
        default:
            break
        }
    }
    func getChartData() {
        RequestHelper.shared.getChartData(vc: self) { (dic, error) in
            guard let newDic = dic else {return}
            guard let paymenNotes = newDic["chi"] as? [Double] else {return}
            guard let receipts = newDic["thu"] as? [Double] else {return}
            guard let chartCell = self.collectionView.cellForItem(at: IndexPath(row: 0, section: 2)) as? ChartCell else {return}
            chartCell.setupDataSource(paymentNotes: paymenNotes, receipts: receipts)
        }
    }
    func getOrdersForToday() {
        let currentDate = Date().toString(dateFormat: "yyyy-MM-dd")
        let param = ["page":"0","fromdate":currentDate,"todate":currentDate,"status": "2"]
        RequestHelper.shared.getOrders(param: param, vc: self) { (dic, error) in
            self.collectionView.finishInfiniteScroll()
            guard let newDic = dic else {return}
            var new = ""
            var processing = ""
            var completed = ""
            var pending = ""
            if let newNumber = newDic["new"] as? String {
                new = newNumber
            }
            if let processingNumber = newDic["processing"] as? String {
                processing = processingNumber
            }
            if let completedNumber = newDic["completed"] as? String {
                completed = completedNumber
            }
            if let pendingNumber = newDic["pending"] as? String {
                pending = pendingNumber
            }
            guard let statisticsCell = self.collectionView.cellForItem(at: IndexPath(row: 0, section: 1)) as? StatisticsForTodayCell else {return}
            statisticsCell.getData(new: new, processing: processing, completed: completed, pending: pending)
        }
    }
    //MARK:- Outlet Actions

}
extension DashboardController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.tasks?.count ?? 0
        default:
            return 1
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return titles.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idTaskCell, for: indexPath) as! TaskCell
            cell.setupDataSource(task: tasks![indexPath.row])
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idStatisticsCell, for: indexPath) as! StatisticsForTodayCell
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idChartCell, for: indexPath) as! ChartCell
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let vc = FormCreateNewTaskController()
            vc.type = .Detail
            vc.task = self.tasks![indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.width, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: idReuseableView, for: indexPath) as! TitleCagoriesView
        view.setupDataSource(title: titles[indexPath.section])
        view.btnMore.tag = indexPath.section
        view.btnMore.isHidden = indexPath.section == 2 ? true : false 
        view.btnMore.addTarget(self, action: #selector(self.didSelectedViewMore(button:)), for: .touchUpInside)
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width: collectionView.width - 10 , height: 80)
        case 1:
            return CGSize(width: collectionView.width  , height: 150)
        default:
            return CGSize(width: collectionView.width  , height: 250)

        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        switch section {
        case 0:
            return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
        default:
            return UIEdgeInsets.init(top: 5, left: 0, bottom: 5, right: 0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


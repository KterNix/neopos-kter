//
//  StatisticsForTodayCell.swift
//  neoPOS
//
//  Created by mac on 8/23/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
private let idCell = "InfoStatisticCell"

class StatisticsForTodayCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var collectionStatistics: UICollectionView!
    
    
    //MARK:- Declare
    let title = ["Mới", "Đang xử lý","Hoàn thành","Tạm ngưng"]
    var number = ["0","0","0","0"]
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.collectionStatistics.backgroundColor = .groupTableViewBackground
        self.collectionStatistics.isHidden = true

    }
    func setupDataSource() {
        self.collectionStatistics.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionStatistics.delegate = self
        self.collectionStatistics.dataSource = self
    }
    func getData(new:String, processing:String, completed:String,pending:String) {
        self.number = [new,processing,completed,pending]
        self.collectionStatistics.reloadData()
        self.collectionStatistics.isHidden = false
    }
    //MARK:- Outlet Actions

}
extension StatisticsForTodayCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return title.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! InfoStatisticCell
        cell.setupDataSource(title: title[indexPath.row], number: number[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.width - CGFloat(self.title.count*5 + 5))/CGFloat(self.title.count), height: collectionView.frame.height * 0.7)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


//
//  TaskCell.swift
//  neoPOS
//
//  Created by mac on 8/22/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class TaskCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var timeLabel: TitleLabel!
    @IBOutlet weak var dateLabel: RecommentLabel!
    @IBOutlet weak var descriptionLabel: NormalLabel!
    @IBOutlet weak var codeAndEmployee: RecommentLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.backgroundColor = .white
    }
    
    func setupDataSource(task:TaskModel) {
        codeAndEmployee.text = task.code
        if let date = Date().toDate(string:task.bdate.asString) {
            let hour = Calendar.current.component(.hour, from: date)
            let minute = Calendar.current.component(.minute, from: date)
            let day = Calendar.current.component(.day, from: date)
            let month = Calendar.current.component(.month, from: date)
            self.dateLabel.text = day.description + "/" + month.description
            self.timeLabel.text = hour.description + ":" + minute.description
        }
        
        let text = task.customer?.code == nil ? "Có hẹn với khách vãng lai" : "Khách hàng \((task.customer?.fullname).asString) đã đặt lịch hẹn."
        self.descriptionLabel.text = text
    }
    
    //MARK:- Outlet Actions

}

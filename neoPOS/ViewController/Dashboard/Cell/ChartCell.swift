//
//  ChartCell.swift
//  neoPOS
//
//  Created by mac on 8/23/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Charts
class ChartCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var barChart: BarChartView!
    @IBOutlet weak var indicator: DefaultIndicator!
    
    
    //MARK:- Declare
    let months = ["1" ,"2" ,"3" ,"4" ,"5" ,"6" ,"7" ,"8" ,"9" ,"10" ,"11" , "12"]
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        barChart.delegate = self
        barChart.drawBarShadowEnabled = false
        barChart.drawValueAboveBarEnabled = false
        barChart.maxVisibleCount = 12

        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 0
        leftAxisFormatter.maximumFractionDigits = 3
        leftAxisFormatter.negativeSuffix = " tr"
        leftAxisFormatter.positiveSuffix = " tr"
        leftAxisFormatter.numberStyle = NumberFormatter.Style.decimal

        let leftAxis = barChart.leftAxis
        leftAxis.labelFont = .systemFont(ofSize: 10)
        leftAxis.labelCount = 8
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        leftAxis.labelPosition = .outsideChart
        leftAxis.spaceTop = 0.15
        leftAxis.axisMinimum = 0 // FIXME: HUH?? this replaces startAtZero = YES
        
        let rightAxis = barChart.rightAxis
        rightAxis.enabled = false
        
        let l = barChart.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 9
        l.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
        l.xEntrySpace = 4
        let marker = XYMarkerView(color: .mainColor,
                                  font: .systemFont(ofSize: 12),
                                  textColor: .white,
                                  insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                  xAxisValueFormatter: DefaultAxisValueFormatter(formatter: leftAxisFormatter))
        marker.chartView = barChart
        marker.minimumSize = CGSize(width: 80, height: 40)
        barChart.marker = marker
        barChart.chartDescription?.text = ""
        self.barChart.isHidden = true
    }
    
    func setupDataSource(paymentNotes:[Double],receipts:[Double]) {
        if self.barChart.data == nil {
            self.barChart.isHidden = false
            let newPayNotes = paymentNotes.map{self.calculationToMilions(number: $0)}
            let newReceipts = receipts.map{self.calculationToMilions(number: $0)}
            setDataForChart(paymentNotes: newPayNotes, receipts: newReceipts)
            self.indicator.isHidden = true
        }
    }
    func calculationToMilions(number:Double) -> Double {
        return number/1000000
    }
    func setDataForChart(paymentNotes:[Double],receipts:[Double]) {
        var paymentNotesEntry = [BarChartDataEntry]()
        var receiptEntry = [BarChartDataEntry]()
        for (i,value) in paymentNotes.enumerated() {
            let dataPaymentEntry = BarChartDataEntry(x: Double(i), y: value)
            paymentNotesEntry.append(dataPaymentEntry)
            
        }
        for (i,value) in receipts.enumerated() {
            let dataReceiptEntry = BarChartDataEntry(x: Double(i), y: value)
            receiptEntry.append(dataReceiptEntry)
        }
        
        let paymentNoteDataSet = BarChartDataSet(values: paymentNotesEntry, label: "Chi")
        let receiptDataSet = BarChartDataSet(values: receiptEntry, label: "Thu")
        paymentNoteDataSet.colors = [UIColor.Social.googlePlus]
        receiptDataSet.colors = [UIColor.secondColor]
        
        let groupSpace = 0.3
        let barSpace = 0.05
        let barWidth = 0.3
        let startYear = 0
        
        let data = BarChartData(dataSets:[paymentNoteDataSet,receiptDataSet])
        data.barWidth = barWidth
        let gg = data.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        data.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        self.barChart.notifyDataSetChanged()
        self.barChart.data = data
        
        let xAxis = barChart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.labelCount = 12
        xAxis.valueFormatter = IndexAxisValueFormatter(values: self.months )
        xAxis.axisMinimum = Double(startYear)
        xAxis.axisMaximum = gg * Double(self.months.count)
        xAxis.centerAxisLabelsEnabled = true
    }
    //MARK:- Outlet Actions

}
extension ChartCell: ChartViewDelegate {
    
}

//
//  InfoStatisticCell.swift
//  neoPOS
//
//  Created by mac on 8/23/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class InfoStatisticCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var titleStatistic: RecommentLabel!
    @IBOutlet weak var number: TitleLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
    }
    func setupDataSource(title: String, number:String) {
        self.titleStatistic.text = title
        self.number.text = number
        var colorBackgroundForNumber:UIColor = .white
        switch title {
        case "Tạm ngưng":
            colorBackgroundForNumber = UIColor.Social.googlePlus
            self.number.color = .white

        case "Hoàn thành":
            colorBackgroundForNumber = UIColor.Social.linkedIn
            self.number.color = .white

        case "Mới":
            colorBackgroundForNumber = UIColor.white
            self.number.color = .black
        default:
            colorBackgroundForNumber = UIColor.orange
            self.number.color = .white

        }
        self.number.backgroundColor = colorBackgroundForNumber
    }
    
    //MARK:- Outlet Actions

}


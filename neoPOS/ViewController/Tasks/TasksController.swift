//
//  TasksController.swift
//  neoPOS
//
//  Created by mac on 9/6/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import ObjectMapper
import FSCalendar
private let idCell = "TaskCell"
typealias CompletionTask = (_ tasks: [TaskModel]?) -> Void
class TasksController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: SearchDefault!
    @IBOutlet weak var calendarView: CalendarDefault!
    @IBOutlet weak var collectionTopContraint: NSLayoutConstraint!
    
    @IBOutlet weak var calendarTopContraint: NSLayoutConstraint!
    
    //MARK:- Declare
    var isPressedSearch = false
    var isPressedCalendar = false
    var page = 0
    var dateSelected = ""
    var tasks:[TaskModel]?
    var tasksOfMonth:[TaskModel]?
    var datesData = [String:Int]()
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        setDataForCalendar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.title = "Danh sách lịch hẹn"
        self.calendarTopContraint.constant =  -self.searchBar.height
        setBackButton()
        self.createBarItem(title: nil, imageName: "search", selector: #selector(self.didPressSearch))
        self.createBarItem(title: nil, imageName: "calendar", selector: #selector(self.didPressCalendar))
    }
    func setupDataSource() {
        getTasks(type: .Date) { [weak self] (tasks) in
            self?.checkData(value: tasks)
        }
        self.collectionView.addInfiniteScroll { [weak self] (collectionView) in
            self?.getTasks(type: .Date, completion: { (tasks) in
                self?.checkData(value: tasks)

            })
        }
        self.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.calendarView.dataSource = self
        self.calendarView.delegate = self

    }
    func checkData(value:[TaskModel]?)  {
        guard let tasks = value else {
            collectionView.finishInfiniteScroll()
            return}
        let oldCount = self.tasks?.count
        if let oldCount = oldCount {
            self.tasks?.append(contentsOf: tasks)
            if let newCount = self.tasks?.count, newCount != 0 {
                guard oldCount != newCount else {
                    collectionView.finishInfiniteScroll()
                    return}
                let numberMore = newCount - oldCount
                var indexs = [IndexPath]()
                for i in 0..<numberMore {
                    let index = IndexPath(row: oldCount + i, section: 0)
                    indexs.append(index)
                }
                collectionView.performBatchUpdates({
                    collectionView.insertItems(at: indexs)
                    
                }, completion: { (bool) in
                    self.page += 1
                })
            }else {return}
        }else {
            self.tasks = tasks
            collectionView.reloadData()
            self.page += 1
        }
        collectionView.finishInfiniteScroll()
    }
    func setDataForCalendar() {
        self.getTasks(type: .Month) { [weak self] (tasks) in
            guard let tasks = tasks else {return}
            self?.tasksOfMonth = tasks
            self?.calculationEvents()
        }
    }
    func getTasks(type:GetTaskType,completion:@escaping CompletionTask) {
        var param = ["page":page.description,"status":"-2","keyword":"","date":dateSelected]
            param["action"] = type.rawValue
        RequestHelper.shared.getTasks(param: param, vc: self) {[weak self] (dic, error) in
            guard let newDic = dic else { completion(nil); return}
            guard let tasks = Mapper<TaskModel>().mapArray(JSONObject: newDic["tasks"]), !tasks.isEmpty else {
                if self?.page == 0 {
                    self?.createEmptyView(onItem: self?.collectionView ?? UIView(),didTryAgain: {
                        switch type {
                        case .Date:
                            self?.getTasks(type: .Date) {  (tasks) in
                                self?.checkData(value: tasks)
                            }
                        case .Month:
                            self?.setDataForCalendar()
                        }
                    })
                }
                completion(nil)
                return}
            if type == .Date {
                self?.removeEmptyView(onItem: self?.collectionView ?? UIView())
            }
            completion(tasks)
        }
    }
    func calculationEvents(){
        var datesData = [String:Int]()
        var dates = self.tasksOfMonth!.map{$0.bdate}
        var indexsNeedDelete = [Int]()
        for (i,value) in dates.enumerated() {
            if value == nil {
                indexsNeedDelete.append(i)
            }else {
                dates[i] = (value!.split(separator: " ").first)?.description
            }
        }
        for i in indexsNeedDelete{
            dates.remove(at: i)
        }
        dates.forEach({ (date) in
            if let tasksForDate = self.tasksOfMonth?.filter({ (task) -> Bool in
                if let bool = task.bdate?.contains(date!) {
                    return bool
                }
                return false
            }){
                datesData[date!] = tasksForDate.count
            }
        })
        self.datesData = datesData
        self.calendarView.reloadData()
        
    }
    @objc func didPressCalendar() {
        isPressedCalendar = !isPressedCalendar
        let topContraint:CGFloat = isPressedCalendar ? self.calendarView.height : 0
        //        guard topContraint != self.collectionTopContraint.constant else {return}
        self.collectionTopContraint.constant = topContraint
        UIView.animate(withDuration: 0.2) {
            self.collectionView.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }
    }
    @objc func didPressSearch() {
        isPressedSearch = !isPressedSearch
        let topContraint:CGFloat = isPressedSearch ? 0 : -self.searchBar.height
//        guard topContraint != self.collectionTopContraint.constant else {return}
        self.calendarTopContraint.constant = topContraint
        UIView.animate(withDuration: 0.2) {
            self.collectionView.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }
    }
    //MARK:- Outlet Actions

}
extension TasksController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tasks?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! TaskCell
        cell.setupDataSource(task: tasks![indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = FormCreateNewTaskController()
        vc.type = .Detail
        vc.task = self.tasks![indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
extension TasksController: FSCalendarDelegate, FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        if let value = datesData[date.toString(dateFormat: "yyyy-MM-dd")] {
            return value
        }
        return 0
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.dateSelected = date.toString(dateFormat: "yyyy-MM-dd")
        self.page = 0
        self.tasks = nil
        self.collectionView.reloadData()
        self.getTasks(type: .Date) { [weak self] (tasks) in
            self?.checkData(value: tasks)
        }
    }
}

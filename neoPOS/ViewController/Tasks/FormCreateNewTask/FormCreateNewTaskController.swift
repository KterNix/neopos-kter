//
//  FormCreateNewTaskController.swift
//  neoPOS
//
//  Created by mac on 8/27/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
import ObjectMapper
import TSMessages
import PopupDialog

private let idCell = "ServiceSelectedCell"

private let tagSectionInforCustomer = "infoCustomer"
private let tagRowNameCustomer = "rowNameCustomer"
private let tagSectionServices = "sectionServices"
private let tagSectionTotal = "sectionTotal"
private let tagRowNameMember = "rowNameMember"

class FormCreateNewTaskController: MasterFormController {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    var task = TaskModel()
    var type = TypeDetail.Create
    var percentViewController:PercentAndDiscountController?
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        setUpForm()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func setUpForm() {
        form
            +++ Section("Loại khách hàng"){
                $0.hidden = self.type == .Detail ? true : false
                $0.evaluateHidden()
            }
            <<< SwitchRow(){
                $0.title = "Khách vãng lai ?"
                $0.value = true
                self.task.otype = 0.description
                }.onChange({  row in
                    if row.value != nil && row.value! {
                        self.form.sectionBy(tag: tagSectionInforCustomer)?.hidden = true
                        self.task.otype = 0.description
                    }else {
                        self.form.sectionBy(tag: tagSectionInforCustomer)?.hidden = false
                        self.task.otype = 1.description
                    }
                    self.form.sectionBy(tag: tagSectionInforCustomer)?.evaluateHidden()
                })
            +++ Section("Thông tin khách hàng"){
                $0.tag = tagSectionInforCustomer
                $0.hidden = self.type == .Detail ? false : true
                $0.evaluateHidden()
            }
            <<< PushRow<String>(){
                $0.title = "Khách hàng"
                $0.tag = tagRowNameCustomer
                $0.disabled = true
                }.onCellSelection({ (cell, row) in
                    let vc = CustomersController()
                    vc.delegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
                }).cellUpdate({ (cell, row) in
                    cell.row.value = self.type == .Detail ? (self.task.customer?.fullname == nil ?  "Khách vãng lai" : self.task.customer?.fullname) : self.task.customer?.fullname
                })
            +++ Section("Mô tả công việc")
            <<< TextAreaRow(){
                $0.value = self.task.descriptionValue
                }.cellUpdate({ (cell, row) in
                    self.task.descriptionValue = row.value
                })

            +++ Section("Giao việc cho")
            <<< PushRow<String>(){
                $0.title = "Nhân viên"
                $0.tag = tagRowNameMember
                $0.disabled = true
                }.onCellSelection({ (cell, row) in
                    let vc = CustomersController()
                    vc.delegate = self
                    vc.typeUser = .Member
                    self.navigationController?.pushViewController(vc, animated: true)
                }).cellUpdate({ (cell, row) in
                    cell.row.value = self.type == .Detail ? (self.task.member?.fullname == nil ?  "Không có" : self.self.task.member?.fullname) : self.task.member?.fullname
                })
            +++ Section("Thời gian")
            <<< DateTimeInlineRow(){
                $0.title = "Thời gian hẹn"
                $0.value = Date().toDate(string: self.task.bdate.asString)
                }.cellUpdate({ (cell, row) in
                    guard let value = row.value  else {return}
                    self.task.bdate = value.toString(dateFormat: "yyyy-MM-dd hh:mm:ss")
                })
            +++ Section(){ section in
                section.tag = tagSectionTotal
                var header =  HeaderFooterView<TotalPriceView>(.nibFile(name:"TotalPriceView",bundle:nil))
                header.height = {130}
                
                header.onSetupView = { view , _ in
                     let total = self.calculationTotal()
                    self.task.total = total?.description ?? 0.description
                    view.getData(total: total ?? 0,discount: self.task.discount)
                    view.btnAddDiscount.addTarget(self, action: #selector(self.didPressAddDiscount), for: .touchUpInside)
                }
                section.header = header
            }
            +++ Section(){ section in
                section.tag = tagSectionServices
                var header = HeaderFooterView<HeaderTitileWithButton>(.nibFile(name:"HeaderTitileWithButton",bundle:nil))
                header.height = {50}
                header.onSetupView = {view,_ in
                    var title = "Dịch vụ"
                    if let details = self.task.details {
                        title += " ( \(details.count) )"
                    }
                    view.title.text = title.uppercased()
                    view.btnMore.title = "Thêm dịch vụ"
                    view.btnMore.addTarget(self, action: #selector(self.didPressAddMoreServices), for: .touchUpInside)
                }
                section.header = header
                
                var footer = HeaderFooterView<CollectionViewForEureka>(.nibFile(name:"CollectionViewForEureka",bundle:nil))
                footer.height = {self.view.height - (Constants.HeightTool.tabbar + 50)}
                footer.onSetupView = {view,_ in
                    guard self.task.details != nil else {return}
                    if view.collectionView.delegate != nil {
                        view.collectionView.reloadData()
                        return
                    }
                    view.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
                    view.collectionView.delegate = self
                    view.collectionView.dataSource = self
                    view.collectionView.reloadData()
                }
                section.footer = footer
        }
        
    }
    deinit {
        print(self.description + "deinit")
    }
    //MARK:- SubFunction
    @objc func didPressAddMoreServices()  {
        let vc = ServicesController()
        vc.delegate = self
        vc.servicesSelected = self.task.details
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func didPressAddDiscount() {
        percentViewController = percentViewController == nil ? PercentAndDiscountController() : percentViewController
        percentViewController?.total = self.task.total.asDouble
        percentViewController?.discount =  self.task.discount.asDouble
        percentViewController?.delegate = self
        let popUp = PopupDialog(viewController: percentViewController!)
        self.present(popUp, animated: true, completion: nil)
    }
    func setupUI() {
        self.title = self.type == .Create ? "Tạo công việc" : "Chi tiết công việc"
        setBackButton()
        if type == .Detail {
            self.createBarItem( title: "Cập nhật", imageName: nil, selector: #selector(self.didPressUpdate), fontSize: 13)

        }else {
            self.createBarItem( title: "Tạo", imageName: nil, selector: #selector(self.didPressCreateTask), fontSize: 13)

        }
    }
    func setupDataSource() {
        
    }
    func calculationTotal() -> Double? {
        var total = 0.0
        if let prices = self.task.details?.map({$0.price.asDouble}) {
            if let quantities = self.task.details?.map({$0.quantity.asDouble}) {
                for (index,value) in prices.enumerated() {
                    total += quantities[index] * value
                }
                return total
            }
        }
        return nil
    }
    @objc func didPressCreateTask() {
        let param = self.parseModelToParam()
        RequestHelper.shared.postTask(param: param, vc: self) { (dic, error) in
            guard let newDic = dic else {return}
            if let message = newDic["message"] as? String {
                if message.contains("Successfully") {
                    TSMessage.showNotification(withTitle: "Đã tạo thành công", type: .success)
                }
            }
        }
    }
    @objc func didPressUpdate() {
        let param = self.parseModelToParam()
        RequestHelper.shared.putTask(idTask: self.task.id.asString, param: param, vc: self) { [unowned self] (dic, error) in
            guard  dic != nil else {return}
            TSMessage.showNotification(withTitle: "Đã cập nhật thành công", type: .success)
        }
    }
    func parseModelToParam() -> [String:Any] {
        var param = ["discount": task.discount.asString,
                     "total":task.total.asString,
                     "voucher":task.voucher.asString,
                     "usecard":"",
                     "status":"1",
                     "pstatus":"0",
                     "description":task.descriptionValue.asString,
                     "note": "",
                     "bdate":task.bdate.asString,
                     "otype": task.otype.asString]
        if task.otype == 1.description {
            param["cid"] = (task.customer?.id).asString
        }
        if var services = task.details {
            if type == .Create {
                let stringDetails = services.toJSONString()
                let details = stringDetails?.replacingOccurrences(of: "id", with: "sid")
                param["details"] = details.asString
                
            }else {
                for i  in 0..<services.count {
                    if services[i].sid == nil {
                        services[i].sid = services[i].id
                        services[i].id = nil
                    }
                }
                let stringDetails = services.toJSONString()
                param["details"] = stringDetails
            }
        }
        
        return param
    }

    func updateTotalPrice() {
        guard let header = self.form.sectionBy(tag: tagSectionTotal)?.header?.viewForSection(self.form.sectionBy(tag: tagSectionTotal)!, type: .header) as?  TotalPriceView else {return}
        if let total = self.calculationTotal() {
            var discount = 0.0
            if let indexSelectedDiscount = self.percentViewController?.indexSelected {
                discount = (total*Double((indexSelectedDiscount*5) + 5))/100
                self.task.discount = discount.description
                self.task.total = total.description
            }
            header.getData(total: total, discount: self.task.discount)
        }
    }
    func getCollectionView() -> UICollectionView? {
        guard let header = self.form.sectionBy(tag: tagSectionServices)?.footer?.viewForSection(self.form.sectionBy(tag: tagSectionServices)!, type: .footer) as? CollectionViewForEureka else {return nil}
        guard let collectionView = header.collectionView else {return nil}
        return collectionView
    }
    //MARK:- Outlet Actions

}
extension FormCreateNewTaskController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.task.details?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! ServiceSelectedCell
        cell.tag = indexPath.row
        cell.setupDataSource(service: self.task.details![indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.width - 10), height: 132)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

extension FormCreateNewTaskController: CustomersControllerDelegate {
    func didSelect(customer: Any, sender: CustomersController) {
        guard let model = customer as? CustomerModel else {
            self.task.member = customer as? MemberModel
            self.form.rowBy(tag: tagRowNameMember)?.updateCell()
        return}
        self.task.customer = model
        self.form.rowBy(tag: tagRowNameCustomer)?.updateCell()
    }
}
extension FormCreateNewTaskController : ServicesControllerDelegate{
    func didSelect(services: [ServiceModel]?) {
        guard var newServies = services else {return}
        for (i,value) in newServies.enumerated(){
            self.task.details?.forEach({ (service) in
                if service.id == value.id {
                    newServies[i].quantity = service.quantity
                }
            })
        }
        self.task.details = newServies
        
        self.form.sectionBy(tag: tagSectionServices)?.reload()
        self.updateTotalPrice()
    }
}
extension FormCreateNewTaskController: TotalPriceViewDelegate{
    func didSelectedDiscount(total: Double) {
        self.task.total = total.description
    }
    
    func didDeselectedDiscount() {
        self.task.total = calculationTotal()?.description
    }
    
}
extension FormCreateNewTaskController:PercentAndDiscountControllerDelegate{
    func didPressDone(discount: Double) {
        self.task.discount = discount == 0 ? nil : discount.description
        self.form.sectionBy(tag: tagSectionTotal)?.reload()
    }
}
extension FormCreateNewTaskController:CellDelegate{
    func didPressDelete(index: Int) {
        guard let collectionView = self.getCollectionView() else {return}
        collectionView.performBatchUpdates({
            self.task.details?.remove(at: index)
            collectionView.deleteItems(at: [IndexPath(row: index, section: 0)])
            collectionView.isUserInteractionEnabled = false
        }) { (bool) in
            collectionView.reloadData()
            collectionView.isUserInteractionEnabled = true
        }
        self.updateTotalPrice()
    }
    func didEndEditing(textField: UITextField) {
        self.task.details?[textField.tag].quantity = textField.text
        self.updateTotalPrice()
    }
    
}

//
//  ServiceSelectedCell.swift
//  neoPOS
//
//  Created by mac on 9/14/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class ServiceSelectedCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var titleService: RecommentLabel!
    @IBOutlet weak var descriptionService: NormalLabel!
    @IBOutlet weak var textFieldQuantity: UITextField!
    @IBOutlet weak var priceService: RecommentLabel!
    @IBOutlet weak var totalPrice: RecommentDiscountLabel!
    @IBOutlet weak var deleteButton: StyleDefault!
    @IBOutlet weak var minusQuantityButton: StyleDefault!
    @IBOutlet weak var addQuantityButton: StyleDefault!
    
    //MARK:- Declare
    var service:ServiceModel?
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.backgroundColor = .white
        
        self.textFieldQuantity.font = UIFont.recommentFont
        self.textFieldQuantity.text = "1"
        self.textFieldQuantity.keyboardType = .numberPad
        self.textFieldQuantity.delegate = self
        
        self.deleteButton.fontTitle = UIFont.recommentFont
        self.deleteButton.colorTitle = .red
        self.deleteButton.borderWidth = 1
        self.deleteButton.borderColor = .red
        
        self.minusQuantityButton.fontTitle = UIFont.recommentFont
        self.minusQuantityButton.borderColor =  .mainColor
        self.minusQuantityButton.borderWidth = 1
        
        self.addQuantityButton.fontTitle = UIFont.recommentFont
        self.addQuantityButton.borderColor =  .mainColor
        self.addQuantityButton.borderWidth = 1
    }
    func setupDataSource(service:ServiceModel?) {
        self.textFieldQuantity.tag = self.tag
        self.service = service
        self.titleService.text = service?.title
        self.descriptionService.text = service?.descriptionValue
        self.updatePrice()
    }
    func updatePrice() {
        let price = (service?.price).asDouble
//        let stringPrice = NSMutableAttributedString(string: "Đơn giá: " + price.withCommas())
//        stringPrice.addAttribute(.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 0, length: 8))
        priceService.text = price.withCommas()
        self.textFieldQuantity.text = self.service?.quantity ?? 1.description
        let quantityFormTextFied = self.textFieldQuantity.text.asDouble
        totalPrice.text = (price * (quantityFormTextFied <= 0 ? 1 : quantityFormTextFied)).withCommas()
    }
    //MARK:- Outlet Actions
    @IBAction func aDeleteButton(_ sender: Any) {
        self.delegate?.didPressDelete!(index: self.tag)
    }
    @IBAction func aMinusQuantityButton(_ sender: Any) {
        guard let quantity = self.service?.quantity else {return}
        guard var number = Int(quantity) else {return}
        number -= 1
        if number <= 0 {
            return
        }
        self.service?.quantity = number.description
        self.updatePrice()
        self.delegate?.didEndEditing!(textField: self.textFieldQuantity)
    }
    @IBAction func aAddQuantityButton(_ sender: Any) {
        guard let quantity = self.service?.quantity else {return}
        guard var number = Int(quantity) else {return}
        number += 1
        self.service?.quantity = number.description
        self.updatePrice()
        self.delegate?.didEndEditing!(textField: self.textFieldQuantity)
        
    }
}
extension ServiceSelectedCell:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text.asDouble > 0 {
            self.service?.quantity = textField.text
            self.delegate?.didEndEditing!(textField: textField)
        }else {
           textField.text = 1.description
        }
        updatePrice()
    }
}

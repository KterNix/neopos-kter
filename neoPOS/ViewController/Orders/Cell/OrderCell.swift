//
//  OrderCell.swift
//  neoPOS
//
//  Created by mac on 8/23/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class OrderCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var statusOrder: StatusLabel!
    @IBOutlet weak var totalPayment: RecommentDiscountLabel!
    @IBOutlet weak var timeOrder: TitleLabel!
    @IBOutlet weak var dayOrder: RecommentLabel!
    @IBOutlet weak var codeOrder: RecommentLabel!
    @IBOutlet weak var nameCustomer: RecommentLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        
    }
    func setupDataSource(order:OrderModel) {
        if let propertiesStatus = DetailOrderFactory.shared.getStatusProperties(status: order.status)  {
            
            self.statusOrder.text = propertiesStatus.keys.first
            self.statusOrder.color = propertiesStatus.values.first
            
        }
        
        self.codeOrder.text = order.code
        if let date = Date().toDate(string:order.created.asString) {
            let hour = Calendar.current.component(.hour, from: date)
            let minute = Calendar.current.component(.minute, from: date)
            let day = Calendar.current.component(.day, from: date)
            let month = Calendar.current.component(.month, from: date)
            self.dayOrder.text = day.description + "/" + month.description
            self.timeOrder.text = hour.description + ":" + minute.description
        }
        self.nameCustomer.text = order.customer?.fullname ?? "Khách vãng lai"
        self.totalPayment.text = (order.total).asDouble.withCommas()
    }
    
    //MARK:- Outlet Actions

}

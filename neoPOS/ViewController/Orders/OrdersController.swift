//
//  OrdersController.swift
//  neoPOS
//
//  Created by mac on 8/22/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import ObjectMapper
import PopupDialog
private let idCell = "OrderCell"

class OrdersController: UIViewController {

    
    //MARK:- Outlet
    @IBOutlet weak var segmentStatus: UISegmentedControl!
    @IBOutlet weak var searchBarView: SearchDefault!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topSegmentConstraint: NSLayoutConstraint!
    
    
    //MARK:- Declare
    var isPressedSearch = false
    var page = 0
    var fromdate : Date?
    var todate : Date?
    var orders:[OrderModel]?
    var ordersFiltered:[OrderModel]?
    var typeFilter = TypeDateToSelect.Today
    var textSearch:String?
    var status = OrderStatus.All
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        getData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkDataForDraft()
    }
    //MARK:- SubFunction
    func checkDataForDraft() {
        if status == .Draft {
            self.parseDataForDraft()
        }
    }
    func setupUI() {
        self.navigationItem.title = "Hôm nay"
        self.collectionView.backgroundColor = .groupTableViewBackground
        
        self.segmentStatus.tintColor = .orange
        self.createBarItem(title: nil, imageName: "caretdown", selector: #selector(self.didPressFilter))
        self.createBarItem( title: "+", imageName: nil, selector: #selector(self.didPressCreateOrder), fontSize: 25)
        self.collectionView.addInfiniteScroll { (collection) in
            if self.status != .Draft {
                collection.isUserInteractionEnabled = false
                self.getData()
            }else {
                self.completedProgressingGetData()
            }
        }
        self.createBarItem(forLeft:true, title: nil, imageName: "search", selector: #selector(self.didPressSearch))
    }
    @objc func didPressCreateOrder() {
        let vc = CartController()
        vc.order = OrderModel()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func didPressFilter() {
        let vc = FilterForPayment_Receipt()
        vc.delegate = self
        let popup = PopupDialog(viewController: vc)
        let doneBtn = DefaultButton(title: "Xong") { [weak self] in
            self?.fromdate = vc.fromdate
            self?.todate = vc.todate
            self?.typeFilter = .Custom
            self?.page = 0
            self?.orders = nil
            self?.getData()
        }
        doneBtn.setTitleColor(.white, for: .normal)
        doneBtn.backgroundColor = .orange
        popup.addButton(doneBtn)
        self.present(popup, animated: true, completion: nil)
    }
    @objc func didPressSearch() {
        isPressedSearch = !isPressedSearch
        let topContraint:CGFloat = isPressedSearch ? self.searchBarView.height : 0
//        guard topContraint != self.topSegmentConstraint.constant else {return}
        self.topSegmentConstraint.constant = topContraint
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    func checkTypeFilterToSetTitle() {
        var title = ""
        switch self.typeFilter {
        case .Yesterday:
            title = "Hôm qua"
        case .ThisWeek:
            title = "Tuần này"
        case .ThisMonth:
            title = "Tháng này"
        case .PreviousMonth:
            title = "Tháng trước"
        case .Custom:
            title = "\((fromdate?.toString(dateFormat: "dd/MM/yyyy"))!) đến \((todate?.toString(dateFormat: "dd/MM/yyyy"))!)"
        default:
            title = "Hôm nay"
        }
        self.navigationItem.title = title
    }
    func setupDataSource() {
        self.todate = Date()
        self.fromdate = Date()
        self.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    func getData()  {
        let todate = self.todate?.toString(dateFormat: "yyyy-MM-dd")
        let fromdate = self.fromdate?.toString(dateFormat: "yyyy-MM-dd")

        let param = ["page":page.description,"fromdate":fromdate.asString,"todate":todate.asString,"status": self.status.rawValue]
        RequestHelper.shared.getOrders(param: param, vc: self) { [weak self](dic, error) in
            self?.completedProgressingGetData()
            guard let newDic = dic else {
                self?.checkAndCreateEmptyView()
                return}
            guard let orders = Mapper<OrderModel>().mapArray(JSONObject: newDic["orders"]), !orders.isEmpty else {
                self?.checkAndCreateEmptyView()
                return}
            self?.page += 1
            self?.removeEmptyView(onItem: self?.collectionView ?? UIView())
            let oldCount = self?.orders?.count
            self?.orders == nil ? self?.orders = orders : self?.orders?.append(contentsOf: orders)
            if self?.status == .All {
                self?.setupDataFor(allOrders: (self?.orders)!)
            }
            let newCount = self?.orders?.count
            self?.insertIndexPaths(oldCount: oldCount ?? 0 , newCount: newCount ?? 0)
        }
    }
    func checkAndCreateEmptyView() {
        if self.page == 0 {
            self.createEmptyView(onItem: self.collectionView, didTryAgain: {
                if self.status != .Draft {
                    self.getData()
                }else {
                    self.parseDataForDraft()
                }
            })
        }
    }
    func completedProgressingGetData() {
        self.collectionView.isUserInteractionEnabled = true
        self.collectionView.finishInfiniteScroll()
    }
    func insertIndexPaths(oldCount:Int,newCount:Int)  {
        
        if newCount > oldCount {
            let range = newCount - oldCount
            var indexs = [IndexPath]()
            for i in 0..<range {
                let indexPath = IndexPath(item: oldCount + i, section: 0)
                indexs.append(indexPath)
            }
            self.collectionView.performBatchUpdates({
                self.collectionView.insertItems(at: indexs)
            }, completion: { (bool) in
                self.completedProgressingGetData()
            })
            return
        }
        self.completedProgressingGetData()
        
    }
    func setupDataFor(allOrders:[OrderModel]) {
        self.orders =  allOrders
        if let draftOrders = Utils.shared.getDraftOrders() {
            self.orders = self.addDraftOrdersForAll(allOrders: allOrders, draftOrders: draftOrders)
        }
    }
    func parseDataForDraft() {
        self.orders = nil
        self.collectionView.reloadData()
        guard let orders = Utils.shared.getDraftOrders() else {
            self.checkAndCreateEmptyView()
            return }
        self.orders = orders
        if !orders.isEmpty {
            self.removeEmptyView(onItem: self.collectionView)
            self.collectionView.reloadData()
        }
    }
    func addDraftOrdersForAll(allOrders:[OrderModel],draftOrders:[OrderModel]) -> [OrderModel] {
        var orders = allOrders
        orders.append(contentsOf: draftOrders)
        orders = orders.sorted(by: { Date().toDate(string: $0.created.asString) ?? Date() <  Date().toDate(string: $1.created.asString) ?? Date()})
        return orders
    }
    //MARK:- Outlet Actions
    @IBAction func aSegmentStatus(_ sender: UISegmentedControl) {
        self.orders?.removeAll()
        self.collectionView.reloadData()
        self.page = 0
        switch sender.selectedSegmentIndex {
        case 0:
            self.status = .All
        case 1:
            self.status = .Pending
        case 2:
            self.status = .Completed
        case 3:
            self.status = .Progressing
        case 4:
            self.status = .New
        default:
            self.status = .Draft
            self.parseDataForDraft()
            return
        }
        self.getData()
    }
    
}
extension OrdersController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if scrollView.contentOffset.y < -30 {
//            self.isPressedSearch = true
//            self.didPressSearch()
//        }else if scrollView.contentOffset.y > 0{
//            self.isPressedSearch = false
//            self.didPressSearch()
//        }
//    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let models = self.ordersFiltered != nil ? self.ordersFiltered : self.orders

        return models?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! OrderCell
        let models = self.ordersFiltered != nil ? self.ordersFiltered : self.orders

        cell.setupDataSource(order: models![indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let models = self.ordersFiltered != nil ? self.ordersFiltered : self.orders
        let model = models![indexPath.row]
        guard model.id != nil else {
            let vc = CartController()
            vc.order = model
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        let vc = DetailOrderController()
        vc.order = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
extension OrdersController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.dismissKeyboard()
        searchBar.text = nil
        self.ordersFiltered = nil
        self.collectionView.reloadData()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.dismissKeyboard()
        guard let text = searchBar.text , !text.isEmpty else {
            self.ordersFiltered = nil
            self.collectionView.reloadData()
            return}
        self.textSearch = text
        self.getData()
    }
    
}
extension OrdersController : FilterForPayment_ReceiptDelegate{
    func didPressChangeTypeDate(fromdate: Date?, todate: Date?, type:TypeDateToSelect) {
        self.fromdate = fromdate
        self.todate = todate
        self.typeFilter = type
        self.page = 0
        self.orders = nil
        self.getData()
        self.checkTypeFilterToSetTitle()
    }
    
    
}

//
//  OrderProductCell.swift
//  neoPOS
//
//  Created by mac on 8/28/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class OrderProductCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var codeProduct: TitleLabel!
    @IBOutlet weak var nameProduct: RecommentLabel!
    @IBOutlet weak var price: RecommentLabel!
    @IBOutlet weak var quantity: RecommentLabel!
    @IBOutlet weak var totalPrice: RecommentDiscountLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.backgroundColor = .white
    }
    func setupDataSource(product:ProductModel) {
        let price = (product.sizeWareHouse?.pricekm).asDouble == 0 ? (product.sizeWareHouse?.price).asDouble : (product.sizeWareHouse?.pricekm).asDouble
        self.codeProduct.text = product.code
        self.nameProduct.text = product.title
        self.price.text = price.withCommas()
        self.totalPrice.text = (price * (product.sizeWareHouse?.quantity).asDouble).withCommas()
        self.quantity.text = product.sizeWareHouse?.quantity
    }
    
    //MARK:- Outlet Actions
}

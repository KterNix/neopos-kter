//
//  DetailOrderFactory.swift
//  neoPOS
//
//  Created by mac on 9/26/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import Foundation
import UIKit
class DetailOrderFactory {
    static let shared = DetailOrderFactory()
    func getStatusProperties(status:String?) -> [String:UIColor]? {
        guard let status = OrderStatus(rawValue: status.asString) else  { return nil }
        var color = UIColor.white
        var text = ""
        switch status {
        case .Pending:
            text = "Tạm ngưng"
            color = UIColor.Social.googlePlus
        case .Completed:
            text = "Hoàn thành"
            color = UIColor.Social.linkedIn
            
        case .Progressing:
            text = "Đang xử lý"
            color = UIColor.orange
            
        case .New:
            text = "Mới"
            color = UIColor.black
            
        default:
            text = "Nháp"
            color = .lightGray
            
        }
        return [text:color]
    }
}

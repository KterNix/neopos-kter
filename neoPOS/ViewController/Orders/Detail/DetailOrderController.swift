//
//  DetailOrderController.swift
//  neoPOS
//
//  Created by mac on 8/24/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
import ObjectMapper
import TSMessages

private let tagSectionProducts = "tagSectionProducts"
private let idCell = "OrderProductCell"
private let heightCell:CGFloat = 140
 
class DetailOrderController: MasterFormController {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    var order: OrderModel?
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        checkIsDetailOrOrderForSetUpForm()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func setUpForm() {
        form
        +++ Section("Thông tin đơn hàng")
            <<< TextRow(){
                $0.title = "Mã đơn hàng"
                $0.value = self.order?.code
                $0.hidden = self.order?.code == nil ? true : false
                $0.evaluateHidden()
            }
            <<< TextRow(){
                $0.title = "Ngày tạo"
                $0.value = self.order?.created
                $0.hidden = self.order?.code == nil ? true : false
                $0.evaluateHidden()
            }
//            <<< TextRow(){
//                $0.title = "Voucher"
//                $0.value = self.order?.voucher
//                }.cellUpdate({ (cell, row) in
//                    self.order?.voucher = row.value
//                })
            <<< PickerInlineRow<String>(){
                $0.title = "Trạng thái"
                $0.value = DetailOrderFactory.shared.getStatusProperties(status: self.order?.status)?.first?.key ?? "Nháp"
                $0.options = ["Mới","Đang xử lý","Đã xong","Tạm ngưng","Nháp"]
                $0.hidden = self.order?.code == nil ? true : false
                $0.evaluateHidden()
            }
            <<< IntRow(){
                $0.title = "Giảm giá"
                $0.value = (self.order?.discount).asInt
                }.cellUpdate({ (cell, row) in
                    self.order?.discount = row.value?.description
                })
            <<< IntRow(){
                $0.title = "Tổng tiền"
                $0.value = (self.order?.total).asInt
                }
            <<< ButtonRow(){
                let text = (self.order?.pstatus).asString == "1" ? "Đã thanh toán" : "Chưa  thanh toán"
                $0.title = text
                $0.hidden = !self.isDetailOrder() ? true : false
                $0.evaluateHidden()
                }.cellUpdate({ (cell, row) in
                    let color:UIColor = (self.order?.pstatus).asString == "1" ? .mainColor : .red
                    cell.textLabel?.textColor = color
                })
        +++ Section("Thông tin khách hàng")
            <<< SwitchRow(){
                $0.title = "Khách vãng lai?"
                $0.value = false
                self.order?.otype = 1.description
                }.onChange({ (row) in
                    guard let value = row.value else {return}
                    self.order?.otype = value ? "0" : "1"
                    self.form.rowBy(tag: "customer")?.hidden = Condition(booleanLiteral: value)
                    self.form.rowBy(tag: "customer")?.evaluateHidden()
                })
            <<< PushRow<String>(){
                $0.title = "Khách hàng"
                $0.disabled = true
                $0.tag = "customer"
                }.onCellSelection({ (cell, row) in
                    let vc = CustomersController()
                    vc.delegate = self
                    self.navigationController?.pushViewController(vc, animated: true)
                }).cellUpdate({ (cell, row) in
                    row.value = self.order?.customer?.fullname
                })
//            <<< PhoneRow(){
//                $0.title = "SĐT"
//                $0.value = self.order?.customer?.mobile
//            }
//            <<< IntRow(){
//                $0.title = "Thẻ VIP"
//                $0.value = (self.order?.customer?.vipCode).asInt
//            }
        
            +++ Section("Sản phẩm đã đặt"){ section in
                section.tag = tagSectionProducts
                var header = HeaderFooterView<HeaderTitileWithButton>(.nibFile(name:"HeaderTitileWithButton",bundle:nil))
                header.onSetupView = {view, _ in
                    view.title.text = "Sản phẩm đã đặt".uppercased()
                    view.btnMore.addTarget(self, action: #selector(self.didPressAddMore), for: .touchUpInside)
                    view.btnMore.isHidden = self.isDetailOrder() ? true : false
                }
                header.height = {50}
                section.header = header
                
                var footer = HeaderFooterView<CollectionViewForEureka>(.nibFile(name:"CollectionViewForEureka",bundle:nil))
                
                footer.onSetupView = { view,_ in
                    view.collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
                    view.collectionView.delegate = self
                    view.collectionView.dataSource = self
                }
//                let count = CGFloat(self.order?.details?.count ?? 0)
                //count*heightCell +  count*5 + 5
                footer.height = { self.view.height - Constants.HeightTool.tabbar - 50 }
                section.footer = footer
        }
    }
    deinit {
        print("\(self) deinit =================")
    }
    //MARK:- SubFunction
    func setupUI() {
        self.title = "Chi tiết đơn hàng"
        self.setBackButton()
        if isDetailOrder() {
//            self.createBarItem( title: "Cập nhật", imageName: nil, selector: #selector(self.didPressUpdateOrder), fontSize: 13)
        }else {
            self.createBarItem( title: "Tạo", imageName: nil, selector: #selector(self.didPressCreateOrder), fontSize: 13)
        }
    }
    func isDetailOrder() -> Bool {
        guard self.order?.id != nil  else { return false }
        return true
    }
    func checkIsDetailOrOrderForSetUpForm() {
        guard let idOrder = self.order?.id else {
            setupDataSource()
            return}
        self.getDetailOrder(id: idOrder)
    }
    func getDetailOrder(id:String) {
        RequestHelper.shared.getOrder(id: id, vc: self) {[weak self] (dic, error) in
            guard let order = Mapper<OrderModel>().map(JSONObject: dic?["order"]) else {return}
            guard let sizesInWareHouse = Mapper<SizeProdcutInWareHouse>().mapArray(JSONObject: (dic?["order"] as? Dictionary)?["details"])  else {return}
            order.details?.removeAll()
            sizesInWareHouse.forEach({ (size) in
                var product = ProductModel()
                product.title = size.title
                product.code =  size.pcode
                product.sizeWareHouse = size
                order.details == nil ? order.details = [product] : order.details?.append(product)
            })
            self?.order = order
            self?.setupDataSource()
        }
    }
    func setupDataSource() {
        setUpForm()
    }
    @objc func didPressUpdateOrder() {
            
    }
    @objc func didPressCreateOrder() {
        let param = self.parseOrderToJSON()
        RequestHelper.shared.postOrder(param: param, vc: self) { (dic, error) in
            guard dic != nil else {return}
            TSMessage.showNotification(in: self, title: "Tạo đơn hàng thành công!", subtitle: "Đã tạo thành công vui lòng tạo đơn hàng khác.", type: .success, duration: 5, canBeDismissedByUser: true)
        }
    }
    func parseOrderToJSON() -> [String:String] {
        var param = ["discount":(order?.discount).asString,
                     "total":(order?.total).asString,
                     "voucher":(order?.voucher).asString,
                     "usecard":(order?.usecard).asString,
                     "status": 0.description,
                     "pmethod": "CASH",
                     "pstatus": "1",
                     "otype": (order?.otype).asString,
                     ]
        if order?.otype != 0.description {
            param["cid"] = (order?.customer?.id).asString
        }
        guard let products = order?.details else {return param}
        let sizes = products.map{$0.sizeWareHouse ?? SizeProdcutInWareHouse()}
        param["details"] = sizes.toJSONString()
        return param
    }
    @objc func didPressAddMore() {
        let vc = ProductsController()
        vc.type = .Order
        vc.order = self.order
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Outlet Actions

}
extension DetailOrderController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.order?.details?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! OrderProductCell
        cell.setupDataSource(product: (self.order?.details?[indexPath.row])!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10, height: heightCell)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
extension DetailOrderController:CustomersControllerDelegate{
    func didSelect(customer: Any, sender: CustomersController) {
        self.order?.customer = customer as? CustomerModel
        self.form.rowBy(tag: "customer")?.updateCell()
    }
}
extension DetailOrderController:ProductsControllerDelegate {
    func didSelected(product: ProductModel) {
        ((self.form.sectionBy(tag: tagSectionProducts)?.footer?.viewForSection(self.form.sectionBy(tag: tagSectionProducts)!, type: .footer)) as? CollectionViewForEureka)?.collectionView.reloadData()
    }
}

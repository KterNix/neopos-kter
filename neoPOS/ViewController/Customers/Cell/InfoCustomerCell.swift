//
//  InfoCustomerCell.swift
//  neoPOS
//
//  Created by mac on 8/30/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class InfoCustomerCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var nameCustomer: TitleLabel!
    @IBOutlet weak var mobileCustomer: RecommentLabel!
    @IBOutlet weak var addressCustomer: RecommentLabel!
    @IBOutlet weak var emailCustomer: NormalLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.addressCustomer.color = .lightGray
        self.backgroundColor =  .white
    }
    func setupDataSource(customer:CustomerModel) {
        self.nameCustomer.text = customer.fullname
        self.mobileCustomer.text = customer.mobile
        self.emailCustomer.text = customer.email
        self.addressCustomer.text = customer.address
    }
    func setupDataSource(member:MemberModel) {
        self.nameCustomer.text = member.fullname
        self.mobileCustomer.text = member.mobile
        self.emailCustomer.text = member.email
        self.addressCustomer.text = member.address
    }
    //MARK:- Outlet Actions
    
}

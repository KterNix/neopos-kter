//
//  CustomersController.swift
//  neoPOS
//
//  Created by mac on 8/31/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import ObjectMapper

private let idInforCustomerCell = "InfoCustomerCell"

protocol CustomersControllerDelegate: AnyObject{
    func didSelect(customer: Any, sender: CustomersController)
}

class CustomersController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var searchBar: SearchDefault!
    @IBOutlet weak var collectionCustomers: UICollectionView!
    
    
    //MARK:- Declare
    var customers:[CustomerModel]?
    var customersFiltered: [CustomerModel]?
    var members:[MemberModel]?
    var membersFiltered:[MemberModel]?
    var page = 0
    weak var delegate:CustomersControllerDelegate?
    var typeUser:AccountType = .Customer
    var typeClass:TypeList = .Select
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        
        self.title = self.typeUser == .Customer ? "Khách hàng" : "Nhân viên"
        self.setBackButton()
        self.collectionCustomers.backgroundColor = .groupTableViewBackground
        self.searchBar.setShowsCancelButton(true, animated: true)
        self.searchBar.delegate = self
        self.searchBar.placeholder = self.typeUser == .Customer ? "Tên khách" : "Tên nhân viên"
        self.collectionCustomers.addInfiniteScroll { (collection) in
            self.setupDataSource()
        }
        if typeUser == .Customer {
            self.createBarItem( title: "+", imageName: nil, selector: #selector(self.didPressAddMoreCustomer), fontSize: 25)
        }
        self.collectionCustomers.register(UINib(nibName: idInforCustomerCell, bundle: nil), forCellWithReuseIdentifier: idInforCustomerCell)
        self.collectionCustomers.delegate = self
        self.collectionCustomers.dataSource = self
    }
    @objc func didPressAddMoreCustomer() {
        let vc = DetailCustomerController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func setupDataSource() {
        let param = ["page": page.description]
        self.getCustomersWith(param: param) { (users) in
            self.collectionCustomers.finishInfiniteScroll()
            guard let customers = users as? [CustomerModel] else {
                // Neu la thanh vien
                guard let members = users as? [MemberModel] else {return}
                self.members == nil ? self.members = members : self.members?.append(contentsOf: members)
                self.collectionCustomers.reloadData()
                self.page += 1
                
                return}
            // Neu la khach hang
            self.customers == nil ? self.customers = customers : self.customers?.append(contentsOf: customers)
            self.collectionCustomers.reloadData()
            self.page += 1
        }
    }
    func getCustomersWith(param:[String:Any],completion:@escaping ([Any]?) -> Void){
        RequestHelper.shared.getUser(type:self.typeUser ,param: param, vc: self) { (dic, error) in
            guard let newDic = dic else { completion(nil)
                return }
            switch self.typeUser{
            case .Customer:
                guard let customers = Mapper<CustomerModel>().mapArray(JSONObject: newDic["customers"]),!customers.isEmpty else { completion(nil)
                    return }
                completion(customers)
            case .Member:
                guard let customers = Mapper<MemberModel>().mapArray(JSONObject: newDic["members"]),!customers.isEmpty else { completion(nil)
                    return }
                completion(customers)
            }
            
        }
    }
    
    //MARK:- Outlet Actions

}
extension CustomersController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch typeUser {
        case .Customer:
            return self.customersFiltered != nil ?  self.customersFiltered?.count ?? 0 : self.customers?.count ?? 0
        default:
            return self.membersFiltered != nil ?  self.membersFiltered?.count ?? 0 : self.members?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cellInfor = collectionView.dequeueReusableCell(withReuseIdentifier: idInforCustomerCell, for: indexPath) as? InfoCustomerCell else {return MasterCollectionViewCell() }
        switch self.typeUser {
        case .Customer:
            if let model = self.customersFiltered == nil ? self.customers?[indexPath.row] : self.customersFiltered?[indexPath.row] {
                cellInfor.setupDataSource(customer: model)
            }
        default:
            if let model = self.membersFiltered == nil ? self.members?[indexPath.row] : self.membersFiltered?[indexPath.row] {
                cellInfor.setupDataSource(member: model)
            }
        }
        
        return cellInfor
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var model: Any?
        var vc = UIViewController()
        switch self.typeUser {
        case .Customer:
             model = self.customersFiltered == nil ? self.customers?[indexPath.row] : self.customersFiltered?[indexPath.row]
            let vcDetail = DetailCustomerController()
             vcDetail.customer = model as? CustomerModel
            vc = vcDetail
        default:
            model = self.membersFiltered == nil ? self.members?[indexPath.row] : self.membersFiltered?[indexPath.row]
            let vcDetail = DetailMemberController()
            vcDetail.member = model as? MemberModel
            vc = vcDetail
        }
        switch self.typeClass {
        case .Select:
            delegate?.didSelect(customer: model!, sender: self)
            self.navigationController?.popViewController(animated: true)
        case .View:
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
        

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.width - 10, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

extension CustomersController:UISearchBarDelegate{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.customersFiltered = nil
        self.collectionCustomers.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let param = ["keyword":searchBar.text.asString]
        self.getCustomersWith(param: param) { (users) in
            guard let customers = users as? [CustomerModel] else {
                
                guard let members = users as? [MemberModel] else {return}
                self.membersFiltered =  members
                self.collectionCustomers.reloadData()
                
                return}
            
            self.customersFiltered = customers
            self.collectionCustomers.reloadData()
        }
    }
}

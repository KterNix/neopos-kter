//
//  DetailCustomerController.swift
//  neoPOS
//
//  Created by mac on 9/28/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
import Eureka
import TSMessages
class DetailCustomerController: MasterFormController {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    var customer:CustomerModel?
    var type = TypeDetail.Create
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setUpForm()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func setUpForm() {
        form
        +++ Section("Chi tiết")
            <<< TextRow(){
                $0.title = "Tên"
                $0.value = customer?.fullname
                }.cellUpdate({ (cell, row) in
                    self.initModel()
                    self.customer?.fullname = row.value
                })
            <<< PickerInlineRow<String>(){
                $0.title = "Giới tính"
                $0.value = customer?.gender == 1.description ? "Nam" : "Nữ"
                $0.options = ["Nam", "Nữ"]
                }.cellUpdate({ (cell, row) in
                    self.initModel()
                    self.customer?.gender = row.value == "Nam" ? 1.description : 2.description
                })
            <<< PhoneRow(){
                $0.title = "Số điện thoại"
                $0.value = customer?.mobile
                }.cellUpdate({ (cell, row) in
                    self.initModel()
                    self.customer?.mobile = row.value
                })
            <<< EmailRow(){
                $0.title = "Email"
                $0.value = customer?.email
                }.cellUpdate({ (cell, row) in
                    self.initModel()
                    self.customer?.email = row.value
                })
            +++ Section("Địa chỉ")
            <<< TextAreaRow(){
                $0.value = customer?.address
                }.cellUpdate({ (cell, row) in
                    self.initModel()
                    self.customer?.address = row.value
                })
        
    }
    //MARK:- SubFunction
    func setupUI() {
        setBackButton()
        if self.customer?.id == nil {
            self.createBarItem( title: "Tạo", imageName: nil, selector: #selector(self.didPressCreateCustomer), fontSize: 13)
            self.title = "Tạo mới khách hàng"

        }else {
            self.createBarItem( title: "Cập nhật", imageName: nil, selector: #selector(self.didPressUpdateCustomer), fontSize: 13)
            self.title = "Chi tiết khách hàng"

        }
    }
    func setupDataSource() {
        
    }
    func initModel() {
        self.customer = self.customer == nil ? CustomerModel() : self.customer
        self.customer?.gender = self.customer?.gender == nil ? 2.description : self.customer?.gender
    }
    func isPropertyNotNil() -> Bool {
        let properties = [customer?.address,customer?.gender,customer?.mobile,customer?.fullname,customer?.email]
        for value in properties {
            if value == nil {
                return false
            }
        }
        return true
    }
    @objc func didPressUpdateCustomer() {
        RequestHelper.shared.putCustomer(customer: self.customer, vc: self) { (dic, error) in
            guard dic != nil else {return}
            TSMessage.showNotification(withTitle: "Cập nhật thành công!", type: .success)
        }
    }
    @objc func didPressCreateCustomer() {
        guard isPropertyNotNil() else {
            TSMessage.showNotification(withTitle: "Vui lòng nhập đầy đủ thông tin. Khách hàng ko có dữ liệu nào vui lòng nhập \"Không\"", type: .error)
            return}
        guard let param = self.customer?.toJSON() else {return}
        RequestHelper.shared.postCustomer(param: param, vc: self) { (dic, error) in
            guard dic != nil else {return}
            TSMessage.showNotification(withTitle: "Tạo khách hàng \((self.customer?.fullname).asString) thành công!", type: .success)
            
        }
    }
    //MARK:- Outlet Actions

}

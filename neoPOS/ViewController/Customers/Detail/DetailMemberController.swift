//
//  DetailMemberController.swift
//  neoPOS
//
//  Created by mac on 9/28/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class DetailMemberController: MasterFormController {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    var member:MemberModel?
    
    //MARK:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.title = ""
        setBackButton()
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions
}

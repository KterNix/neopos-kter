//
//  StyleRecomment.swift
//  POS
//
//  Created by mac on 4/3/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class StyleRecomment: UIButton {
    var text :String?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }
    override init(frame: CGRect) {
        super.init(frame:frame )
        self.setupUI()
    }
    func setupUI() {
        self.setAttributedTitle(NSAttributedString(string: text == nil ? (self.titleLabel?.text).asString : text.asString, attributes: [NSAttributedString.Key.font : UIFont.recommentFont, NSAttributedString.Key.foregroundColor : UIColor.mainColor]), for: .normal)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

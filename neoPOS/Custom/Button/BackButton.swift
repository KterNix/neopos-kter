//
//  BackButton.swift
//  POS
//
//  Created by mac on 4/3/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class BackButton: UIButton {
    var color:UIColor?
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    func setupUI() {
//        self.setAttributedTitle(NSAttributedString(string: "←", attributes: [NSAttributedStringKey.font : UIFont.backButtonFont, NSAttributedStringKey.foregroundColor : color ?? UIColor.mainColor ]), for: .normal)
        self.tintColor = color ?? .mainColor
        self.setImage(UIImage(named: "arrowLeft")?.resize(width: 20, height: 20)?.withRenderingMode(.alwaysTemplate), for: .normal)
    }
    func setupDataSource() {
        
    }
    convenience init() {
        self.init()
        self.setupUI()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

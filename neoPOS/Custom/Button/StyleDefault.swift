//
//  StyleDefault.swift
//  POS
//
//  Created by mac on 4/3/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class StyleDefault: UIButton {
    var colorTitle:UIColor?{
        didSet{
            setupUI()
        }
    }
    var title:String?{
        didSet{
            setupUI()
        }
    }
    var fontTitle:UIFont?{
        didSet{
            setupUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        
    }
    func setUpWithBackground(color: UIColor) {
        setupUI()
        self.backgroundColor = color
    }
    func setupUI() {
        self.setAttributedTitle(NSAttributedString(string: title ?? (self.titleLabel?.text).asString, attributes: [NSAttributedString.Key.font : fontTitle ?? UIFont.buttonFont, NSAttributedString.Key.foregroundColor : colorTitle ?? UIColor.mainColor ]), for: .normal)
        self.setDefault()
    }
    func setDefault()  {
        self.tintColor = UIColor.white
        self.layer.cornerRadius = DefaultSetting.radius
        self.imageEdgeInsets = UIEdgeInsets.init(top: 10,left: 10,bottom: 10,right: 10)
        self.setImage(self.currentImage?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.layer.masksToBounds = true
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}

//
//  GradiantView.swift
//  neoReal
//
//  Created by mac on 4/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class GradiantView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
    }
    func setupUI() {
        let gradient = CAGradientLayer()
        let frame = CGRect(origin: self.frame.origin, size: CGSize(width: self.frame.width, height: self.frame.height))
        gradient.frame = frame
        gradient.colors = [UIColor.mainColor.cgColor]
        self.layer.insertSublayer(gradient, at: 0)
        
    }
    func setupDataSource() {
        
    }

}

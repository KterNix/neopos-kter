//
//  ViewEmpty.swift
//  SB App
//
//  Created by mac on 8/13/18.
//

import UIKit

class ViewEmpty: UICustomView {
    
    //MARK:- Outlet
    @IBOutlet weak var btnTryAgain: StyleDefault!
    
    
    //MARK:- Declare
    var didTryAgian: (() -> Void)?
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        setupUI()
    }
    //MARK:- SubFunction
    func setupUI() {
        self.backgroundColor = UIColor.groupTableViewBackground
//        self.imageEmpty.tintColor = .lightGray
//        self.imageEmpty.image = imageEmpty.image?.withRenderingMode(.alwaysTemplate)
        self.btnTryAgain.colorTitle = .secondColor
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions
    @IBAction func abtnTryAgain(_ sender: Any) {
        didTryAgian!()
    }
}

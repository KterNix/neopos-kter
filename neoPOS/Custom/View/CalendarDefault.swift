//
//  CalendarDefault.swift
//  neoPOS
//
//  Created by mac on 9/18/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import Foundation
import FSCalendar
class CalendarDefault: FSCalendar {
    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.appearance.selectionColor = .mainColor
        self.appearance.titleWeekendColor = UIColor.Social.googlePlus
        self.appearance.headerTitleColor = .mainColor
        self.appearance.weekdayTextColor = UIColor.Social.googlePlus
        
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions
}

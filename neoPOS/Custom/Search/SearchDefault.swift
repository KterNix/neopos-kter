//
//  SearchDefault.swift
//  neoPOS
//
//  Created by mac on 9/6/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class SearchDefault: UISearchBar {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    override var placeholder: String?{
        didSet{
            for subView in self.subviews {
                for subSubView in subView.subviews {
                    if let _ = subSubView as? UITextInputTraits {
                        let textField = subSubView as! UITextField
                        textField.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "Tìm", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
                    }
                }
            }
        }
    }
    //MARK:- SubFunction
    func setupUI() {
        self.setShowsCancelButton(true, animated: false)
        self.setImage(nil, for: .search, state: .normal)
        self.backgroundColor = .mainColor
        self.tintColor = .white
        self.changeSearchBarColor(color: .secondColor)
        self.barTintColor = .white
        
    }
    
    //MARK:- Outlet Actions

}

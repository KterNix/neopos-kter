//
//  UICustomView.swift
//  LimeSoftware
//
//  Created by KieuVan on 10/11/16.
//  Copyright © 2016 KieuVan. All rights reserved.
//

import UIKit

class UICustomView: UIView {
    @IBOutlet  var view: UIView!
    func classNameAsString(obj: Any) -> String
    {
        return String(describing: type(of: (obj as AnyObject))).replacingOccurrences(of:"", with:".Type")
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        view.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        view.frame = bounds
        addSubview(view)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func  setIPadStyleA()
    {
        
    }
    
    func setIphoneStyleA()
    {
    }
    
    
    func setStyleA()
    {
    }
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:self.classNameAsString(obj: self), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    
}

//
//  TitleLabel.swift
//  POS
//
//  Created by mac on 4/3/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
class TitleLabel: UILabel {
    var color:UIColor?{
        didSet{
            self.setupUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    func setupUI() {
        self.textColor = color ?? UIColor.mainColor
        self.font = UIFont.titleFont
    }
    func setStyleBackGround(color: UIColor) {
        self.backgroundColor = color
        self.layer.cornerRadius  = DefaultSetting.radius
        self.layer.masksToBounds = true
        self.setupUI()
    }
}

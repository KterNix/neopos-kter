//
//  RecommentLabel.swift
//  POS
//
//  Created by mac on 4/3/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class RecommentLabel: UILabel {
    var color:UIColor?{
        didSet{
            self.setupUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    func setupUI() {
        self.textColor = color ?? UIColor.black
        self.font = UIFont.recommentFont

    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
}

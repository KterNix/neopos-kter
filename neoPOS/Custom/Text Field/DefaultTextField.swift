//
//  DefaultTextField.swift
//  neoPOS
//
//  Created by mac on 9/19/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class DefaultTextField: UITextField {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    var leftLabel : NormalLabel?
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    required public init(title:String, isSecure:Bool,frame:CGRect) {
        super.init(frame: frame)
        initLeftView()
        leftLabel?.text = title
        isSecureTextEntry = isSecure
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 8, y: 0, width: 70, height: self.height)
    }
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: 10, height: self.height)
    }
    //MARK:- SubFunction
    func setupUI() {
        self.font  = UIFont.recommentFont
        self.textAlignment = .right
        self.borderStyle = .none
        self.backgroundColor = .white
        self.cornerRadius = 5
        self.initLeftView()
        self.textColor = .mainColor
    }
    func initLeftView() {
        self.leftLabel = NormalLabel(frame:CGRect(x: 0, y: 0, width: 70, height: 20))
        self.leftLabel?.text = self.placeholder
        self.placeholder = nil
        self.leftView = self.leftLabel
        self.leftViewMode = .always
        self.rightView = UIView()
        self.rightViewMode = .always
    }
    
    //MARK:- Outlet Actions

}


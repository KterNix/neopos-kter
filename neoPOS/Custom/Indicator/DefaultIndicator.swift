
//
//  DefaultIndicator.swift
//  neoPOS
//
//  Created by mac on 9/12/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class DefaultIndicator: UIActivityIndicatorView {

    //MARK:- Outlet
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.tintColor = .secondColor
        self.color = .secondColor
        self.tintColorDidChange()
        self.startAnimating()
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}

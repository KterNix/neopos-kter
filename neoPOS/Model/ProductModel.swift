//
//  ProductModel.swift
//
//  Created by mac on 8/15/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public struct ProductModel: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let note = "note"
    static let applycard = "applycard"
    static let id = "id"
    static let code = "code"
    static let ishow = "ishow"
    static let pcateid = "pcateid"
    static let descriptionValue = "description"
    static let title = "title"
    static let pprice = "pprice"
    static let pmin = "pmin"
    static let path = "path"
    static let image = "imagePath"
    static let sizes = "sizes"
    static let sizeWareHouse = "sizeWareHouse"

  }

  // MARK: Properties
  public var status: String?
  public var note: String?
  public var applycard: String?
  public var id: String?
  public var code: String?
  public var ishow: String?
  public var pcateid: String?
  public var descriptionValue: String?
  public var title: String?
  public var pprice: String?
  public var pmin: String?
  public var path: String?
    public var image: String?
    public var sizes: [SizeProductModel]?
    public var sizeWareHouse:SizeProdcutInWareHouse?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }
    init() {
    }
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    note <- map[SerializationKeys.note]
    applycard <- map[SerializationKeys.applycard]
    id <- map[SerializationKeys.id]
    code <- map[SerializationKeys.code]
    ishow <- map[SerializationKeys.ishow]
    pcateid <- map[SerializationKeys.pcateid]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    title <- map[SerializationKeys.title]
    pprice <- map[SerializationKeys.pprice]
    pmin <- map[SerializationKeys.pmin]
    path <- map[SerializationKeys.path]
        image <- map[SerializationKeys.image]
        sizes <- map[SerializationKeys.sizes]
        sizeWareHouse <- map[SerializationKeys.sizeWareHouse]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = applycard { dictionary[SerializationKeys.applycard] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = ishow { dictionary[SerializationKeys.ishow] = value }
    if let value = pcateid { dictionary[SerializationKeys.pcateid] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = pprice { dictionary[SerializationKeys.pprice] = value }
    if let value = pmin { dictionary[SerializationKeys.pmin] = value }
    if let value = path { dictionary[SerializationKeys.path] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = sizes { dictionary[SerializationKeys.sizes] = value.map { $0.dictionaryRepresentation() } }
    if let value = sizeWareHouse { dictionary[SerializationKeys.sizeWareHouse] = value }

    return dictionary
  }

}

//
//  SizeProductModel.swift
//
//  Created by mac on 8/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public struct SizeProductModel: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let title = "title"
    static let pid = "pid"
    static let code = "code"
    static let id = "id"

  }

  // MARK: Properties
  public var status: String?
  public var title: String?
  public var pid: String?
  public var code: String?
    public var id: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }
    init() {
    
    }
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public mutating func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    title <- map[SerializationKeys.title]
    pid <- map[SerializationKeys.pid]
    code <- map[SerializationKeys.code]
    id <- map[SerializationKeys.id]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = pid { dictionary[SerializationKeys.pid] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }

    return dictionary
  }

}

//
//  CustomerModel.swift
//
//  Created by mac on 8/27/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public struct CustomerModel: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let note = "note"
    static let city = "city"
    static let source = "source"
    static let email = "email"
    static let mobile = "mobile"
    static let cdate = "cdate"
    static let address = "address"
    static let gender = "gender"
    static let id = "id"
    static let code = "code"
    static let password = "password"
    static let fullname = "fullname"
    static let point = "point"
    static let country = "country"
    static let vipCode = "vipCode"

  }

  // MARK: Properties
  public var note: String?
  public var city: String?
  public var source: String?
  public var email: String?
  public var mobile: String?
  public var cdate: String?
  public var address: String?
  public var gender: String?
  public var id: String?
  public var code: String?
  public var password: String?
  public var fullname: String?
  public var point: String?
  public var country: String?
    public var vipCode: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }
    init() {
    
    }
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
    note <- map[SerializationKeys.note]
    city <- map[SerializationKeys.city]
    source <- map[SerializationKeys.source]
    email <- map[SerializationKeys.email]
    mobile <- map[SerializationKeys.mobile]
    cdate <- map[SerializationKeys.cdate]
    address <- map[SerializationKeys.address]
    gender <- map[SerializationKeys.gender]
    id <- map[SerializationKeys.id]
    code <- map[SerializationKeys.code]
    password <- map[SerializationKeys.password]
    fullname <- map[SerializationKeys.fullname]
    point <- map[SerializationKeys.point]
    country <- map[SerializationKeys.country]
    vipCode <- map[SerializationKeys.vipCode]

        
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = city { dictionary[SerializationKeys.city] = value }
    if let value = source { dictionary[SerializationKeys.source] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = mobile { dictionary[SerializationKeys.mobile] = value }
    if let value = cdate { dictionary[SerializationKeys.cdate] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = gender { dictionary[SerializationKeys.gender] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = password { dictionary[SerializationKeys.password] = value }
    if let value = fullname { dictionary[SerializationKeys.fullname] = value }
    if let value = point { dictionary[SerializationKeys.point] = value }
    if let value = country { dictionary[SerializationKeys.country] = value }
    if let value = vipCode { dictionary[SerializationKeys.vipCode] = value }

    return dictionary
  }

}
extension CustomerModel {
    func propertiesAreNil() throws -> Bool {
        
        let mirror = Mirror(reflecting: self)
        
        return !mirror.children.contains(where: { $0.value as Any? == nil})
    }
}

//
//  UserModel.swift
//
//  Created by mac on 9/25/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class UserModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let note = "note"
    static let city = "city"
    static let email = "email"
    static let mobile = "mobile"
    static let cdate = "cdate"
    static let address = "address"
    static let gender = "gender"
    static let status = "status"
    static let id = "id"
    static let code = "code"
    static let password = "password"
    static let udate = "udate"
    static let avatar = "avatar"
    static let fullname = "fullname"
    static let leave = "leave"
    static let country = "country"
  }

  // MARK: Properties
  public var note: String?
  public var city: String?
  public var email: String?
  public var mobile: String?
  public var cdate: String?
  public var address: String?
  public var gender: String?
  public var status: String?
  public var id: String?
  public var code: String?
  public var password: String?
  public var udate: String?
  public var avatar: String?
  public var fullname: String?
  public var leave: String?
  public var country: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    note <- map[SerializationKeys.note]
    city <- map[SerializationKeys.city]
    email <- map[SerializationKeys.email]
    mobile <- map[SerializationKeys.mobile]
    cdate <- map[SerializationKeys.cdate]
    address <- map[SerializationKeys.address]
    gender <- map[SerializationKeys.gender]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    code <- map[SerializationKeys.code]
    password <- map[SerializationKeys.password]
    udate <- map[SerializationKeys.udate]
    avatar <- map[SerializationKeys.avatar]
    fullname <- map[SerializationKeys.fullname]
    leave <- map[SerializationKeys.leave]
    country <- map[SerializationKeys.country]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = city { dictionary[SerializationKeys.city] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = mobile { dictionary[SerializationKeys.mobile] = value }
    if let value = cdate { dictionary[SerializationKeys.cdate] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = gender { dictionary[SerializationKeys.gender] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = password { dictionary[SerializationKeys.password] = value }
    if let value = udate { dictionary[SerializationKeys.udate] = value }
    if let value = avatar { dictionary[SerializationKeys.avatar] = value }
    if let value = fullname { dictionary[SerializationKeys.fullname] = value }
    if let value = leave { dictionary[SerializationKeys.leave] = value }
    if let value = country { dictionary[SerializationKeys.country] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.note = aDecoder.decodeObject(forKey: SerializationKeys.note) as? String
    self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.mobile = aDecoder.decodeObject(forKey: SerializationKeys.mobile) as? String
    self.cdate = aDecoder.decodeObject(forKey: SerializationKeys.cdate) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? String
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.code = aDecoder.decodeObject(forKey: SerializationKeys.code) as? String
    self.password = aDecoder.decodeObject(forKey: SerializationKeys.password) as? String
    self.udate = aDecoder.decodeObject(forKey: SerializationKeys.udate) as? String
    self.avatar = aDecoder.decodeObject(forKey: SerializationKeys.avatar) as? String
    self.fullname = aDecoder.decodeObject(forKey: SerializationKeys.fullname) as? String
    self.leave = aDecoder.decodeObject(forKey: SerializationKeys.leave) as? String
    self.country = aDecoder.decodeObject(forKey: SerializationKeys.country) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(note, forKey: SerializationKeys.note)
    aCoder.encode(city, forKey: SerializationKeys.city)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(mobile, forKey: SerializationKeys.mobile)
    aCoder.encode(cdate, forKey: SerializationKeys.cdate)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(gender, forKey: SerializationKeys.gender)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(code, forKey: SerializationKeys.code)
    aCoder.encode(password, forKey: SerializationKeys.password)
    aCoder.encode(udate, forKey: SerializationKeys.udate)
    aCoder.encode(avatar, forKey: SerializationKeys.avatar)
    aCoder.encode(fullname, forKey: SerializationKeys.fullname)
    aCoder.encode(leave, forKey: SerializationKeys.leave)
    aCoder.encode(country, forKey: SerializationKeys.country)
  }

}

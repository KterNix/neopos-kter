//
//  TaskModel.swift
//
//  Created by mac on 8/27/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public struct TaskModel: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let note = "note"
    static let usecard = "usecard"
    static let discount = "discount"
    static let descriptionValue = "description"
    static let details = "details"
    static let pstatus = "pstatus"
    static let cid = "cid"
    static let status = "status"
    static let bdate = "bdate"
    static let pmethod = "pmethod"
    static let id = "id"
    static let code = "code"
    static let total = "total"
    static let voucher = "voucher"
    static let otype = "otype"
    static let customer = "customer"
    static let member = "member"

  }

  // MARK: Properties
  public var note: String?
  public var usecard: String?
  public var discount: String?
  public var descriptionValue: String?
  public var details: [ServiceModel]?
  public var pstatus: String?
  public var cid: String?
  public var status: String?
  public var bdate: String?
  public var pmethod: String?
  public var id: String?
  public var code: String?
  public var total: String? 
  public var voucher: String?
  public var otype: String?
    public var customer: CustomerModel?
    public var member: MemberModel?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }
    init() {
        
    }
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
    note <- map[SerializationKeys.note]
    usecard <- map[SerializationKeys.usecard]
    discount <- map[SerializationKeys.discount]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    details <- map[SerializationKeys.details]
    pstatus <- map[SerializationKeys.pstatus]
    cid <- map[SerializationKeys.cid]
    status <- map[SerializationKeys.status]
    bdate <- map[SerializationKeys.bdate]
    pmethod <- map[SerializationKeys.pmethod]
    id <- map[SerializationKeys.id]
    code <- map[SerializationKeys.code]
    total <- map[SerializationKeys.total]
    voucher <- map[SerializationKeys.voucher]
    otype <- map[SerializationKeys.otype]
        customer <- map[SerializationKeys.customer]
        member <- map[SerializationKeys.member]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = usecard { dictionary[SerializationKeys.usecard] = value }
    if let value = discount { dictionary[SerializationKeys.discount] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = details { dictionary[SerializationKeys.details] = value }
    if let value = pstatus { dictionary[SerializationKeys.pstatus] = value }
    if let value = cid { dictionary[SerializationKeys.cid] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = bdate { dictionary[SerializationKeys.bdate] = value }
    if let value = pmethod { dictionary[SerializationKeys.pmethod] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = total { dictionary[SerializationKeys.total] = value }
    if let value = voucher { dictionary[SerializationKeys.voucher] = value }
    if let value = otype { dictionary[SerializationKeys.otype] = value }
    if let value = customer { dictionary[SerializationKeys.customer] = value }
    if let value = member { dictionary[SerializationKeys.member] = value }

    return dictionary
  }

}

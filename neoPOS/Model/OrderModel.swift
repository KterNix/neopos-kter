//
//  OrderModel1.swift
//
//  Created by mac on 8/24/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class OrderModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let pmethod = "pmethod"
    static let created = "cdate"
    static let code = "code"
    static let usecard = "usecard"
    static let discount = "discount"
    static let total = "total"
    static let details = "details"
    static let pstatus = "pstatus"
    static let cid = "cid"
    static let voucher = "voucher"
    static let otype = "otype"
    static let cname = "cname"
    static let customer = "customer"
    static let id = "id"

  }

  // MARK: Properties
  public var status: String?
  public var pmethod: String?
  public var created: String?
  public var code: String?
  public var usecard: String?
  public var discount: String?
  public var total: String?
  public var details: [ProductModel]?
  public var pstatus: String?
  public var cid: String?
  public var voucher: String?
  public var otype: String?
    public var cname: String?
    public var customer: CustomerModel?
    public var id: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }
    init() {
        
    }
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    pmethod <- map[SerializationKeys.pmethod]
    created <- map[SerializationKeys.created]
    code <- map[SerializationKeys.code]
    usecard <- map[SerializationKeys.usecard]
    discount <- map[SerializationKeys.discount]
    total <- map[SerializationKeys.total]
    details <- map[SerializationKeys.details]
    pstatus <- map[SerializationKeys.pstatus]
    cid <- map[SerializationKeys.cid]
    voucher <- map[SerializationKeys.voucher]
    otype <- map[SerializationKeys.otype]
    cname <- map[SerializationKeys.cname]
    customer <- map[SerializationKeys.customer]
    id <- map[SerializationKeys.id]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = pmethod { dictionary[SerializationKeys.pmethod] = value }
    if let value = created { dictionary[SerializationKeys.created] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = usecard { dictionary[SerializationKeys.usecard] = value }
    if let value = discount { dictionary[SerializationKeys.discount] = value }
    if let value = total { dictionary[SerializationKeys.total] = value }
    if let value = details { dictionary[SerializationKeys.details] = value }
    if let value = pstatus { dictionary[SerializationKeys.pstatus] = value }
    if let value = cid { dictionary[SerializationKeys.cid] = value }
    if let value = voucher { dictionary[SerializationKeys.voucher] = value }
    if let value = otype { dictionary[SerializationKeys.otype] = value }
    if let value = cname { dictionary[SerializationKeys.cname] = value }
    if let value = customer { dictionary[SerializationKeys.customer] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }

    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.pmethod = aDecoder.decodeObject(forKey: SerializationKeys.pmethod) as? String
    self.created = aDecoder.decodeObject(forKey: SerializationKeys.created) as? String
    self.code = aDecoder.decodeObject(forKey: SerializationKeys.code) as? String
    self.usecard = aDecoder.decodeObject(forKey: SerializationKeys.usecard) as? String
    self.discount = aDecoder.decodeObject(forKey: SerializationKeys.discount) as? String
    self.total = aDecoder.decodeObject(forKey: SerializationKeys.total) as? String
    self.details = aDecoder.decodeObject(forKey: SerializationKeys.details) as? [ProductModel]
    self.pstatus = aDecoder.decodeObject(forKey: SerializationKeys.pstatus) as? String
    self.cid = aDecoder.decodeObject(forKey: SerializationKeys.cid) as? String
    self.voucher = aDecoder.decodeObject(forKey: SerializationKeys.voucher) as? String
    self.otype = aDecoder.decodeObject(forKey: SerializationKeys.otype) as? String
    self.cname = aDecoder.decodeObject(forKey: SerializationKeys.cname) as? String
    self.customer = aDecoder.decodeObject(forKey: SerializationKeys.customer) as? CustomerModel
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String

  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(pmethod, forKey: SerializationKeys.pmethod)
    aCoder.encode(created, forKey: SerializationKeys.created)
    aCoder.encode(code, forKey: SerializationKeys.code)
    aCoder.encode(usecard, forKey: SerializationKeys.usecard)
    aCoder.encode(discount, forKey: SerializationKeys.discount)
    aCoder.encode(total, forKey: SerializationKeys.total)
    aCoder.encode(details, forKey: SerializationKeys.details)
    aCoder.encode(pstatus, forKey: SerializationKeys.pstatus)
    aCoder.encode(cid, forKey: SerializationKeys.cid)
    aCoder.encode(voucher, forKey: SerializationKeys.voucher)
    aCoder.encode(otype, forKey: SerializationKeys.otype)
    aCoder.encode(cname, forKey: SerializationKeys.cname)
    aCoder.encode(customer, forKey: SerializationKeys.customer)
    aCoder.encode(id, forKey: SerializationKeys.id)

  }

}

//
//  BaseUser.swift
//
//  Created by mac on 9/20/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class BaseUser: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let shop = "shop"
    static let error = "error"
    static let message = "message"
    static let token = "token"
    static let user = "user"
    static let expires = "expires"
  }

  // MARK: Properties
  public var shop: [ShopModel]?
  public var error: Bool? = false
  public var message: String?
  public var token: String?
  public var user: UserModel?
  public var expires: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    shop <- map[SerializationKeys.shop]
    error <- map[SerializationKeys.error]
    message <- map[SerializationKeys.message]
    token <- map[SerializationKeys.token]
    user <- map[SerializationKeys.user]
    expires <- map[SerializationKeys.expires]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = shop { dictionary[SerializationKeys.shop] = value.map { $0.dictionaryRepresentation() } }
    dictionary[SerializationKeys.error] = error
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = token { dictionary[SerializationKeys.token] = value }
    if let value = user { dictionary[SerializationKeys.user] = value.dictionaryRepresentation() }
    if let value = expires { dictionary[SerializationKeys.expires] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.shop = aDecoder.decodeObject(forKey: SerializationKeys.shop) as? [ShopModel]
    self.error = aDecoder.decodeBool(forKey: SerializationKeys.error)
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.token = aDecoder.decodeObject(forKey: SerializationKeys.token) as? String
    self.user = aDecoder.decodeObject(forKey: SerializationKeys.user) as? UserModel
    self.expires = aDecoder.decodeObject(forKey: SerializationKeys.expires) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(shop, forKey: SerializationKeys.shop)
    aCoder.encode(error, forKey: SerializationKeys.error)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(token, forKey: SerializationKeys.token)
    aCoder.encode(user, forKey: SerializationKeys.user)
    aCoder.encode(expires, forKey: SerializationKeys.expires)
  }

}

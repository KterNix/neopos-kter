//
//  SizeProdcutInWareHouse.swift
//
//  Created by mac on 8/17/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public struct SizeProdcutInWareHouse: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let pricekm = "pricekm"
    static let note = "note"
    static let sizeid = "sizeid"
    static let pid = "pid"
    static let quantity = "quantity"
    static let package = "package"
    static let price = "price"
    static let id = "id"
    static let expired = "expired"
    static let code = "code"
    static let udate = "udate"
    static let title = "title"
    static let wid = "wid"
    static let pwid = "pwid"
    static let pcode = "pcode"

  }

  // MARK: Properties
  public var pricekm: String?
  public var note: String?
  public var sizeid: String?
  public var pid: String?
  public var quantity: String?
  public var package: String?
  public var price: String?
  public var id: String?
  public var expired: String?
  public var code: String?
  public var udate: String?
  public var title: String?
  public var wid: String?
    public var pwid: String?
    public var pcode: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }
    init() {

    }
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
    pricekm <- map[SerializationKeys.pricekm]
    note <- map[SerializationKeys.note]
    sizeid <- map[SerializationKeys.sizeid]
    pid <- map[SerializationKeys.pid]
    quantity <- map[SerializationKeys.quantity]
    package <- map[SerializationKeys.package]
    price <- map[SerializationKeys.price]
    id <- map[SerializationKeys.id]
    expired <- map[SerializationKeys.expired]
    code <- map[SerializationKeys.code]
    udate <- map[SerializationKeys.udate]
    title <- map[SerializationKeys.title]
    wid <- map[SerializationKeys.wid]
        pwid <- map[SerializationKeys.pwid]
        pcode <- map[SerializationKeys.pcode]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = pricekm { dictionary[SerializationKeys.pricekm] = value }
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = sizeid { dictionary[SerializationKeys.sizeid] = value }
    if let value = pid { dictionary[SerializationKeys.pid] = value }
    if let value = quantity { dictionary[SerializationKeys.quantity] = value }
    if let value = package { dictionary[SerializationKeys.package] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = expired { dictionary[SerializationKeys.expired] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = udate { dictionary[SerializationKeys.udate] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = wid { dictionary[SerializationKeys.wid] = value }
    if let value = pwid { dictionary[SerializationKeys.pwid] = value }
    if let value = pcode { dictionary[SerializationKeys.pcode] = value }

    return dictionary
  }

}

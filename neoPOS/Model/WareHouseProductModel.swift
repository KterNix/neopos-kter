//
//  WareHouseProductModel.swift
//
//  Created by mac on 8/16/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public struct WareHouseProductModel: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let pwid = "pwid" // Ware product id
    static let note = "note"
    static let id = "id" // Product id
    static let expired = "expired"
    static let sizeId = "sizeId"
    static let quantity = "quantity"
    static let package = "package"
    static let price = "price"
    static let pricekm = "pricekm"
    static let code = "code"
    static let image = "imagePath"
    static let title = "title"
    static let pid = "pid"
    static let sizes = "sizes"


  }

  // MARK: Properties
  public var pwid: String?
  public var note: String?
  public var id: String?
  public var expired: String?
  public var sizeId: String?
  public var quantity: String?
  public var package: String?
  public var price: String?
  public var pricekm: String?
    public var image: String?
    public var title: String?
    public var code: String?
    public var pid: String?
    public var sizes: [SizeProdcutInWareHouse]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }
    init() {
    }
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
    pwid <- map[SerializationKeys.pwid]
    note <- map[SerializationKeys.note]
    id <- map[SerializationKeys.id]
    expired <- map[SerializationKeys.expired]
    sizeId <- map[SerializationKeys.sizeId]
    quantity <- map[SerializationKeys.quantity]
    package <- map[SerializationKeys.package]
    price <- map[SerializationKeys.price]
    pricekm <- map[SerializationKeys.pricekm]
        image <- map[SerializationKeys.image]
        title <- map[SerializationKeys.title]
        code <- map[SerializationKeys.code]
        pid <- map[SerializationKeys.pid]
        sizes <- map[SerializationKeys.sizes]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = pwid { dictionary[SerializationKeys.pwid] = value }
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = expired { dictionary[SerializationKeys.expired] = value }
    if let value = sizeId { dictionary[SerializationKeys.sizeId] = value }
    if let value = quantity { dictionary[SerializationKeys.quantity] = value }
    if let value = package { dictionary[SerializationKeys.package] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = pricekm { dictionary[SerializationKeys.pricekm] = value }
    if let value = image { dictionary[SerializationKeys.image] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = pid { dictionary[SerializationKeys.pid] = value }
    if let value = sizes { dictionary[SerializationKeys.sizes] = value.map{$0.dictionaryRepresentation()} }

    return dictionary
  }

}

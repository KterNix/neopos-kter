//
//  ServiceModel.swift
//
//  Created by mac on 8/27/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public struct ServiceModel: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let note = "note"
    static let scateid = "scateid"
    static let applycard = "applycard"
    static let id = "id"
    static let sid = "sid"
    static let sprice = "sprice"
    static let code = "code"
    static let status = "status"
    static let descriptionValue = "description"
    static let ishow = "ishow"
    static let title = "title"
    static let stime = "stime"
    static let price = "price"
    static let quantity = "quantity"
    static let scode = "scode"

  }

  // MARK: Properties
  public var note: String?
  public var scateid: String?
  public var applycard: String?
  public var id: String?
  public var sprice: String?
  public var code: String?
  public var status: String?
  public var descriptionValue: String?
  public var ishow: String?
  public var title: String?
  public var stime: String?
  public var price: String?
    public var quantity: String?
    public var sid: String?
    public var scode: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public mutating func mapping(map: Map) {
    note <- map[SerializationKeys.note]
    scateid <- map[SerializationKeys.scateid]
    applycard <- map[SerializationKeys.applycard]
    id <- map[SerializationKeys.id]
    sprice <- map[SerializationKeys.sprice]
    code <- map[SerializationKeys.code]
    status <- map[SerializationKeys.status]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    ishow <- map[SerializationKeys.ishow]
    title <- map[SerializationKeys.title]
    stime <- map[SerializationKeys.stime]
    price <- map[SerializationKeys.price]
    quantity <- map[SerializationKeys.quantity]
    sid <- map[SerializationKeys.sid]
    scode <- map[SerializationKeys.scode]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = note { dictionary[SerializationKeys.note] = value }
    if let value = scateid { dictionary[SerializationKeys.scateid] = value }
    if let value = applycard { dictionary[SerializationKeys.applycard] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = sprice { dictionary[SerializationKeys.sprice] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = ishow { dictionary[SerializationKeys.ishow] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = stime { dictionary[SerializationKeys.stime] = value }
    if let value = price { dictionary[SerializationKeys.price] = value }
    if let value = quantity { dictionary[SerializationKeys.quantity] = value }
    if let value = sid { dictionary[SerializationKeys.sid] = value }
    if let value = scode { dictionary[SerializationKeys.scode] = value }

    return dictionary
  }

}

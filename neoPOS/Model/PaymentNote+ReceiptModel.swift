//
//  PaymentNote+ReceiptModel.swift
//
//  Created by mac on 9/6/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper
public struct PaymentNote_ReceiptModel: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bankinfo = "bankinfo"
    static let mid = "mid"
    static let cdate = "cdate"
    static let descriptionValue = "description"
    static let amount = "amount"
    static let type = "type"
    static let refer = "refer"
    static let shopid = "shopid"
    static let status = "status"
    static let id = "id"
    static let refertype = "refertype"
    static let code = "code"
    static let udate = "udate"
    static let fullname = "fullname"
    static let aid = "aid"
    static let ctype = "ctype"
    static let ltype = "ltype"
  }

  // MARK: Properties
  public var bankinfo: String?
  public var mid: String?
  public var cdate: String?
  public var descriptionValue: String?
  public var amount: String?
  public var type: String?
  public var refer: String?
  public var shopid: String?
  public var status: String?
  public var id: String?
  public var refertype: String?
  public var code: String?
  public var udate: String?
  public var fullname: String?
  public var aid: String?
  public var ctype: String?
  public var ltype: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
    bankinfo <- map[SerializationKeys.bankinfo]
    mid <- map[SerializationKeys.mid]
    cdate <- map[SerializationKeys.cdate]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    amount <- map[SerializationKeys.amount]
    type <- map[SerializationKeys.type]
    refer <- map[SerializationKeys.refer]
    shopid <- map[SerializationKeys.shopid]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    refertype <- map[SerializationKeys.refertype]
    code <- map[SerializationKeys.code]
    udate <- map[SerializationKeys.udate]
    fullname <- map[SerializationKeys.fullname]
    aid <- map[SerializationKeys.aid]
    ctype <- map[SerializationKeys.ctype]
    ltype <- map[SerializationKeys.ltype]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = bankinfo { dictionary[SerializationKeys.bankinfo] = value }
    if let value = mid { dictionary[SerializationKeys.mid] = value }
    if let value = cdate { dictionary[SerializationKeys.cdate] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = amount { dictionary[SerializationKeys.amount] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = refer { dictionary[SerializationKeys.refer] = value }
    if let value = shopid { dictionary[SerializationKeys.shopid] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = refertype { dictionary[SerializationKeys.refertype] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = udate { dictionary[SerializationKeys.udate] = value }
    if let value = fullname { dictionary[SerializationKeys.fullname] = value }
    if let value = aid { dictionary[SerializationKeys.aid] = value }
    if let value = ctype { dictionary[SerializationKeys.ctype] = value }
    if let value = ltype { dictionary[SerializationKeys.ltype] = value }
    return dictionary
  }

}

//
//  ShopModel.swift
//
//  Created by mac on 9/20/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ShopModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let hasservice = "hasservice"
    static let name = "name"
    static let id = "id"
    static let code = "code"
    static let cdate = "cdate"
    static let address = "address"
    static let hotline = "hotline"
    static let owner = "owner"
    static let taxinfo = "taxinfo"
    static let vat = "vat"
  }

  // MARK: Properties
  public var status: String?
  public var hasservice: String?
  public var name: String?
  public var id: String?
  public var code: String?
  public var cdate: String?
  public var address: String?
  public var hotline: String?
  public var owner: String?
  public var taxinfo: String?
  public var vat: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    hasservice <- map[SerializationKeys.hasservice]
    name <- map[SerializationKeys.name]
    id <- map[SerializationKeys.id]
    code <- map[SerializationKeys.code]
    cdate <- map[SerializationKeys.cdate]
    address <- map[SerializationKeys.address]
    hotline <- map[SerializationKeys.hotline]
    owner <- map[SerializationKeys.owner]
    taxinfo <- map[SerializationKeys.taxinfo]
    vat <- map[SerializationKeys.vat]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = hasservice { dictionary[SerializationKeys.hasservice] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = cdate { dictionary[SerializationKeys.cdate] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = hotline { dictionary[SerializationKeys.hotline] = value }
    if let value = owner { dictionary[SerializationKeys.owner] = value }
    if let value = taxinfo { dictionary[SerializationKeys.taxinfo] = value }
    if let value = vat { dictionary[SerializationKeys.vat] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.hasservice = aDecoder.decodeObject(forKey: SerializationKeys.hasservice) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.code = aDecoder.decodeObject(forKey: SerializationKeys.code) as? String
    self.cdate = aDecoder.decodeObject(forKey: SerializationKeys.cdate) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.hotline = aDecoder.decodeObject(forKey: SerializationKeys.hotline) as? String
    self.owner = aDecoder.decodeObject(forKey: SerializationKeys.owner) as? String
    self.taxinfo = aDecoder.decodeObject(forKey: SerializationKeys.taxinfo) as? String
    self.vat = aDecoder.decodeObject(forKey: SerializationKeys.vat) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(hasservice, forKey: SerializationKeys.hasservice)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(code, forKey: SerializationKeys.code)
    aCoder.encode(cdate, forKey: SerializationKeys.cdate)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(hotline, forKey: SerializationKeys.hotline)
    aCoder.encode(owner, forKey: SerializationKeys.owner)
    aCoder.encode(taxinfo, forKey: SerializationKeys.taxinfo)
    aCoder.encode(vat, forKey: SerializationKeys.vat)
  }

}

//
//  CategoryModel.swift
//
//  Created by mac on 9/21/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class ProductCategoryModel: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let status = "status"
    static let productNumber = "productNumber"
    static let id = "id"
    static let code = "code"
    static let sort = "sort"
    static let title = "title"
    static let shopid = "shopid"
  }

  // MARK: Properties
  public var status: String?
  public var productNumber: String?
  public var id: String?
  public var code: String?
  public var sort: String?
  public var title: String?
  public var shopid: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    status <- map[SerializationKeys.status]
    productNumber <- map[SerializationKeys.productNumber]
    id <- map[SerializationKeys.id]
    code <- map[SerializationKeys.code]
    sort <- map[SerializationKeys.sort]
    title <- map[SerializationKeys.title]
    shopid <- map[SerializationKeys.shopid]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = productNumber { dictionary[SerializationKeys.productNumber] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = code { dictionary[SerializationKeys.code] = value }
    if let value = sort { dictionary[SerializationKeys.sort] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = shopid { dictionary[SerializationKeys.shopid] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.productNumber = aDecoder.decodeObject(forKey: SerializationKeys.productNumber) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? String
    self.code = aDecoder.decodeObject(forKey: SerializationKeys.code) as? String
    self.sort = aDecoder.decodeObject(forKey: SerializationKeys.sort) as? String
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.shopid = aDecoder.decodeObject(forKey: SerializationKeys.shopid) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(productNumber, forKey: SerializationKeys.productNumber)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(code, forKey: SerializationKeys.code)
    aCoder.encode(sort, forKey: SerializationKeys.sort)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(shopid, forKey: SerializationKeys.shopid)
  }

}

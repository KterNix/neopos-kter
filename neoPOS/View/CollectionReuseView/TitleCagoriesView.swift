//
//  TitleCagoriesView.swift
//  neoPOS
//
//  Created by mac on 8/22/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class TitleCagoriesView: UICollectionReusableView {

    //MARK:- Outlet
    @IBOutlet weak var title: TitleLabel!
    @IBOutlet weak var btnMore: StyleDefault!
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.btnMore.colorTitle = .secondColor
    }
    func setupDataSource(title:String ) {
        self.title.text = title
    }
    
    //MARK:- Outlet Actions
    
}

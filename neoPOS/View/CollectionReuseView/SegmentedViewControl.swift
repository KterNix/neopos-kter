//
//  SegmentedViewControl.swift
//  neoPOS
//
//  Created by mac on 9/7/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class SegmentedViewControl: UICollectionReusableView {

    //MARK:- Outlet
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.segmentControl.tintColor = UIColor.orange
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions
    
}

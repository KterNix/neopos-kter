//
//  HeaderTitileWithButton.swift
//  neoPOS
//
//  Created by mac on 8/24/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class HeaderTitileWithButton: UIView {

    //MARK:- Outlet
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var btnMore: StyleDefault!
    @IBOutlet weak var btnEdit: StyleDefault!
    @IBOutlet weak var viewLine: UIView!
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.title.textColor = UIColor.textTitleFormColor
        self.backgroundColor = .groupTableViewBackground
        self.btnMore.colorTitle = .secondColor
        self.btnEdit.colorTitle = .secondColor
    }
    func setupDataSource() {
        
    }
    func editButton(hide:Bool) {
        self.btnEdit.isHidden = hide
        self.viewLine.isHidden = hide
    }
    //MARK:- Outlet Actions

}

//
//  TotalPriceView.swift
//  neoPOS
//
//  Created by mac on 9/5/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
private let idCell = "TextCell"
protocol TotalPriceViewDelegate:class{
    func didSelectedDiscount(total:Double)
    func didDeselectedDiscount()
}
class TotalPriceView: UIView {

    //MARK:- Outlet
    @IBOutlet weak var price: RecommentDiscountLabel!
    @IBOutlet weak var priceDiscount: RecommentDiscountLabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewStrikThrough: UIView!
    @IBOutlet weak var widthContraintPrice: NSLayoutConstraint!
    @IBOutlet weak var discountTitle: RecommentLabel!
    @IBOutlet weak var btnAddDiscount: StyleDefault!
    
    
    
    //MARK:- Declare
    var total:Double?
    var indexSelected:Int?
    var discount:Double?
    weak var delegate:TotalPriceViewDelegate?
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
    }
    
    //MARK:- SubFunction
    func setupUI() {
        self.discountTitle.color = .lightGray
    }
    func getData(total:Double,discount:String?) {
        self.total = total
        self.discount = discount == nil ? nil : discount.asDouble
        self.discount = self.discount == 0 ? nil : self.discount
        price.text = total.withCommas()
        widthContraintPrice.constant = self.price.intrinsicContentSize.width
        calculationDiscount()
    }
    func checkDiscount() {
        priceDiscount.isHidden = self.discount != nil ? false : true
        viewStrikThrough.isHidden = self.discount != nil ? false : true
        price.color = self.discount != nil ? .lightGray : .red
    }
    func setupDataSource() {
        collectionView.register(UINib(nibName: idCell, bundle: nil), forCellWithReuseIdentifier: idCell)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    func calculationDiscount() {
        checkDiscount()
//        guard let indexSelected = self.indexSelected else {return}
//        let percent = 100 - Double((indexSelected + 1) * 5)
//        let discount = ((self.total ?? 0)  * percent)/100
        let discount = (total ?? 0) - (self.discount ?? 0)
        priceDiscount.text = discount.withCommas()
        if let discount = self.discount {
            if discount > 0 {
                self.btnAddDiscount.title = "-\(discount.withCommas())"
            }else {
                self.btnAddDiscount.title = "Thêm"
            }
        }else {
            self.btnAddDiscount.title = "Thêm"
        }
        delegate?.didSelectedDiscount(total: discount)
    }
    //MARK:- Outlet Actions

    
}
extension TotalPriceView : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: idCell, for: indexPath) as! TextCell
        cell.labelText.text = "-" + (5 * (indexPath.row + 1)).description + "%"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexSelected == nil || indexPath.row != indexSelected {
            indexSelected = indexPath.row
            calculationDiscount()
            
        }else {
            indexSelected = nil
            collectionView.deselectItem(at: indexPath, animated: true)
            checkDiscount()
            delegate?.didDeselectedDiscount()
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: collectionView.frame.height * 0.8)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


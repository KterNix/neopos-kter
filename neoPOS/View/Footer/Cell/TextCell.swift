//
//  TextCell.swift
//  neoPOS
//
//  Created by mac on 9/5/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class TextCell: MasterCollectionViewCell {

    //MARK:- Outlet
    @IBOutlet weak var labelText: RecommentLabel!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        setupDataSource()
    }
    override var isSelected: Bool{
        didSet{
            if isSelected {
                self.labelText.color = .mainColor
                self.borderColor = .mainColor

            }else {
                self.labelText.color = .lightGray
                self.borderColor = .white

            }
        }
    }
    //MARK:- SubFunction
    
    func setupUI() {
        self.backgroundColor = .white
        self.labelText.color = .lightGray
        self.borderWidth = 0.5
        self.borderColor = .white
        self.dropShadow()
    }
    func setupDataSource() {
        
    }
    
    //MARK:- Outlet Actions

}

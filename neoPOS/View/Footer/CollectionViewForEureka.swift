//
//  ColletionViewForEureka.swift
//  neoPOS
//
//  Created by mac on 8/24/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit

class CollectionViewForEureka: UIView {
    //MARK:- Outlet
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    //MARK:- Declare
    
    
    //MARK:- Override
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpUI()
        
    }
    
    //MARK:- SubFunction
    func setUpUI() {
        self.collectionView.backgroundColor = .groupTableViewBackground
    }
    func changeToScrollHorizontal() {
        guard  let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        self.collectionView.collectionViewLayout.invalidateLayout()
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
    }
    
    //MARK:- Outlet Actions

}

//
//  Enums.swift
//  neoReal
//
//  Created by pro on 4/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation




enum AccountRole: String {
    case ADMIN = "admin", OWNER = "owner", AGENCY = "agency", CUSTOMER = "customer", EDITTOR = "editor", MANAGER = "manager", VIEWER = "viewer", INVESTOR =  "investor"
}

enum ApartmentStatus {
    case SOLD, BOOK, DEPOSITED, ACTIVE, DEACTIVE, BLOCK
}

enum PlaceHolder:String {
    case AVATAR = "avatarPlaceHolder"
    case NORMAL_IMAGE = "placeholderImage"
}
enum PostType {
    case Booking
    case Deposit
    case NewDeposit

}
enum ProjetStatus: String {
    case Deactive = "0"
    case Active = "1"
    case Sellable = "2"
    case Lock = "-1"
    case Done = "3"
}
enum ErrorString: String {
    case CANT_REQUEST = "CANT_REQUEST"
    case NONE = "NONE"
    case MESSAGE = "MASSAGE"

}
enum EventStatus:String{
    case Cancel = "-1"
    case New = "0"
    case InPropress = "1"
    case Done = "2"
}
enum EndEventStatus:String {
    case Event = "0"
    case Phase1 = "1"
    case Phase2 = "2"
}
enum SocketKey: String {
    case CONNECT = "conneted"
    case CONNECT_USER = "connectUser"
}

enum SocketAction: String {
    case REQUEST = "REQUEST"
}

enum ProductsViewControllerType {
    case View, Order
}
enum WareHouseType {
    case Order
    case View
}
enum ChangePasswordStyle{
    case Forgot
    case Themself
}
enum AccountType {
    case Customer, Member
}
enum TypeDateToSelect {
    case Today, Yesterday, ThisWeek, ThisMonth, PreviousMonth, Custom
}
enum TypeDetail {
    case Create, Detail
}
enum TypeList {
    case View, Select, Multiselect
}
enum GetTaskType:String{
    case Date = "DATE"
    case Month = "MONTH"
}
enum OrderStatus:String {
    case New = "0"
    case Progressing = "1"
    case Completed = "2"
    case Pending = "-1"
    case All = "-2"
    case Draft = "-3"

}

//
//  DefaultSetting.swift
//  SB App
//
//  Created by mac on 7/8/18.
//

import Foundation
import UIKit
struct DefaultSetting {
    static let radius:CGFloat = 5
    static let mainColor = "#38bbc1"
    static let secondColor = "#098489"
    static let textColor = "#ffffff"
    static let textTitileFormColor = "#8b8b8f"
}

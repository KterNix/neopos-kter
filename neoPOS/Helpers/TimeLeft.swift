//
//  TimeLeft.swift
//  neoReal
//
//  Created by pro on 5/1/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

import UIKit

struct TimeLeft {
    
    var totalSeconds: TimeInterval
    
    var years: Int {
        return Int(totalSeconds) / 31536000
    }
    
    var days: Int {
        return (Int(totalSeconds) % 31536000) / 86400
    }
    
    var hours: Int {
        return (Int(totalSeconds) % 86400) / 3600
    }
    
    var minutes: Int {
        return (Int(totalSeconds) % 3600) / 60
    }
    
    var seconds: Int {
        return Int(totalSeconds) % 60
    }
    
    //simplified to what OP wanted
    var hoursMinutesAndSeconds: (hours: Int, minutes: Int, seconds: Int) {
        return (hours, minutes, seconds)
    }
}

//
//  UIViewController.swift
//  neoLock
//
//  Created by pro on 9/30/17.
//  Copyright © 2017 bantayso.neoLock. All rights reserved.
//

import UIKit

enum gestureVisible {
    case visible,invisible
}


extension UIViewController {
    func createBarItem(forLeft:Bool = false,title :String?,imageName:String?, selector: Selector,fontSize:CGFloat = 30) {
        var bar = UIBarButtonItem()
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        button.addTarget(self, action: selector, for: .touchUpInside)

        if let textTitle = title {
            button.setAttributedTitle(NSAttributedString(string: textTitle, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: fontSize), NSAttributedString.Key.foregroundColor: UIColor.white]), for: .normal)
        }else {
            guard let imageName = imageName else {return}
            guard let image = UIImage(named: imageName)?.resize(width: 20, height: 20)?.withRenderingMode(.alwaysTemplate) else {return}
            button.setImage(image, for: .normal)
        }
        bar = UIBarButtonItem(customView: button)

        if forLeft {
             self.navigationItem.leftBarButtonItems == nil ? self.navigationItem.leftBarButtonItems = [bar] : self.navigationItem.leftBarButtonItems?.append(bar)
        }else {
             self.navigationItem.rightBarButtonItems == nil ? self.navigationItem.rightBarButtonItems = [bar] : self.navigationItem.rightBarButtonItems?.append(bar)
        }
    }
    func internetAvailable() -> Bool {
        let networkStatus = NetworkStatus.sharedInstance
        return networkStatus.reachabilityManager?.networkReachabilityStatus == .notReachable ? false : true
    }
    func createNaviControllerForTabbar(icon:String,title:String) -> UINavigationController {
        let navi = UINavigationController(rootViewController: self)
        navi.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: icon), selectedImage: nil)
        navi.title = title
        return navi
    }
    func listenKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(textField:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    
    @objc func keyboardWillDisappear(textField:UITextField) {
        hideKeyboardWhenTappedAround(gesture: .invisible)
    }
    
    @objc func keyboardWillAppear() {
        hideKeyboardWhenTappedAround(gesture: .visible)
    }
    
    
    
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func hideKeyboardWhenTappedAround(gesture: gestureVisible,completionHandler: (() -> Void)? = nil) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        switch gesture {
        case .visible:
            view.addGestureRecognizer(tap)
        case .invisible:
            view.gestureRecognizers?.removeAll()
            completionHandler?()
        }
    }
    
    
    func hideBackButton() {
        self.navigationItem.hidesBackButton = true
    }
    
    func applyAttributedNAV() {
        self.navigationController?.navigationBar.tintColor = UIColor.mainColor
        let nsAttributed = [NSAttributedString.Key.foregroundColor : UIColor.mainColor,NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 20)]
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.titleTextAttributes = nsAttributed
            self.navigationController?.navigationBar.largeTitleTextAttributes = nsAttributed
        } else {
            self.navigationController?.navigationBar.titleTextAttributes = nsAttributed
        }
    }
    
    func setBackButton() {
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = backBTN()
    }
    
    
    
    @objc func popViewController() {
        
        if isModal {
            dismiss(animated: true, completion: nil)
        }else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    var isModal: Bool {
        if let index = navigationController?.viewControllers.index(of: self), index > 0 {
            return false
        } else if presentingViewController != nil {
            return true
        } else if navigationController?.presentingViewController?.presentedViewController == navigationController  {
            return true
        } else if tabBarController?.presentingViewController is UITabBarController {
            return true
        } else {
            return false
        }
    }
    
    
    
    private func backBTN() -> UIBarButtonItem {
        let b = UIButton(type: .custom)
        b.setAttributedTitle(NSAttributedString(string: "✕", attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 20),NSAttributedString.Key.foregroundColor:UIColor.white]), for: .normal)
        b.tintColor = .mainColor
//        b.setImage(UIImage(named: "arrowLeft")?.resize(width: 20, height: 20)?.withRenderingMode(.alwaysTemplate), for: .normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(popViewController), for: UIControl.Event.touchUpInside)
        let backBTN = UIBarButtonItem(customView: b)
        return backBTN
    }
    
    
    
    func updateNAVTint(color: UIColor) {
        navigationController?.navigationBar.tintColor = color
    }
    
    
//    func applyBackgroundGradient() {
//        let colorTop = UIColor.init(red: 156, green: 219, blue: 204)
//        let colorBottom = UIColor.init(red: 86, green: 197, blue: 238)
//        view.applyGradient(withColours: [colorTop,colorBottom], gradientOrientation: .vertical)
//    }
    
    
    func postNotificationWithKey(_ name: String,userInfo: [AnyHashable : Any]?) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: name), object: nil, userInfo: userInfo)
    }
    
    
    func orderDayFromDate(inputDate:Date) -> NSInteger {
    
        let calendar:NSCalendar = NSCalendar(calendarIdentifier: .gregorian)!
        let timeZone:TimeZone = TimeZone(identifier: TimeZone.current.identifier)!
        
        calendar.timeZone = timeZone
        
        let calendarUnit = NSCalendar.Unit.weekday
        
        let theComponents:DateComponents = calendar.components(calendarUnit, from: inputDate)
        
        return theComponents.weekday!
    }
    
    
    func encodeDataWith<T>(object: T,filePath: String) {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        let filePaths = url!.appendingPathComponent(filePath).path
        NSKeyedArchiver.archiveRootObject(object, toFile: filePaths)
    }
    
    func decodeDataFrom<T>(filePath: String,completionHandler: @escaping (T)->()) {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        let filePaths = url!.appendingPathComponent(filePath).path
        if let ourData = NSKeyedUnarchiver.unarchiveObject(withFile: filePaths) as? T {
            completionHandler(ourData)
        }
    }
    
    func convertToCelsius(t:Float, source:String) -> Float? {
        switch source {
        case "Kelvin": return t - 273.15
        case "Celsius": return t
        case "Fahrenheit": return (t - 32) * 5 / 9
        case "Rankine": return (t - 491.67) * 5 / 9
        case "Delisle": return 100 - t * 2 / 3
        case "Newton": return t * 100 / 33
        case "Reaumur": return t * 5 / 4
        case "Romer": return (t - 7.5) * 40 / 21
        default: return nil
        }
    }
    
    func convertFromCelsius(t:Float, target:String) -> Float? {
        switch target {
        case "Kelvin":return t + 273.15
        case "Celsius": return t
        case "Fahrenheit": return t * 9 / 5 + 32
        case "Rankine": return (t + 273.15) * 9 / 5
        case "Delisle": return (100 - t) * 3 / 2
        case "Newton": return t * 33 / 100
        case "Reaumur": return t * 4 / 5
        case "Romer": return t * 21 / 40 + 7.5
        default: return nil
        }
    }
    
    func convertTemperatures(t:Float, source:String, target:String) -> Float {
        return convertFromCelsius(t: convertToCelsius(t: t, source: source)!, target: target)!
    }
    
    
    func pushDetail(_ viewControllerToPush: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(viewControllerToPush, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    
    
    func clearNavigationColor() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    func whiteNavigationColor() {
        navigationController?.navigationBar.backgroundColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func lightStatusBar() {
        DispatchQueue.main.async {
            UIApplication.shared.statusBarStyle = .lightContent
        }
    }
    
    func defaultStatusBar() {
        DispatchQueue.main.async {
            UIApplication.shared.statusBarStyle = .default
        }
    }
    func createEmptyView(onItem:UIView,didTryAgain:@escaping() -> Void) {
        if self.view.viewWithTag(100) == nil {
            let viewEmpty = ViewEmpty(frame:CGRect(x: 0, y: 0, width: onItem.width, height: onItem.height))
            viewEmpty.didTryAgian = {
                didTryAgain()
            }
            viewEmpty.tag = 100
            onItem.addSubview(viewEmpty)
//            viewEmpty.translatesAutoresizingMaskIntoConstraints = false
//            onItem.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0(\(onItem.width))]|", options: [.alignAllCenterY], metrics: nil, views: ["v0" : viewEmpty]))
//            onItem.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\((onItem.height - 200)/2)-[v0]-\((onItem.height - 200)/2)-|", options: [.alignAllCenterX], metrics: nil, views: ["v0" : viewEmpty]))
//            onItem.layoutIfNeeded()
        }
    }
    func removeEmptyView(onItem:UIView) {
        onItem.viewWithTag(100)?.removeFromSuperview()
    }
    func drawLineSeparatorFor(cell: UITableViewCell,height: CGFloat) {
        let additionalSeparatorThickness = CGFloat(1)
        let additionalSeparator = UIView(frame: CGRect(x: 8, y: height - additionalSeparatorThickness, width: cell.frame.size.width - 16, height: additionalSeparatorThickness))
        additionalSeparator.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        cell.addSubview(additionalSeparator)
    }
    
    func animateCell(cell: UITableViewCell) {
        cell.transform = CGAffineTransform(translationX: 0, y: 50)
        cell.alpha = 0
        UIView.beginAnimations("rotation", context: nil)
        UIView.setAnimationDuration(0.5)
        cell.transform = CGAffineTransform(translationX: 0, y: 0)
        cell.alpha = 1
        UIView.commitAnimations()
    }
    
    
    func call(number: String) {
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    
    func customTabbarItem(title: String, image: String) -> UITabBarItem {
        return UITabBarItem(title: title, image: UIImage(named: image), selectedImage: nil)
    }
    
}

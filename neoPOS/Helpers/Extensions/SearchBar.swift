//
//  SearchBar.swift
//  neoLock
//
//  Created by pro on 9/30/17.
//  Copyright © 2017 bantayso.neoLock. All rights reserved.
//

import UIKit

extension UISearchBar {
    
    func changeSearchBarColor(color : UIColor) {
        for subView in self.subviews {
            for subSubView in subView.subviews {
                
                if let _ = subSubView as? UITextInputTraits {
                    let textField = subSubView as! UITextField
                    textField.backgroundColor = color
                    textField.textColor = .white
                    textField.tintColor = .white
                    textField.leftView = nil
                    textField.leftViewMode = .never
                    textField.layer.cornerRadius = textField.height/2
                    textField.layer.masksToBounds = true
                    textField.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "Tìm", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
                }
                if let button = subSubView as? UIButton{
                    button.tintColor = .white
                    button.setTitle("Huỷ", for: .normal)
                    button.setTitle("Huỷ", for: .disabled)

                }
                if let imageView = subSubView as? UIImageView {
                    imageView.removeFromSuperview()
                }
            }
        }
    }
    
    func maskSearchBar() {
        self.changeSearchBarColor(color: UIColor.white.withAlphaComponent(0))
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.withAlphaComponent(1).cgColor
        self.barTintColor = UIColor.white
    }
}

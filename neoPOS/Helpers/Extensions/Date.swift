//
//  Date.swift
//  soAir
//
//  Created by pro on 12/4/17.
//  Copyright © 2017 pro. All rights reserved.
//

import Foundation

extension Date {
    
    
    func addedBy(type: Calendar.Component,value: Int) -> Date {
        return Calendar.current.date(byAdding: type, value: value, to: self)!
    }
    
    @nonobjc static var localFormatter: DateFormatter = {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateStringFormatter
    }()
    func getCurrentDate() -> Date {
        return self.localDateString().toDate(haveTime: true)!
    }
    func localDateString() -> String
    {
        return Date.localFormatter.string(from: self)
    }
    func compareWithCurrentDay() -> ComparisonResult{
        return self.compare(Date().getCurrentDate())
    }
    func compareInTime(startDate:Date?,endDate:Date?) -> Bool {
        guard let startDate = startDate else {return false}
        guard let endDate = endDate else {return false}
        return startDate.compareWithCurrentDay() == .orderedAscending && endDate.compareWithCurrentDay() == .orderedDescending
    }
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: self)
    }
    func toString( dateFormat format: String,dateString: String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        if let date = dateFormatter.date(from: dateString) {
            return dateFormatter.string(from: date)
        }
        return "Sai định dạng ngày"

    }
    func toDate(string:String) -> Date? {
        let dateFormatter = Date.localFormatter
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from: string)
        return date
    }
}

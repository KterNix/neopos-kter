//
//  Font.swift
//  POS
//
//  Created by mac on 4/3/18.
//  Copyright © 2018 Ban Tay So. All rights reserved.
//

import UIKit
extension UIFont{
    private static var sizeButton:CGFloat = 0
    private static var sizeText:CGFloat = 0
    private static var sizeRecomment:CGFloat = 0
    private static var sizeRecommentDiscount:CGFloat = 0
    private static var sizeTitle:CGFloat = 0

    static func setSizeFont() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            UIFont.sizeButton = 18
            UIFont.sizeText = 15
            UIFont.sizeRecomment = 18
            UIFont.sizeRecommentDiscount = 25
            UIFont.sizeTitle = 35
        }else {
            UIFont.sizeButton = 13
            UIFont.sizeText = 12
            UIFont.sizeRecomment = 13
            UIFont.sizeRecommentDiscount = 15
            UIFont.sizeTitle = 20
        }
    }
    static let titleFont = UIFont.systemFont(ofSize: UIFont.sizeTitle, weight: .thin)
    static let backButtonFont =  UIFont.systemFont(ofSize: 30, weight: .bold)
    static let buttonFont =  UIFont.systemFont(ofSize: UIFont.sizeButton, weight: .bold)
    static let textFont =  UIFont.systemFont(ofSize: UIFont.sizeText, weight: .thin)
    static let recommentFont =  UIFont.systemFont(ofSize: UIFont.sizeRecomment, weight: .bold)
    static let recommentDiscountFont =  UIFont.systemFont(ofSize: UIFont.sizeRecommentDiscount, weight: .thin)

}

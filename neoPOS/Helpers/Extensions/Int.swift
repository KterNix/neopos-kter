//
//  Int.swift
//  neoReal
//
//  Created by pro on 5/1/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

extension Int {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))!
    }
}
extension Double {
    func withCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        return numberFormatter.string(from: NSNumber(value:self))! + " VNĐ"
    }
    func priceWithVAT() -> Double {
        return self + (self*10)/100
    }
    var clean: String {
        return String(format: "%.0f", self)
    }
}


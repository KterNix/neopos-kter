//
//  UIImageView.swift
//  neoReal
//
//  Created by pro on 4/23/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
extension UIImageView {
    
    func setImageWith(path: String?,imageType: PlaceHolder) {
        
        DispatchQueue.global().async {
            if let image = path {
                var fullPath:String = ""
                if image.contains("googleusercontent.com") || image.contains("graph.facebook.com") || image.contains("firebasestorage") {
                    fullPath = image
                }else{
                    fullPath = image
                }
                let url = URL(string: fullPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
                DispatchQueue.main.async {
                    self.sd_setImage(with: url, placeholderImage: UIImage(named: imageType.rawValue), options: [], completed: nil)
                }
            }
        }
    }
    func setImageWith(text:String) {
        guard text != "Trống" else {
            self.image = UIImage(named: PlaceHolder.NORMAL_IMAGE.rawValue)
            return }

        guard let data = Data(base64Encoded: text, options: .ignoreUnknownCharacters) else {
            self.image = UIImage(named: PlaceHolder.NORMAL_IMAGE.rawValue)
            return
        }
        self.image = UIImage(data: data)
    }
    func setImageWith(tintColor:UIColor) {
        self.tintColor = tintColor
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
    }
    func getCurrentImageSize() ->CGSize {
        var resultFrame = CGSize()
        let imageSize = self.image?.size
        let frameSize = self.frame.size
        let imageSmallerThanFrame = ((imageSize?.width)! < frameSize.width) && ((imageSize?.height)! < frameSize.height)
        if imageSmallerThanFrame
        {
            resultFrame = imageSize!;
        }
        else
        {
            let widthRatio  = roundf(Float(imageSize!.width  / frameSize.width))
            let heightRatio = roundf(Float(imageSize!.height / frameSize.height))
            let maxRatio = max(widthRatio, heightRatio)
            
            resultFrame = CGSize(width: CGFloat(roundf(Float(imageSize!.width) / maxRatio)), height: CGFloat(roundf(Float(imageSize!.height) / maxRatio)))
        }
        return resultFrame
    }
}

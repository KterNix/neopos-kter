//
//  ManagerNetworking.swift
//  Pikapeer
//
//  Created by pro on 2/1/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import Alamofire


class ManagerNetworking: NSObject {

    static let shared = ManagerNetworking()

    var manager = Alamofire.SessionManager.default
    var timeout:TimeInterval = 20  // seconds

    override init() {
        super.init()
        manager = self.getAlamofireManager()
    }

    private func getAlamofireManager() -> Alamofire.SessionManager  {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = timeout
        configuration.timeoutIntervalForRequest = timeout

        let alamofireManager = Alamofire.SessionManager(configuration: configuration)
        return alamofireManager
    }
}


//
//  LocalNotification.swift
//  neoReal
//
//  Created by pro on 5/10/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications


class LocalNotification: NSObject, UNUserNotificationCenterDelegate {
    
    class func pushNoticeWith(identifier: String, title: String, body: String, badge: NSNumber = 1, userInfo: [AnyHashable: Any]? = nil, date: Date) {
        //        if #available(iOS 10, *) {
        //
        //            let dateTime = date.timeIntervalSinceNow < 1 ? 1 : Double(date.timeIntervalSinceNow.clean)
        //            let center = UNUserNotificationCenter.current()
        //            let content = UNMutableNotificationContent()
        //            content.title = title
        //            content.body = body
        //            content.badge = UIApplication.shared.applicationIconBadgeNumber as NSNumber
        //            content.categoryIdentifier = identifier
        //            content.sound = UNNotificationSound.default()
        //            content.userInfo = userInfo!
        //
        //            let comp = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute], from: date)
        //
        //            let triggers = UNTimeIntervalNotificationTrigger(timeInterval: 120, repeats: false)
        ////            let tri = uitrig
        //            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: triggers)
        //            center.add(request, withCompletionHandler: { (err) in
        //
        //            })
        //        } else {
        //
        let notification = UILocalNotification()
        notification.fireDate = date
        notification.alertTitle = title
        notification.alertBody = body
        notification.timeZone = TimeZone.current
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.userInfo = userInfo!
        UIApplication.shared.scheduleLocalNotification(notification)
        //        }
        
//        Utils.userDefault.set(userInfo, forKey: userInfo?.first?.value as! String)
//        Utils.userDefault.synchronize()
    }
    
    
    class func removePendingNoticeWith(identi: String) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getPendingNotificationRequests { (notificationRequests) in
                var identifiers: [String] = []
                for notification:UNNotificationRequest in notificationRequests {
                    if notification.identifier == identi {
                        identifiers.append(notification.identifier)
                    }
                }
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: identifiers)
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    
    
    //    class func registerForLocalNotification(on application:UIApplication) {
    //        if (UIApplication.instancesRespond(to: #selector(UIApplication.registerUserNotificationSettings(_:)))) {
    //            let notificationCategory:UIMutableUserNotificationCategory = UIMutableUserNotificationCategory()
    //            notificationCategory.identifier = "NOTIFICATION_CATEGORY"
    //
    //            //registerting for the notification.
    //            application.registerUserNotificationSettings(UIUserNotificationSettings(types:[.sound, .alert, .badge], categories: nil))
    //        }
    //    }
    //
    //    class func dispatchlocalNotification(with title: String, body: String, userInfo: [AnyHashable: Any]? = nil, at date:Date) {
    //
    //        if #available(iOS 10.0, *) {
    //
    //            let center = UNUserNotificationCenter.current()
    //            let content = UNMutableNotificationContent()
    //            content.title = title
    //            content.body = body
    //            content.categoryIdentifier = "NOTIFICATION_CATEGORY"
    //
    //            if let info = userInfo {
    //                content.userInfo = info
    //            }
    //
    //            content.sound = UNNotificationSound.default()
    //
    //            let comp = Calendar.current.dateComponents([.hour, .minute], from: date)
    //
    //            let trigger = UNCalendarNotificationTrigger(dateMatching: comp, repeats: true)
    //
    //            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
    //
    //            center.add(request)
    //
    //        } else {
    //
    //            let notification = UILocalNotification()
    //            notification.fireDate = date
    //            notification.alertTitle = title
    //            notification.alertBody = body
    //
    //            if let info = userInfo {
    //                notification.userInfo = info
    //            }
    //
    //            notification.soundName = UILocalNotificationDefaultSoundName
    //            UIApplication.shared.scheduleLocalNotification(notification)
    //
    //        }
    //
    //        print("WILL DISPATCH LOCAL NOTIFICATION AT ", date)
    //
    //    }
}

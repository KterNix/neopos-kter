//
//  BaseURL.swift
//  neoReal
//
//  Created by pro on 4/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation


class BaseURL {
    static let URL_IMAGE = ""
    static let url = "http://206.189.145.160/"
    static let trueUrl = "http://api.neohome.vn/"

    static let URL_GET_PRODUCTS = trueUrl + "products"
    static let URL_GET_PRODUCTS_IN_WARE_HOUSE = trueUrl + "productsFromWarehouse"
    static let URL_PUT_PRODUCT = trueUrl + "product/"
    static let URL_POST_PRODUCT = trueUrl + "product"
    static let URL_GET_PRODUCT = trueUrl + "product/"
    static let URL_POST_AND_PUT_PRODUCT_IN_WARE_HOUSE = trueUrl + "productsFromWarehouse"
    static let URL_POST_SIZE = trueUrl + "size"
    static let URL_GET_PRODUCT_IN_WARE_HOUSE = trueUrl + "productsFromWarehouse/"
    static let URL_LOGIN = trueUrl + "token"
    static let URL_GET_SERVICES = trueUrl + "services"
    static let URL_GET_CUSTOMERS = trueUrl + "customers"
    static let URL_GET_MEMBERS = trueUrl + "members"
    static let URL_GET_ORDERS = trueUrl + "orders"
    static let URL_GET_TASKS = trueUrl + "tasks"
    static let URL_GET_CHART_DATA = trueUrl + "ledgersChart"
    static let URL_GET_PAYMENTNOT_RECEIPT = trueUrl + "ledgers"
    static let URL_POST_PAYMENTNOT_RECEIPT = trueUrl + "ledger"
    static let URL_POST_TASK = trueUrl + "task"
    static let URL_PUT_TASK = trueUrl + "task/"
    static let URL_POST_ORDER = trueUrl + "order"
    static let URL_GET_ORDER = trueUrl + "order/"
    static let URL_GET_PRODUCT_CATEGORIES = trueUrl + "productCate"
    static let URL_PUT_USER = trueUrl + "member/"
    static let URL_PUT_SIZE = trueUrl + "size/"
    static let URL_POST_CUSTOMER = trueUrl + "customer"
    static let URL_PUT_CUSTOMER = trueUrl + "customer/"
    static let URL_GET_SERVICES_CATE = trueUrl + "serviceCate"

    static let URL_SOCKET = "http://clickfood.vn:8080"


}

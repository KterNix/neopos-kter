//
//  Constants.swift
//  neoReal
//
//  Created by pro on 4/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    
    static let BUTTON_RADIUS:CGFloat = 5
    
    
    
    
    
    
    //MARK: - GOOGLE API KEY
    struct GOOGLE_API {
        static let KEY = ""
    }
    
    
    
    
    //MARK: - USER_DEFAULT KEY
    struct USER_DEFAULT {
        static let USER_TOKEN = "USER_TOKEN"
        static let USER_MODEL = "USER_MODEL"
        static let TOKEN_EXPIRED = "TOKEN_EXPIRED"
        static let SHOPS = "SHOPS"
        static let DRAFT_ORDERS = "DRAFT_ORDERS"

    }
    
    
    //MARK: - ERROR STRING
    struct ERROR {
        static let NO_DATA = "Không có dữ liệu"
        static let CANT_REQUEST = "Đã xảy ra sự cố. Vui lòng thử lại !"
        static let INTERNET_TITLE = "Mạng không khả dụng."
        static let INTERNET_SUB_TITLE = "Vui lòng kiểm tra lại kết nối internet !"
    }
    
    
    
    //MARK: - COLLECTIONVIEW IN TABLEVIEW CELL
    
    struct CUSTOMCOLL {
        static let totalItem: CGFloat = 8
        
        static let column: CGFloat = 2
        
        static let minLineSpacing: CGFloat = 1.0
        static let minItemSpacing: CGFloat = 1.0
        
        static let offset: CGFloat = 1.0 // TODO: for each side, define its offset
        
        static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
            
            // totalCellWidth = (collectionview width or tableview width) - (left offset + right offset) - (total space x space width)
            let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
            
            return totalWidth / column
        }
    }
    
    
    struct NOTIFICATION_KEY {
        static let DROPDOWN_ACTION = "DROPDOWN_ACTION"
        static let FIRED = "FIRED"
    }
    struct HeightTool {
        static let navigationBar:CGFloat = 64
        static let tabbar:CGFloat = UIDevice.modelName == "iPhone X" ? 83 : 49
        static let total = HeightTool.navigationBar + HeightTool.tabbar
        
    }
    
    
}

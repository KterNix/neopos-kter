//
//  Utils.swift
//  RestaurantBTS
//
//  Created by pro on 7/10/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import ObjectMapper
import SideMenu
open class Utils {
    
    var projectName:String?
    
    static let shared = Utils()
    static let userDefault = UserDefaults.standard
    static var productsHome = [ProductModel]()
    static var productsDidUpdated : ProductModel?
    static var productCategories = [ProductCategoryModel]()
    static var serviceCategories = [ServiceCateModel]()

    static var orders = [OrderModel]()
    static var indexOrderSelected :Int?
    static let avatarDefault = "https://firebasestorage.googleapis.com/v0/b/neolock-a17c1.appspot.com/o/avatars%2FavatarDefault.png?alt=media&token=42e34b09-84ae-43f6-9c00-bd8581f00c1b"
    
    static var errorString = ""
    
    public static func getUserDefaults(withKey:String) -> Bool {
        return (userDefault.string(forKey: withKey) != nil) ? true:false
    }
    static func isIPad() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    public static func checkLogedIn() -> Bool {
        if getUserDefaults(withKey: Constants.USER_DEFAULT.USER_TOKEN) {
            return true
        } else {
            return false
        }
    }
    func getDraftOrders() -> [OrderModel]? {
        guard let value = Utils.userDefault.value(forKey: Constants.USER_DEFAULT.DRAFT_ORDERS) as? String else {return nil}
        guard let orders =  Mapper<OrderModel>().mapArray(JSONString: value) else {return nil}
        return orders
    }
    func saveDraftOrders(value:[OrderModel]?) {
        Utils.userDefault.set(value?.toJSONString(), forKey: Constants.USER_DEFAULT.DRAFT_ORDERS)
        Utils.userDefault.synchronize()
    }
    func suffixNumber(number:NSNumber) -> String {
        
        var num:Double = number.doubleValue
        let sign = ((num < 0) ? "-" : "" )
        
        num = fabs(num)
        
        if (num < 1000.0){
            return "\(sign)\(num)"
        }
        
        let exp:Int = Int(log10(num) / 3.0 ) //log10(1000));
        
        let units:[String] = ["K","M","G","T","P","E"]
        
        let roundedNum:Double = round(10 * num / pow(1000.0,Double(exp))) / 10
        
        return "\(sign)\(roundedNum)\(units[exp-1])"
    }
    
    public static func setDefaultNavicontroller() {
        let instance = UINavigationBar.appearance()
        instance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        instance.tintColor = .white
        instance.barTintColor = UIColor.mainColor
        instance.isTranslucent = false
        instance.backgroundColor = .mainColor
        instance.shadowImage = UIImage()
    }
    public static func getCurrentUser() -> UserModel? {
        guard let user = Utils.userDefault.value(forKey: Constants.USER_DEFAULT.USER_MODEL) else { return nil }
        guard let userModel = Mapper<UserModel>().map(JSONObject: user) else { return nil }
        return userModel
    }
    public static func getCurrentToken() -> String? {
        guard let token = Utils.userDefault.string(forKey: Constants.USER_DEFAULT.USER_TOKEN) else {return nil}
        return "Bearer " + token
    }
    public static func getCurrentTokenWithoutFirstKey() -> String {
        let token = Utils.userDefault.string(forKey: Constants.USER_DEFAULT.USER_TOKEN)
        return token ?? ""
    }
    public static func saveModelForUser(_ user: BaseUser) {
        Utils.userDefault.set(user.user?.dictionaryRepresentation(), forKey: Constants.USER_DEFAULT.USER_MODEL)
        Utils.userDefault.set(user.token, forKey: Constants.USER_DEFAULT.USER_TOKEN)
        Utils.userDefault.set(user.shop?.toJSON(), forKey: Constants.USER_DEFAULT.SHOPS)
        Utils.userDefault.synchronize()
    }
   
    public static func removeUser() {
        Utils.userDefault.removeObject(forKey: Constants.USER_DEFAULT.USER_MODEL)
        Utils.userDefault.removeObject(forKey: Constants.USER_DEFAULT.USER_TOKEN)
        Utils.userDefault.removeObject(forKey: Constants.USER_DEFAULT.SHOPS)
        Utils.userDefault.synchronize()
    }
    public static func getCurrentDateTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)
        let date = Date()
        return formatter.string(from: date)
    }
    public static func alertWithTextfield (titile : String, message : String,placeholder1:String?,placeholder2:String?,viewController : UIViewController, actionDone:@escaping (([String?]?) -> Void)){
        let alert = UIAlertController(title: titile, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive) { (action) in
            let texts =  alert.textFields?.map{$0.text}
            actionDone(texts)
        }
        let cancel = UIAlertAction(title: "Thoát", style: .cancel) { (alert) in
        }
        if let placeholder = placeholder1 {
            alert.addTextField { (textField) in
                textField.placeholder = placeholder
                textField.tag = 1
            }
        }
        if let placeholder = placeholder2 {
            alert.addTextField { (textField) in
                textField.placeholder = placeholder
                textField.tag = 2
            }
        }
        
        alert.addAction(cancel)
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
        
    }
    func setDefautForSideMenu() {
        SideMenuManager.defaultManager.menuWidth = 300
        SideMenuManager.defaultManager.menuPushStyle = .popWhenPossible
        SideMenuManager.defaultManager.menuPresentMode = .menuSlideIn
        SideMenuManager.defaultManager.menuFadeStatusBar = false
        SideMenuManager.defaultManager.menuAnimationTransformScaleFactor = 0.9
        SideMenuManager.defaultManager.menuAnimationFadeStrength = 0.5
        SideMenuManager.defaultManager.menuAnimationOptions = .curveLinear
        SideMenuManager.defaultManager.menuDismissOnPush = true
        SideMenuManager.defaultManager.menuAlwaysAnimate = true
    }
//    func changeOrientation(mark: UIInterfaceOrientationMask ) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.orientation = mark
//        var orientation = UIInterfaceOrientation.portrait
//        switch mark {
//        case UIInterfaceOrientationMask.landscape:
//            orientation = .landscapeLeft
//        default:
//            orientation = .portrait
//        }
//        UIDevice.current.setValue(orientation.rawValue, forKey: "orientation")
//    }
    
    func changeRootView(_ vc : UIViewController,completion_: (() -> (Void))?){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let a = appDelegate.window {
            let snapshot:UIView = a.snapshotView(afterScreenUpdates: true)!
            vc.view.addSubview(snapshot)
            a.rootViewController = vc
            a.makeKeyAndVisible()
            UIView.animate(withDuration: 0.3, animations: {() in
                snapshot.layer.opacity = 0;
                snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            }, completion: {
                (value: Bool) in
                snapshot.removeFromSuperview()
                completion_?()
            })
        }
    }

    
    public static func compare(dateA:Date,dateB:Date) -> Bool {
        let gregorian = NSCalendar(calendarIdentifier: .gregorian)
        
        let compA:DateComponents = (gregorian?.components([NSCalendar.Unit.day,NSCalendar.Unit.year,NSCalendar.Unit.month], from: dateA))!
        
        let compB:DateComponents = (gregorian?.components([NSCalendar.Unit.day,NSCalendar.Unit.year,NSCalendar.Unit.month], from: dateB))!
        
        if compA.day == compB.day && compA.month == compB.month && compA.year == compB.year {
            return true
        }
        return false
    }
    
    
    
    public static func localeDate() -> Date {
        let date = Date()
        let zone = TimeZone.ReferenceType.system
        let interVal = zone.secondsFromGMT(for: date)
        return date.addingTimeInterval(TimeInterval(interVal))
    }
    
    
    
    
    public static func calculateContentSize(scrollView: UIScrollView) -> CGSize {
        var topPoint = CGFloat()
        var height = CGFloat()
        
        for subview in scrollView.subviews {
            if subview.frame.origin.y > topPoint {
                topPoint = subview.frame.origin.y
                height = subview.frame.size.height
            }
        }
        return CGSize(width: scrollView.frame.size.width, height: height + topPoint)
    }
    
    public static func showAlertWithCompletion(title: String, message: String,viewController: UIViewController,completion completionHandler: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action) in
            completionHandler?()
        }
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    public static func alertWithAction(title: String?,cancelTitle:String = "OK", message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?],actionStyle: UIAlertAction.Style = .default,viewController: UIViewController,style: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        let cancel = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        alert.addAction(cancel)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: actionStyle, handler: actions[index])
            alert.addAction(action)
        }
        DispatchQueue.main.async {
            viewController.present(alert, animated: true, completion: nil)
        }
    }

    // get current time
//    public static func getCurrentDateTime() -> String {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let date = Date()
//        return formatter.string(from: date)
//    }
    
    public static func stringToDate(_ dateString: String, stringFormat: String = "yyyy-MM-dd HH:mm:ss") -> Date {
        if(dateString != "") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = stringFormat
            dateFormatter.timeZone = TimeZone.ReferenceType.system
            if let d = dateFormatter.date( from: dateString ) {
                return d
            }
        }
        return Date()
    }
    

    public static func getDayOfWeek(today:String)->Int {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.date(from: today)!
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let myComponents = myCalendar.components(.weekday, from: todayDate)
        let weekDay = myComponents.weekday
        return weekDay!
    }
    
    public static func StringToDate(_ dateString: String) -> Date {
        let stringFormat: String = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = stringFormat //"yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: dateString)!
    }
    
    public static func StringToDateFormat(_ dateString: String, stringFormat: String = "HH:mm") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date:Date = dateFormatter.date( from: dateString )!
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }
    
    public static func StringDateFormat(_ dateString: String, stringFormat: String = "dd/MM/yyyy HH:mm") -> String {
        let dateFormatter =  DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date:Date = dateFormatter.date( from: dateString )!
        dateFormatter.dateFormat = stringFormat
        return dateFormatter.string(from: date)
    }
    public static func getDayName(_ day: Int) ->String {
        switch day {
        case 1:
            return "Thứ 2"
        case 2:
            return "Thứ 3"
        case 3:
            return "Thứ 4"
        case 4:
            return "Thứ 5"
        case 5:
            return "Thứ 6"
        case 6:
            return "Thứ 7"
        case 7:
            return "Chủ nhật"
        default:
            return ""
        }
    }
    public static func timeAgoSince(_ date: Date, lang: String = "vn") -> String {
        
        let calendar = Calendar.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
        var prefix:String = ""
        
        
            if components.year! >= 2 {
                prefix = (components.year?.description)! as String
                
                return "\(prefix) năm trước"
            }
        
            if components.year! >= 1 {
                return "1 năm trước"
            }
        
            if components.month! >= 2 {
                prefix = (components.month?.description)! as String
                return "\(prefix) tháng trước"
            }
        
            if components.month! >= 1 {
                return "1 tháng trước"
            }
        
            if components.weekOfYear! >= 2 {
                prefix = (components.weekOfYear?.description)! as String
                return "\(prefix) tuần trước"
            }
        
            if components.weekOfYear! >= 1 {
                return "1 tuần trước"
            }
        
            if components.day! >= 2 {
                prefix = (components.day?.description)! as String
                return prefix + " ngày trước"
            }
        
            if components.day! >= 1 {
                return "Hôm qua"
            }
        
            if components.hour! >= 2 {
                prefix = (components.hour?.description)! as String
                return "\(prefix) giờ trước"
            }
        
            if components.hour! >= 1 {
                return "1 giờ trước"
            }
        
            if components.minute! >= 2 {
                prefix = (components.minute?.description)! as String
                return prefix + " phút trước"
            }
        
            if components.minute! >= 1 {
                return "1 phút trước"
            }
        
            if components.second! >= 3 {
                prefix = (components.second?.description)! as String
                return "\(prefix) giây trước"
            }
        
            return "vừa xong"
        
    }
    
    public static func openUrl(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                          completionHandler: {
                                            (success) in
//                                            print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
//                print("Open \(scheme): \(success)")
            }
        }
    }

    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

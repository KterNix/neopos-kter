//
//  Config.swift
//  neoReal
//
//  Created by mac on 4/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
class Config {
    static let firstCustomerQueueColor = "#7d8b92"
    static let secondCustomerQueueColor = "#d39e00"
    static let thirdCustomerQueueColor = "#bd2130"
    
    static let emptyColor = "FFFFFF"
    static let bookColor = "008744"
    static let depositedColor = "ffa700"
    static let soldColor = "0277bd"
    static let lockedColor = "800000"
    static let deactiveColor = "800000"

    //Socket Action
    static let Deposit = "DEPPOSIT"
    static let EndEvent = "END_EVENT"
    static let BookingCountdown = "BOOKING_COUNTDOWN"

    static let statusSend = "1"
    static let statusCancel = "-1"
    static let statusAccept = "2"
    static let statusDone = "3"
    static let statusEndEvent = "4"

}

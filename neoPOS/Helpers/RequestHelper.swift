//
//  RequestHelper.swift
//  neoReal
//
//  Created by pro on 4/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import TSMessages
import SVProgressHUD
typealias RequestHandle = (_ values: NSDictionary?,_ error: ErrorString) -> Void

class RequestHelper: NSObject {

    static let shared = RequestHelper()
    
    func loginWith(username: String, password: String, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        let param = ["email": username,
                     "password": password]
        
        let errorTitle = "Đăng nhập thất bại."
        let errorMessage = "Email hoặc mật khẩu sai. Vui lòng kiểm tra lại thông tin đăng nhập."
        requestMethod(url: BaseURL.URL_LOGIN, method: .post, param: param, encoding:URLEncoding.default , errorTitle: errorTitle, errorMessage: errorMessage, viewController: viewController, completionHandler: completionHandler)
        
    }
    func getProducts(page: String?,keyWord:String?,vc:UIViewController,completetion :@escaping RequestHandle ) {
        var param = ["":""]

        if let key = keyWord {
            param["keyword"] =  key
        }
        if let page = page {
            param["page"] = page
        }
        requestMethod(url: BaseURL.URL_GET_PRODUCTS, method: .get, param: param, encoding: URLEncoding.default, errorTitle: "Lấy danh sách sản phẩm thất bại.", errorMessage: "Không lấy được danh sách sản phẩm. Lỗi ngoài mong đợi vui lòng thử lại.", viewController: vc, completionHandler: completetion)
    }
    func postProductInWareHouse(product:ProductModel,vc:UIViewController, completion: @escaping RequestHandle) {
        
    }
    func putProduct(product:ProductModel,vc:UIViewController,completetion :@escaping RequestHandle ) {
        var param = product.toJSON()
        param["image"] = product.image?.contains("http") ?? true ? nil : product.image.asString
        param["sizes"] = product.sizes?.toJSONString()
        self.requestMethod(url: BaseURL.URL_PUT_PRODUCT + product.id.asString, method: .put, param: param, encoding: URLEncoding.default, errorTitle: "Cập nhật sản phẩm thất bại!", errorMessage: "Cập nhật sản phẩm không thành công vui lòng thử lại sau.", viewController: vc, completionHandler: completetion)
    }
    func postProduct(product:ProductModel,vc:UIViewController,completetion :@escaping RequestHandle) {
        var param = product.toJSON()
        param["sizes"] = product.sizes?.toJSONString()
        param["image"] = product.image.asString
        requestMethod(url: BaseURL.URL_POST_PRODUCT, method: .post, param: param, encoding: JSONEncoding.default, errorTitle: "Tạo sản phẩm thất bại.", errorMessage: "Lỗi trong quá trình tạo trên máy chủ, vui lòng thử lại sau.", viewController: vc, completionHandler: completetion)
    }
    func getProduct(id:String,vc:UIViewController,completetion :@escaping RequestHandle) {
        
        requestMethod(url: BaseURL.URL_GET_PRODUCT + id, method: .post, param: nil, encoding: URLEncoding.default, errorTitle: "Tạo sản phẩm thất bại.", errorMessage: "Lỗi trong quá trình tạo trên máy chủ, vui lòng thử lại sau.", viewController: vc, completionHandler: completetion)
    }
    func postAndPutProductInWareHouse(size:SizeProdcutInWareHouse,vc:UIViewController,completetion :@escaping RequestHandle) {
        var param = size.toJSON()
        param["size"] = param["sizeid"]
        let title = param["id"] == nil ? "Tạo" : "Cập nhật"
        requestMethod(url: BaseURL.URL_POST_AND_PUT_PRODUCT_IN_WARE_HOUSE, method: .post, param: param, encoding: JSONEncoding.default, errorTitle: title + " sản phẩm trong kho thất bại.", errorMessage: "Lỗi trong quá trình đưa lên máy chủ vui lòng thử lại sau.", viewController: vc, completionHandler: completetion)
    }
    func getProductsInWareHouse(param:[String:Any],vc:UIViewController,completetion :@escaping RequestHandle ) {

        requestMethod(url: BaseURL.URL_GET_PRODUCTS_IN_WARE_HOUSE, method: .get, param: param, encoding: URLEncoding.default, errorTitle: "Lấy danh sách sản phẩm thất bại.", errorMessage: "Không lấy được danh sách sản phẩm. Lỗi ngoài mong đợi vui lòng thử lại.", viewController: vc, completionHandler: completetion)
    }
    func postSize(size:SizeProductModel,vc:UIViewController,completetion :@escaping RequestHandle ) {
        let param = size.toJSON()
        requestMethod(url: BaseURL.URL_POST_SIZE, method: .post, param: param, encoding: JSONEncoding.default, errorTitle: "Tạo size thất bại.", errorMessage: "Quá trình tạo size mới cho sản phẩm không thành công. Vui lòng xem lại thông tin.", viewController: vc, completionHandler: completetion)
    }
    func getProductInWareHouse(product:WareHouseProductModel?,vc:UIViewController,completetion :@escaping RequestHandle ) {
        requestMethod(url: BaseURL.URL_GET_PRODUCT_IN_WARE_HOUSE + (product?.id).asString, method: .get, param: nil, encoding: JSONEncoding.default, errorTitle: "Lấy chi tiết sản phẩm trong kho thất bại.", errorMessage: "Lấy thông tin thất bại, lỗi kết nối với máy chủ, vui lòng thử lại sau", viewController: vc, completionHandler: completetion)
    }
    func getServices(vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_GET_SERVICES, method: .get, param: nil, encoding: URLEncoding.default, errorTitle: "Lấy danh sách dịch vụ thất bại", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau", viewController: vc, completionHandler: completion)
    }
    
    func postTask(param:[String:Any],vc:UIViewController, completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_POST_TASK, method: .post, param: param, encoding: JSONEncoding.default, errorTitle: "Tạo lịch hẹn thất bại!", errorMessage: "Quá trình tạo lịch hẹn thất bại. Vui lòng kiểm tra lại thông tin trước khi tạo.", viewController: vc, completionHandler: completion)
    }
    func putTask(idTask:String,param:[String:Any],vc:UIViewController, completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_PUT_TASK +  idTask, method: .put, param: param, encoding: JSONEncoding.default, errorTitle: "Tạo lịch hẹn thất bại!", errorMessage: "Quá trình tạo lịch hẹn thất bại. Vui lòng kiểm tra lại thông tin trước khi tạo.", viewController: vc, completionHandler: completion)
    }
    func postOrder(param:[String:Any], vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_POST_ORDER, method: .post, param: param, encoding: JSONEncoding.default, errorTitle: "Tạo đơn hàng thất bại", errorMessage: "Quá trình tạo đơn hàng đã thất bại, lỗi có thể xuất phát từ đường truyền mạng hoặc từ máy chủ. Vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func getUser(type:AccountType,param:[String:Any],vc:UIViewController,completion:@escaping RequestHandle) {
        let urlString = type == .Customer ? BaseURL.URL_GET_CUSTOMERS : BaseURL.URL_GET_MEMBERS
        requestMethod(url:urlString , method: .get, param: param, encoding: URLEncoding.default, errorTitle: "Lấy danh sách khách hàng thât bại!", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau", viewController: vc, completionHandler: completion)
    }
    func getOrders(param:[String:String],vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_GET_ORDERS, method: .get, param: param, encoding: URLEncoding.default, errorTitle: "Lấy danh sách đơn hàng thất bại!", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau", viewController: vc, completionHandler: completion)
    }
    func getTasks(param:[String:Any],vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_GET_TASKS, method: .get, param: param, encoding: URLEncoding.default, errorTitle: "Lấy danh sách lịch hẹn thất bại!", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func getPaymentNote_Receipt(param:[String:Any],vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_GET_PAYMENTNOT_RECEIPT, method: .get, param: param, encoding: URLEncoding.default, errorTitle: "Lấy danh sách thu chi thất bại!", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func getChartData(vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_GET_CHART_DATA, method: .get, param: nil, encoding: URLEncoding.default, errorTitle: "Lấy dữ liệu thu chi của năm nay thất bại!", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func postLedger(param:[String:Any],vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_POST_PAYMENTNOT_RECEIPT, method: .post, param: param, encoding: JSONEncoding.default, errorTitle: "Tạo thu chi thất bại!", errorMessage: "Quá trình tạo phiếu thu chi đã thất bại, lỗi có thể xuất phát từ đường truyền mạng hoặc từ máy chủ. Vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func postCustomer(param:[String:Any],vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_POST_CUSTOMER, method: .post, param: param, encoding: JSONEncoding.default, errorTitle: "Tạo khách hàng mới thất bại!", errorMessage: "Quá trình tạo khách hàng mới đã thất bại, lỗi có thể xuất phát từ đường truyền mạng hoặc từ máy chủ. Vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func putCustomer(customer:CustomerModel?,vc:UIViewController,completion:@escaping RequestHandle) {
        let param = customer?.toJSON()
        requestMethod(url: BaseURL.URL_PUT_CUSTOMER + (customer?.id).asString, method: .put, param: param, encoding: JSONEncoding.default, errorTitle: "Cập nhật thất bại!", errorMessage: "Quá trình cập nhật đã thất bại, lỗi có thể xuất phát từ đường truyền mạng hoặc từ máy chủ. Vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func getOrder(id:String,vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_GET_ORDER + id, method: .get, param: nil, encoding: URLEncoding.default, errorTitle: "Lấy chi tiết đơn hàng thất bại!", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func getCategories(vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_GET_PRODUCT_CATEGORIES, method: .get, param: nil, encoding: URLEncoding.default, errorTitle: "Lấy chi tiết đơn hàng thất bại!", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func putUser(id:String,param:[String:Any],vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_PUT_USER + id , method: .put, param: param, encoding: JSONEncoding.default, errorTitle: "Cập nhật không thành công!", errorMessage: "Quá trình cập nhật đã thất bại, lỗi có thể xuất phát từ đường truyền mạng hoặc từ máy chủ. Vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func putSize(size:SizeProductModel?,vc:UIViewController,completion:@escaping RequestHandle) {
        let param = size?.toJSON()
        requestMethod(url: BaseURL.URL_PUT_SIZE + (size?.id).asString  , method: .put, param: param, encoding: JSONEncoding.default, errorTitle: "Cập nhật không thành công!", errorMessage: "Quá trình cập nhật đã thất bại, lỗi có thể xuất phát từ đường truyền mạng hoặc từ máy chủ. Vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func getMember(id:String,vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_PUT_USER + id, method: .get, param: nil, encoding: URLEncoding.default, errorTitle: "Lấy thông tin của bạn không thành công!", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    func getServicesCate(vc:UIViewController,completion:@escaping RequestHandle) {
        requestMethod(url: BaseURL.URL_GET_SERVICES_CATE, method: .get, param: nil, encoding: URLEncoding.default, errorTitle: "Lấy danh sách danh mục dịch vụ không thành công!", errorMessage: "Lấy thông tin từ máy chủ thất bại, vui lòng thử lại sau.", viewController: vc, completionHandler: completion)
    }
    
}

extension RequestHelper {
    //MARK: - BASE FUNCTION
    private func requestMethod(url: String, method: HTTPMethod, param: [String: Any]?,encoding:ParameterEncoding,errorTitle:String,errorMessage:String, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        var header = ["":""]

        if url.contains("token"){
            header = param! as! [String : String]
        }else {
            if let token = Utils.getCurrentToken() {
                header = ["Authorization": token]
            }
        }
        
        
        //MARK: CHECK INTERNET CONNECTION
        guard checkInternetConnection(viewController: viewController) else {
            SVProgressHUD.dismiss()
            return
        }
        
        loadingOnStatusBar(isShow: true)
        SVProgressHUD.show()
        ManagerNetworking.shared.manager.request(url, method: method, parameters: param, encoding: encoding, headers: header).responseJSON { (response) in
//            print(response.result.value)
            SVProgressHUD.dismiss()
            self.loadingOnStatusBar(isShow: false)
            //            print(response.result.value)
            switch response.result {
            case .failure:
                //MARK: - REQUEST ERROR: internet connection, timeout...
                completionHandler(nil, .CANT_REQUEST)
                self.showTSMessage(inView: viewController, title: "Truyền dữ liệu thất bại!", subTitle: "Đường truyền mạng không ổn định vui lòng thử lại sau.")
                break
            case .success:
                
                let values = response.result.value as? NSDictionary
               
                // kiểm tra cho server SB APP
                guard let apiERROR = values?.value(forKey: "error") as? Bool else {
                    //MARK: API ERROR
                    completionHandler(nil, .CANT_REQUEST)
                    self.showTSMessage(inView: viewController, title: "Kiểm tra dữ liệu thất bại.", subTitle: "Không tìm thấy đúng dữ liệu. Vui lòng thử lại sau. (Error bool not found)")
                    return
                }
                if apiERROR {
                    SVProgressHUD.dismiss()
                    self.showTSMessage(inView: viewController, title: errorTitle , subTitle: errorMessage)
//                    completionHandler(values, .MESSAGE)
                    return
                }
                completionHandler(values, .NONE)

            }
        }
    }
    
    private func checkInternetConnection(viewController: UIViewController) -> Bool {
        guard viewController.internetAvailable() else {
            showTSMessage(inView: viewController, title: Constants.ERROR.INTERNET_TITLE, subTitle: Constants.ERROR.INTERNET_SUB_TITLE)
            return false
        }
        return true
    }
    
    
    
    func showTSMessage(inView: UIViewController, title: String, subTitle: String) {
        TSMessage.showNotification(in: inView, title: title, subtitle: subTitle, type: TSMessageNotificationType.error, duration: 10, canBeDismissedByUser: true)
    }
    
    func loadingOnStatusBar(isShow: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = isShow
    }
}












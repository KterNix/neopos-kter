////
////  SocketIOManager.swift
////  neoReal
////
////  Created by pro on 4/20/18.
////  Copyright © 2018 pro. All rights reserved.
////
//
//import Foundation
//import SocketIO
//import ObjectMapper
//import PopupDialog
//class SocketIOManager: NSObject {
//    
//    static let shared = SocketIOManager()
//    var socket:SocketIOClient?
//    var idSocket :String?
//    
//    func connectToServer() {
//        socket?.on("connected") { (data, ack) in
//            guard let id = data.first as? String else { return }
//            self.idSocket = self.idSocket == nil ? id : self.idSocket
//            if self.idSocket == id {
//                guard let user = Utils.getCurrentUser() else {
//                    self.socket?.disconnect()
//                    return }
//                let userString = "\(user.id.asString),\(user.code.asString),\(user.avatar.asString),\(user.fullname.asString)"
//                            
//                self.socket?.emit("connectUser", userString)
//                self.socket?.on("userConnectUpdate", callback: { (data, ack) in
//                   
//                })
//            }
//        }
//        
//    }
//    func stop() {
//        self.socket?.disconnect()
//    }
//    func start() {
//        self.socket?.connect()
//    }
//    private var socketManager = SocketManager(socketURL: URL(string: BaseURL.URL_SOCKET)!, config: [.log(true), .compress])
//
//    private let manager = SocketManager(socketURL: URL(string: BaseURL.URL_SOCKET)!, config: [.log(true), .compress])
//    override init() {
//        super.init()
//        socket = socketManager.defaultSocket
//
//    }
//    func listenBroadcast(completion : @escaping (_ data: [Any]) -> Void) {
//        self.socket?.on("broadcast", callback: { (data, ack) in
//            completion(data)
//        })
//    }
//    func sendBroadcast(data:String) {
//        self.socket?.emit("broadcast", data)
//    }
//    func getConfirm() {
//        self.listenBroadcast { (data) in
//            guard let user = Utils.getCurrentUser() else {return}
//            guard let socket = Mapper<SocketActionModel>().map(JSONObject: data) else{return}
//            if socket.payLoad?.code == user.code {
//                if socket.status == Config.statusSend {
//                    guard let vc = UIApplication.shared.keyWindow?.visibleViewController() else {return}
//                    
//                    let popupVC = ConfirmDepositController()
//                    popupVC.socketModel = socket
//                    let popup = PopupDialog(viewController: popupVC)
//                    vc.present(popup, animated: true, completion: nil)
//                }
//            }
//            
//        }
//    }
//    func sendDepositWithStatus(status: String?, house: HouseModel?, sender: UserModel?, recipient: UserModel?) {
//        let payload = PayLoadModel(recipient: recipient?.code, fullname: recipient?.fullname, email:recipient?.email , code: nil, mobile: recipient?.mobile, avatar: recipient?.avatar, houseId: house?.id, houseCode: house?.code, sellType: house?.sellType, areaWall: house?.areaWall, type: house?.type, price: house?.price, direction: house?.direction, bedNum: house?.bedNum, priceByMeter: house?.priceByMeter, num: house?.num, viewType: house?.viewType, floor: house?.floor, area: house?.area, projectTitle: house?.projectTitle, projectCode: house?.projectCode, blockName: house?.blockName, blockCode: house?.blockCode, path: house?.path, bookingId: nil, memberToken: nil)
//        let socketModel = SocketActionModel(id: nil, payload: payload, customerName: nil, senderCode: sender?.code, action: Config.Deposit, room: nil, dateTime: nil, token: nil, status: status)
//        self.sendBroadcast(data: socketModel.toJSONString()!)
//    }
//}
